// const fs = require('fs-extra');
// const concat = require('concat'); 
// (async function build() {    
// const files = [
//         './dist/test-widget/main-es2015.js',
//         './dist/test-widget/polyfills-es2015.js',
//         './dist/test-widget/scripts.js'    
// ]
// await fs.ensureDir('elements')    
// await concat(files, 'elements/test-widget.js')})()


const fs = require('fs-extra');
const concat = require('concat');
(async function build() {
  const files = [
        './dist/fuse/main-es2015.js',
        './dist/fuse/polyfills-es2015.js'
  ]
  await fs.ensureDir('elements')
  await concat(files, 'elements/elements.js');
})()