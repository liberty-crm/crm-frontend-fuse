// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  hmr: false,
  apiUrl: 'https://localhost:44357/api/',
  openIdConnectSettings: {
    authority: 'https://localhost:44349/',
    client_id: 'tourmanagementclient',
    redirect_uri: 'https://localhost:4200/signin-oidc',
    scope: 'openid profile roles tourmanagementapi',
    response_type: 'id_token token',
    post_logout_redirect_uri: 'https://localhost:4200/',
    automaticSilentRenew: true,
    silent_redirect_uri: 'https://localhost:4200/',
    loadUserInfo: true
  },
};

// export const environment = {
//   production: true,
//   hmr: false,
//   apiUrl: 'https://libertybackend.azurewebsites.net/api/',
//   openIdConnectSettings: {
//   authority: 'https://libertyidentity.azurewebsites.net/',
//   client_id: 'tourmanagementclient',
//   redirect_uri: 'https://libertycrm.azurewebsites.net/signin-oidc',
//   scope: 'openid profile roles tourmanagementapi',
//   response_type: 'id_token token',
//   post_logout_redirect_uri: 'https://libertycrm.azurewebsites.net/',
//     automaticSilentRenew: true,
//     silent_redirect_uri: 'https://libertycrm.azurewebsites.net/appWithoutSidebarAuthentication/redirect-silentrenew',
//     loadUserInfo: true
//   }
// };

// export const environment = {
//   production: true,
//   hmr: false,
//   apiUrl: 'https://lfgcrmbackend.azurewebsites.net/api/',
//   openIdConnectSettings: {
//   authority: 'https://lfgcrmidentity.azurewebsites.net/',
//   client_id: 'tourmanagementclient',
//   redirect_uri: 'https://crm.libertyfinancialgroup.ca/signin-oidc',
//   scope: 'openid profile roles tourmanagementapi',
//   response_type: 'id_token token',
//   post_logout_redirect_uri: 'https://crm.libertyfinancialgroup.ca/',
//     automaticSilentRenew: true,
//     silent_redirect_uri: 'https://crm.libertyfinancialgroup.ca/appWithoutSidebarAuthentication/redirect-silentrenew',
//     loadUserInfo: true
//   }
// };

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
