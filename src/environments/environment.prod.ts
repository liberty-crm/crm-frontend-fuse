export const environment = {
  production: true,
  hmr: false,
   apiUrl: 'https://libertybackend.azurewebsites.net/api/',
  openIdConnectSettings: {
  authority: 'https://libertyidentity.azurewebsites.net/',
  client_id: 'tourmanagementclient',
  redirect_uri: 'https://libertycrm.azurewebsites.net/signin-oidc',
  scope: 'openid profile roles tourmanagementapi',
  response_type: 'id_token token',
  post_logout_redirect_uri: 'https://libertycrm.azurewebsites.net/',
    automaticSilentRenew: true,
    silent_redirect_uri: 'https://libertycrm.azurewebsites.net/appWithoutSidebarAuthentication/redirect-silentrenew',
    loadUserInfo: true
  }
};

// export const environment = {
//   production: true,
//   hmr: false,
//   apiUrl: 'https://lfgcrmbackend.azurewebsites.net/api/',
//   openIdConnectSettings: {
//   authority: 'https://lfgcrmidentity.azurewebsites.net/',
//   client_id: 'tourmanagementclient',
//   redirect_uri: 'https://crm.libertyfinancialgroup.ca/signin-oidc',
//   scope: 'openid profile roles tourmanagementapi',
//   response_type: 'id_token token',
//   post_logout_redirect_uri: 'https://crm.libertyfinancialgroup.ca/',
//     automaticSilentRenew: true,
//     silent_redirect_uri: 'https://crm.libertyfinancialgroup.ca/appWithoutSidebarAuthentication/redirect-silentrenew',
//     loadUserInfo: true
//   }
// };