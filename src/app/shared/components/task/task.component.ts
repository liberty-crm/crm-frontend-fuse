import { Component, OnInit, Input } from '@angular/core';
import { Task } from './models/task.model';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.scss']
})
export class TaskComponent implements OnInit {

  isNewTask = false;
  isTaskList = true;
  @Input() leadId: string;
  @Input() serviceId: string;
  openedTask: Task;

  constructor() { }

  ngOnInit(): void {
  }

  addTask() {
    this.isNewTask = true;
    this.isTaskList = false;
    this.openedTask = null;
  }

  showTaskList() {
    this.isNewTask = false;
    this.isTaskList = true;
    this.openedTask = null;
  }

  openTask(event) {
    this.openedTask = event;
    this.isNewTask = true;
    this.isTaskList = false;
  }

}
