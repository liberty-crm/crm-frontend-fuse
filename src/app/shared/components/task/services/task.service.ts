import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { FilterOptionModel } from 'app/shared/models/filter-option.model';
import { Observable } from 'rxjs';
import { Task } from '../models/task.model';

@Injectable({
  providedIn: 'root'
})
export class TaskService {
    
    Url = environment.apiUrl;

    constructor(private http: HttpClient) { 

    }
  
    getFilteredTasks(filter: FilterOptionModel): Observable<any> {  
      const params = `PageNumber=${filter.pageNumber}&PageSize=${filter.pageSize}&SearchQuery=${filter.searchQuery}&OrderBy=${filter.orderBy}&Fields=${filter.fields}`;
      return this.http.get<any>(`${this.Url}tasks?${params}`, {observe: 'response'});
    }
  
    createTask(task: Task): Observable<Task> {
      return this.http.post<Task>(`${this.Url}tasks`, task);
    }

    updateTaskPartially(id: string, task: any): Observable<any> {
      const options = { headers: new HttpHeaders().set('Content-Type', 'application/json-patch+json') };
      return this.http.patch<any>(`${this.Url}tasks/${id}`, task, options);
    }

    updateTask(id: string, selectedTask: any): Observable<Task> {
      return this.http.put<Task>(`${this.Url}tasks/${id}`, selectedTask);
    }
  
    // getLeadById(id: string): Observable<Lead> {
    //   return this.http.get<Lead>(this.Url + 'leads/' + id);
    // }
  
        
  
    // getLeadServices(filter: FilterOptionModel): Observable<any> {
    //   const params = `PageNumber=${filter.pageNumber}&PageSize=${filter.pageSize}&SearchQuery=${filter.searchQuery}&OrderBy=${filter.oderBy}&Fields=${filter.fields}`;
    //   return this.http.get<any>(`${this.Url}leadserviceoffereds?${params}`, {observe: 'response'});
    // }
  
    // getLeadServiceById(id: string, fields: string): Observable<Lead> {
    //   return this.http.get<Lead>(`${this.Url}leadserviceoffereds/${id}?fields=${fields}`);
    // }
  
    // deleteLeadById(id: number): Observable<Lead> {
    //    return this.http.delete<Lead>(this.Url + 'leads/' + id);
    // }
  }
  