import { Component, OnInit, OnDestroy, ViewChild, Output, EventEmitter, Input } from '@angular/core';
import { Todo } from '../../todo/todo.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subject, Subscription } from 'rxjs';
import { TodoService } from '../../todo/todo.service';
import { takeUntil, debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { FuseUtils } from '@fuse/utils';
import { Task } from '../models/task.model';
import { TaskService } from '../services/task.service';
import { SnackbarService } from 'app/shared/services/snackbar/snackbar.service';
import { DatePipe } from '@angular/common';
import { User } from 'oidc-client';
import { OpenIdConnectService } from 'app/shared/services/open-id-connect.service';
import { HistoryData } from '../../history/models/history.model';
import { HistoryEnum } from 'app/shared/enumerations/enum';
import { HistoryService } from '../../history/services/history.service';

@Component({
  selector: 'app-create-task',
  templateUrl: './create-task.component.html',
  styleUrls: ['./create-task.component.scss']
})
export class CreateTaskComponent implements OnInit , OnDestroy {

    private subscription: Subscription = new Subscription();

    todo: Todo;
    tags: any[];
    formType: string;
    todoForm: FormGroup;
    isFormLoaded = false;        
    taskForm: FormGroup;
    task = new Task();
    loggedInUser: User;
    newValue:any;
    oldValue:any;
    @ViewChild('titleInput')
    titleInputField;
    @Input() leadId: string;
    @Input() serviceId: string;
    @Output() back: EventEmitter<any> = new EventEmitter();
    @Input() openedTask: Task;

    // Private
    private _unsubscribeAll: Subject<any>;
   
    constructor(
        private _todoService: TodoService,
        private _formBuilder: FormBuilder,
        private taskService: TaskService,
        private snackbarService: SnackbarService,
        private datePipe: DatePipe,
        private openIdConnectService: OpenIdConnectService,
        private historyService: HistoryService
    ) {
        // Set the private defaults
        this._unsubscribeAll = new Subject();
        this.loggedInUser = this.openIdConnectService.user;
    }
  
    ngOnInit(): void {

        this.initTaskForm();


        //this.todoForm = this.createTodoForm();
        
        // Subscribe to update the current todo
        this._todoService.onCurrentTodoChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(([todo, formType]) => {

                if ( todo && formType === 'edit' )
                {
                    this.formType = 'edit';
                    this.todo = todo;                   
                }
            });

        // Subscribe to update on tag change
        this._todoService.onTagsChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(labels => {
                this.tags = labels;
            });

        // Subscribe to update on tag change
        this._todoService.onNewTodoClicked
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(() => {
                this.todo = new Todo({});
                this.todo.id = FuseUtils.generateGUID();
                this.formType = 'new';
                // this.todoForm = this.createTodoForm();
                this.focusTitleField();
                this._todoService.onCurrentTodoChanged.next([this.todo, 'new']);
            });
    }
   
    focusTitleField(): void {
        setTimeout(() => {
            this.titleInputField.nativeElement.focus();
        });
    }  
   
    toggleStar(event): void {
        event.stopPropagation();
        this.task.isMarked = !this.task.isMarked;      
    }
   
    toggleImportant(event): void {
        event.stopPropagation();
        this.task.isImportant = !this.task.isImportant;        
    }
  
    toggleCompleted(event): void {
        event.stopPropagation();
        this.task.isCompleted = !this.task.isCompleted;      
    }

    cancel() {
        this.back.emit();
    }

    submitForm(): void {
        // this._todoService.updateTodo(this.todoForm.getRawValue());
        this.taskForm.markAllAsTouched();
        if (this.taskForm.valid) {
           if (this.openedTask) {
               this.updateTask();
           } else {
               this.createTask();
           }
        }
    }

    private updateTask(): void {
        const taskDetails = this.getTaskDetails();
        taskDetails.id = this.openedTask.id;
        this.subscription.add(this.taskService.updateTask(this.openedTask.id, taskDetails)
        .subscribe(res => {
            this.newValue = {
                details: {
                    with: taskDetails,
                },
            };
            const historyTagData = FuseUtils.getDifference(
                this.oldValue,
                this.newValue
            );

            if (historyTagData) {
                const historyToCreate = this.initHistoryDataForUpdate(historyTagData);

                this.createHistory(historyToCreate);
            } else {
                this.back.emit();
            }    
            this.snackbarService.show('Task updated successfully!');
            
        }));
    }

    private createTask() {
        const taskDetails = this.getTaskDetails();
        this.subscription.add(this.taskService.createTask(taskDetails)
        .subscribe(res => {
            const historyToCreate = this.initHistoryDataForCreate();

            this.createHistory(historyToCreate);
            this.snackbarService.show("Task created successfully!");            
        }));
    }

    private createHistory(historyToCreate) {
        this.subscription.add(this.historyService.createHistory(historyToCreate)
          .subscribe(res => {
            this.snackbarService.show('History created successfully!');            
            this.back.emit();
          }, err => {
            console.log(err);
          }));
      }

      private initHistoryDataForCreate() :HistoryData {
        var history = new HistoryData;
        history.leadId = this.leadId;
        history.serviceId = this.serviceId ? this.serviceId : null,
        history.activity = HistoryEnum["Task Created"];
        history.userId = this.loggedInUser.profile.sub;
        return history;
      }

      private initHistoryDataForUpdate(historyTagData: string) :HistoryData {
        var history = new HistoryData;
        history.leadId = this.leadId;
        history.serviceId = this.serviceId ? this.serviceId : null,
        history.activity = HistoryEnum["Task Updated"];
        history.userId = this.loggedInUser.profile.sub;
        history.tagData = historyTagData;
        return history;
      }
    
    private initTaskForm(): void {
        if (this.openedTask) {
            this.task = this.openedTask;

            if (this.openedTask.date) {
                this.openedTask.date = this.datePipe.transform(this.openedTask.date.toString(), 'yyyy-MM-dd');
            }

            this.oldValue = {        
                details: {          
                  with: this.openedTask
                }
              };            
            this.taskForm = this._formBuilder.group({         
                title: [this.task.title, [Validators.required, Validators.maxLength(250)]],
                description: [this.task.description],
                date: [this.task.date]
            });

        } else {
            this.taskForm = this._formBuilder.group({         
                title: ['', [Validators.required, Validators.maxLength(250)]],
                description: [''],
                date: [null]
            });
        }
          
    }  

    private getTaskDetails(): Task {
        return {
            id: '',
            title: this.taskForm.get('title').value,
            description: this.taskForm.get('description').value,
            date: this.datePipe.transform(this.taskForm.get('date').value?.toString(), 'yyyy-MM-dd'),
            isCompleted: this.task.isCompleted,
            isMarked: this.task.isMarked,
            isImportant: this.task.isImportant,
            leadId: this.leadId ? this.leadId : null,
            serviceId: this.serviceId ? this.serviceId : null,
            userId: this.loggedInUser.profile.sub
        }
    }

    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
        this.subscription.unsubscribe();
    }
}

