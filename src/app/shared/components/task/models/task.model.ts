export class Task {
    id: string;
    title: string;
    date: null | string | Date;
    isCompleted: boolean;
    description: string;
    isImportant: boolean;
    isMarked: boolean;
    leadId: string;
    serviceId: string;
    userId: string;
}