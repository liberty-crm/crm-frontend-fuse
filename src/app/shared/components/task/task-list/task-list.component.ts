import { Component, OnInit, ViewEncapsulation, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { Todo } from '../../todo/todo.model';
import { Subject, Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { TodoService } from '../../todo/todo.service';
import { takeUntil } from 'rxjs/operators';
import { Location } from '@angular/common';
import { TaskService } from '../services/task.service';
import { Task } from '../models/task.model';
import { FilterOptionModel } from 'app/shared/models/filter-option.model';
import { Pagination } from 'app/shared/models/pagination';
import { User } from 'oidc-client';
import { OpenIdConnectService } from 'app/shared/services/open-id-connect.service';
import { CustomEventService } from 'app/shared/services/custom-event-serverice/custom-event.service';

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations   : fuseAnimations
})
export class TaskListComponent implements OnInit, OnDestroy {
    
    private subscription: Subscription = new Subscription();
    todos: Todo[];
    currentTodo: Todo;
    tasks: Task[] = [];
    currentTask: Task;
    paginationInfo: Pagination;
    filterOptionModel: FilterOptionModel;
    loggedInUser: User;
    isTasksLoaded = false;
    loggedInUserId: string;
    @Input() showCompletedTasks: boolean = true;
    @Input() leadId: string;
    @Input() serviceId: string;
    @Output() openTask: EventEmitter<any> = new EventEmitter();
    private _unsubscribeAll: Subject<any>;

    constructor(
        private _activatedRoute: ActivatedRoute,
        private _todoService: TodoService,
        private _location: Location,
        private taskService: TaskService,
        private customEventService: CustomEventService,
        private openIdConnectService: OpenIdConnectService
    ) {
        // Set the private defaults
        this.loggedInUser = this.openIdConnectService.user;
        this.loggedInUserId = this.loggedInUser.profile.sub;
        this._unsubscribeAll = new Subject();
    }
   
    ngOnInit(): void {

        this.initFilterOptionModel();
        this.getTaskList();

        this.registerRefreshTaskListEvent();

         // Subscribe to update todos on changes
         this._todoService.onTodosChanged
         .pipe(takeUntil(this._unsubscribeAll))
         .subscribe(todos => {
             this.todos = todos;
         });

     // Subscribe to update current todo on changes
     this._todoService.onCurrentTodoChanged
         .pipe(takeUntil(this._unsubscribeAll))
         .subscribe(currentTodo => {
             if ( !currentTodo )
             {
                 // Set the current todo id to null to deselect the current todo
                 this.currentTodo = null;

                 // Handle the location changes
                 const tagHandle    = this._activatedRoute.snapshot.params.tagHandle,
                       filterHandle = this._activatedRoute.snapshot.params.filterHandle;

                 if ( tagHandle )
                 {
                     this._location.go('apps/todo/tag/' + tagHandle);
                 }
                 else if ( filterHandle )
                 {
                     this._location.go('apps/todo/filter/' + filterHandle);
                 }
                 else
                 {
                     this._location.go('apps/todo/all');
                 }
             }
             else
             {
                 this.currentTodo = currentTodo;
             }
         });
    }
   
    readTodo(task): void {
        // Set current todo
        // this._todoService.setCurrentTodo(todoId);
        this.openTask.emit(task);
    }
   
    onDrop(ev): void {

    }

    getNextPage() {
        this.filterOptionModel.pageNumber = this.paginationInfo.currentPage + 1;
        this.getTaskList();
      }

    private registerRefreshTaskListEvent() {
        this.subscription.add(this.customEventService.RefreshTaskListEvent
            .subscribe(res => {
                this.initFilterOptionModel();
                this.getTaskList();
            }));
    }

    private getTaskList() {
        this.subscription.add(this.taskService.getFilteredTasks(this.filterOptionModel)
        .subscribe(res => {
            this.paginationInfo = JSON.parse(res.headers.get('X-Pagination'));
            this.paginationInfo.currentPage = this.paginationInfo.currentPage;             
            if (this.filterOptionModel.pageNumber === 1) {
                this.tasks = res.body;    
            } else {
               this.tasks.push(...res.body);
            }            
            this.isTasksLoaded = true;
        }));
    }

    private initFilterOptionModel() {
        let searchQuery = '';
        if (!this.serviceId && !this.leadId) {
            searchQuery ='UserId=="' + this.loggedInUserId + '"';
        } else if (this.serviceId) {
            searchQuery = 'UserId=="' + this.loggedInUserId 
                                                + '" AND ServiceId=="' + this.serviceId + '"'
                                                + ' AND LeadId=="' + this.leadId + '"';
        } else {
           searchQuery = 'UserId=="' + this.loggedInUserId 
                                                + '" AND LeadId=="' + this.leadId + '"'
                                                + ' AND ServiceId==NULL';
        }

        if (this.showCompletedTasks === false) {
            if (searchQuery) {
                searchQuery = searchQuery + ' AND IsCompleted==false'
            } else {
                searchQuery = 'IsCompleted==false';
            }
        }

        this.filterOptionModel = new FilterOptionModel({searchQuery, pageSize:5, orderBy: 'UpdatedOn desc'});

      }

    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
        this.subscription.unsubscribe();
    }
}

