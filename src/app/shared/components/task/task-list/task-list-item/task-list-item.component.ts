import { Component, OnInit, OnDestroy, Input, HostBinding, ViewEncapsulation } from '@angular/core';
import { Subject, Subscription } from 'rxjs';
import { Todo } from 'app/shared/components/todo/todo.model';
import { TodoService } from 'app/shared/components/todo/todo.service';
import { ActivatedRoute } from '@angular/router';
import { Task } from '../../models/task.model';
import { TaskService } from '../../services/task.service';
import { CustomEventService } from 'app/shared/services/custom-event-serverice/custom-event.service';
import { FuseUtils } from '@fuse/utils';
import { HistoryData } from 'app/shared/components/history/models/history.model';
import { HistoryEnum } from 'app/shared/enumerations/enum';
import { User } from 'oidc-client';
import { OpenIdConnectService } from 'app/shared/services/open-id-connect.service';
import { HistoryService } from 'app/shared/components/history/services/history.service';
import { cloneDeep } from 'lodash';

@Component({
  selector: 'app-task-list-item',
  templateUrl: './task-list-item.component.html',
  styleUrls: ['./task-list-item.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TaskListItemComponent implements OnInit , OnDestroy
{
    private subscription: Subscription = new Subscription();
    tags: any[];

    // @Input()
    // todo: Todo;
    @Input() task: Task;

    @HostBinding('class.selected')
    selected: boolean;

    @HostBinding('class.completed')
    completed: boolean;

    @HostBinding('class.move-disabled')
    moveDisabled: boolean;

    markAsDoneTooltip: string;

    loggedInUser: User;
    newValue: any;
    oldValue: any;
 
    private _unsubscribeAll: Subject<any>;
   
    constructor(
        private _todoService: TodoService,
        private _activatedRoute: ActivatedRoute,
        private customEventService: CustomEventService,
        private taskService: TaskService,
        private openIdConnectService: OpenIdConnectService,
        private historyService: HistoryService
    ) {
        this.loggedInUser = this.openIdConnectService.user;
        this._unsubscribeAll = new Subject();
    }

  
    ngOnInit(): void
    {
        // Set the initial values
        // this.todo = new Todo(this.todo);
        this.completed = this.task.isCompleted;

        this.markAsDoneTooltip = this.task.isCompleted ? 'Mark as undone' : 'Mark as done';

        this.oldValue = {        
            details: {          
              with: cloneDeep(this.task)
            }
          };  
    }
   
    onSelectedChange(): void
    {
        // this._todoService.toggleSelectedTodo(this.task.id);
    }
  
    toggleStar(event): void
    {
        event.stopPropagation();
        this.task.isMarked = !this.task.isMarked;
        const partialTask = [{ value: this.task.isMarked, path: '/isMarked', op: 'replace' }];
        const updatingTask = cloneDeep(this.task);
        this.subscription.add(this.taskService.updateTaskPartially(this.task.id, partialTask)
        .subscribe(res => {
            // create history
            this.createHistory(updatingTask);
        }));
    }

    toggleImportant(event): void
    {
        event.stopPropagation();
        this.task.isImportant = !this.task.isImportant;
        const partialTask = [{ value: this.task.isImportant, path: '/isImportant', op: 'replace' }];
        const updatingTask = cloneDeep(this.task);
        this.subscription.add(this.taskService.updateTaskPartially(this.task.id, partialTask)
        .subscribe(res => {
            
            // create history
            this.createHistory(updatingTask);
        }));        
    }
    
    toggleCompleted(event): void
    {
        event.stopPropagation();
        this.task.isCompleted = !this.task.isCompleted;
        this.completed = this.task.isCompleted;
        const partialTask = [{ value: this.task.isCompleted, path: '/isCompleted', op: 'replace' }];
        const updatingTask = cloneDeep(this.task);
        this.subscription.add(this.taskService.updateTaskPartially(this.task.id, partialTask)
        .subscribe(res => {            
            
            // create history
            this.createHistory(updatingTask);

            this.customEventService.RefreshTaskListEvent.next();
        }));

        // this.todo.toggleCompleted();
        // this._todoService.updateTodo(this.todo);
    }

    private initHistoryDataForUpdate(historyTagData: string): HistoryData {
        const history = new HistoryData();
        history.leadId = this.task.leadId;
        history.serviceId = this.task.serviceId,
        history.activity = HistoryEnum['Task Updated'];
        history.userId = this.loggedInUser.profile.sub;
        history.tagData = historyTagData;
        return history;
    }

    private createHistory(updatedTask): void {
    this.newValue = {        
        details: {          
            with: updatedTask
        }
        };

    const historyTagData = FuseUtils.getDifference(this.oldValue, this.newValue);

    const historyToCreate = this.initHistoryDataForUpdate(historyTagData);

    this.subscription.add(this.historyService.createHistory(historyToCreate)
        .subscribe(res => {
        // do nothing      
        }));
    }

    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
        this.subscription.unsubscribe();
    }
}
