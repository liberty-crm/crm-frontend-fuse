import { Component, OnInit, Input } from '@angular/core';
import { Subscription } from 'rxjs';
import { Pagination } from 'app/shared/models/pagination';
import { FilterOptionModel } from 'app/shared/models/filter-option.model';
import { Router, ActivatedRoute } from '@angular/router';
import { Payment } from './models/payment.model';
import { PaymentService } from './services/payment.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss']
})
export class PaymentComponent implements OnInit {

  private subscription: Subscription = new Subscription();
  @Input() leadServiceOfferedId: string;
  rows: Payment[];
  loadingIndicator: boolean;
  reorderable: boolean;  
  paginationInfo: Pagination;
  filterOptionModel: FilterOptionModel; 
  isEditPayment = false;
  isNewPayment = false;
  isPaymentList = true;
  openedPayment: Payment;
  leadId: string;
  selectedPaymentId: string;

  columns = [
    { prop: 'date', name: 'Date' },
    { prop: 'mode', name: 'Payment Mode' },
    { prop: 'amount', name: 'Payment Amount' },
    { prop: 'location', name: 'Payment Location' },
    { prop: 'referenceDetails', name: 'Payment Reference Detail' }  
   ];
  
  constructor(private router: Router,
             private activatedRoute: ActivatedRoute,
             private datePipe: DatePipe,
             private paymentService: PaymentService) {
      this.activatedRoute.params.subscribe(params => {
        //this.leadServiceOfferedId = params.id;
        
      });
      this.loadingIndicator = true;
      this.reorderable = true;
     }

  ngOnInit(): void {
    this.initFilterOptionModel();
        this.getPayment();
  }

  
  getNextPage(pageInfo) {
    this.filterOptionModel.pageNumber = pageInfo.offset + 1;
    // this.getPayment();
  }

  public getPayment(): void { 
    this.paymentService.getFilteredPayments(this.filterOptionModel).subscribe(res => {  
        this.paginationInfo = JSON.parse(res.headers.get('X-Pagination'));
        this.paginationInfo.currentPage = this.paginationInfo.currentPage - 1;
        this.rows = res.body;
        this.rows.forEach(elem => {
            elem.date = this.datePipe.transform(elem.date + 'Z', 'MM/dd/yyyy');
        });
        this.loadingIndicator = false;                      
    });
  }

  paymentRowSelect(event): any {        
    if (event.type === 'click') {   
        this.isEditPayment = true;
        this.selectedPaymentId = event.row.id; 
    } 
  }

  private initFilterOptionModel() {
    let searchQuery = 'LeadServiceId=="' + this.leadServiceOfferedId + '"';
    this.filterOptionModel = new FilterOptionModel({searchQuery});                                          
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  goToCreatePayment(): void {
    this.isNewPayment = true;
  }

  paymentCreated() {
    this.initFilterOptionModel();
    this.getPayment();
    this.isNewPayment = false;    
  }

  paymentUpdated() {
    this.initFilterOptionModel();
    this.getPayment();
    this.isEditPayment = false;
  }

}
