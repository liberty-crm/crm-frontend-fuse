import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { HttpClient } from '@angular/common/http';
import { environment } from 'environments/environment';
import { FilterOptionModel } from 'app/shared/models/filter-option.model';
import { Payment } from '../models/payment.model';

@Injectable({
  providedIn: 'root'
})
export class PaymentService {
  selectedProduct: Payment;
  productList: Payment[];
  Url = environment.apiUrl;

  constructor(private http: HttpClient) {    
   }

   createPayment(paymentSetupData: Payment): Observable<Payment> {
    return this.http.post<Payment>(this.Url + '/payments', paymentSetupData);
  }

  updatePayment(id: string, paymentSetupData: Payment): Observable<Payment> {
    return this.http.put<Payment>(`${this.Url}/payments/${id}`, paymentSetupData);
  }

  getPaymentById(id: string): Observable<Payment> {
    return this.http.get<Payment>(`${this.Url}/payments/${id}`);
  }

  getFilteredPayments(filter: FilterOptionModel): Observable<any> {   
    const params = `PageNumber=${filter.pageNumber}&PageSize=${filter.pageSize}&SearchQuery=${filter.searchQuery}&OrderBy=${filter.orderBy}&Fields=${filter.fields}`;
    return this.http.get<any>(this.Url + `payments?${params}`, {observe: 'response'});
  }

}
