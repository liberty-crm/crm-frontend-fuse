export class Payment {
    id: string;
    date: null | string | Date;
    mode: string;
    amount: number;
    location: string;
    referenceDetails: string;
    leadServiceId: string;
}