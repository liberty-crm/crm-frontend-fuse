import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FuseSharedModule } from '@fuse/shared.module';
import { SharedModule } from 'app/shared/shared.module';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { RouterModule, Routes } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { PaymentComponent } from '../payment.component';
import { CreatePaymentComponent } from '../create-payment/create-payment.component';
import { EditPaymentComponent } from '../edit-payment/edit-payment.component';

const routes: Routes = [
  { path: '', component: PaymentComponent },
  { path: 'create', component: CreatePaymentComponent },
  { 
      path: 'edit/:id', 
      component: EditPaymentComponent
   },
];

@NgModule({
  declarations: [
    PaymentComponent,
    CreatePaymentComponent,
    EditPaymentComponent,
  ],
  imports: [
    CommonModule,
    FuseSharedModule,
    SharedModule,
    // RouterModule.forChild(routes),
    NgxDatatableModule,
    MatButtonModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatSelectModule,
    MatCheckboxModule,
  ],
  exports: [
    PaymentComponent,
    CreatePaymentComponent,
    EditPaymentComponent,
  ]
})
export class PaymentModule { }
