import { Component, OnInit, Output, EventEmitter, OnDestroy, Input } from '@angular/core';
import { Subscription } from 'rxjs';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { SnackbarService } from 'app/shared/services/snackbar/snackbar.service';
import { REGEX_PATTERNS } from 'app/shared/constants/regex-pattern.constant';
import { CustomAsyncValidator } from 'app/shared/async-validators/async-validators';
import { Payment } from '../models/payment.model';
import { PaymentService } from '../services/payment.service';

@Component({
  selector: 'app-edit-payment',
  templateUrl: './edit-payment.component.html',
  styleUrls: ['./edit-payment.component.scss']
})
export class EditPaymentComponent implements OnInit, OnDestroy {

  private subscription: Subscription = new Subscription();
  paymentForm: FormGroup;
  isFormSubmit = false;
  isFormLoaded = false;
  @Output() paymentUpdated: EventEmitter<any> = new EventEmitter();
  leadId: string;
  @Input() id: string;
  paymentModel: Payment;
  modes= [
    'Email Payment',
    'Cash',
    'Cheque',
    'Credit Card',
    'Debit Card',
    'Payment Gateway',
    'Others'
  ]
  locations=[
    'Online', 
    'Central Email Inbox',
    'LFG Office',
    'Store',
    'Others'
  ]
  
  constructor(private fb: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private snackbarService: SnackbarService,
    private router: Router,           
    private paymentService: PaymentService,
    private customAsyncValidator: CustomAsyncValidator) { }
    

  ngOnInit(): void {
    this.getPaymentById();
  }

  submitForm(){
    var payment = new Payment;    
    if (this.paymentForm.valid) {
      const result: Payment = Object.assign(this.paymentModel, this.paymentForm.value);
      this.paymentService.updatePayment(this.paymentModel.id, result).subscribe((res: Payment) => {
        this.snackbarService.show('Payment updated successfully!');
        this.paymentUpdated.emit();
      });
    }
  }

  private initPaymentForm(): void {
    this.paymentForm = this.fb.group({
      date: [this.paymentModel.date, Validators.required],
      mode: [this.paymentModel.mode, Validators.required],
      amount: [this.paymentModel.amount, [Validators.required, Validators.pattern(REGEX_PATTERNS.DECIMAL_NUMBER)]],
      location: [this.paymentModel.location, Validators.required],
      referenceDetails: [this.paymentModel.referenceDetails, Validators.required]
    });  
    this.isFormLoaded = true;
  }

  private getPaymentById() {
    this.subscription.add(this.paymentService.getPaymentById(this.id)
    .subscribe(res => {
        this.paymentModel = res;
        this.initPaymentForm();
    }));
  }

  cancel() {
    this.paymentUpdated.emit();
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

}
