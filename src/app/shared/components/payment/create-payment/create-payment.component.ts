import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { Subscription } from 'rxjs';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { SnackbarService } from 'app/shared/services/snackbar/snackbar.service';
import { REGEX_PATTERNS } from 'app/shared/constants/regex-pattern.constant';
import { CustomAsyncValidator } from 'app/shared/async-validators/async-validators';
import { Payment } from '../models/payment.model';
import { PaymentService } from '../services/payment.service';

@Component({
  selector: 'app-create-payment',
  templateUrl: './create-payment.component.html',
  styleUrls: ['./create-payment.component.scss']
})
export class CreatePaymentComponent implements OnInit {

  @Input() leadServiceOfferedId: string;
  private subscription: Subscription = new Subscription();
  paymentForm: FormGroup;
  isFormSubmit = false;
  @Output() paymentCreated: EventEmitter<any> = new EventEmitter();
  modes= [
    'Email Payment',
    'Cash',
    'Cheque',
    'Credit Card',
    'Debit Card',
    'Payment Gateway',
    'Others'
  ]
  locations=[
    'Online', 
    'Central Email Inbox',
    'LFG Office',
    'Store',
    'Others'
  ]
  constructor(private fb: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private snackbarService: SnackbarService,
    private router: Router,           
    private customAsyncValidator: CustomAsyncValidator,
    private paymentService: PaymentService,) { }

  ngOnInit(): void {
    this.initPaymentForm();
  }
  submitForm(){
    var payment = new Payment;
    payment.leadServiceId = this.leadServiceOfferedId;
    if (this.paymentForm.valid) {
      const result: Payment = Object.assign({}, this.paymentForm.value);
      this.paymentService.createPayment(result).subscribe((res: Payment) => {
        this.snackbarService.show('Payment added successfully!');
        this.paymentCreated.emit();    
      });
    }
  }

  private initPaymentForm(): void {
  
    this.paymentForm = this.fb.group({
      date: ['', Validators.required],
      mode: ['', Validators.required],
      location: ['', Validators.required],
      amount: ['', [Validators.required, Validators.pattern(REGEX_PATTERNS.DECIMAL_NUMBER)]],
      referenceDetails: ['', Validators.required],
      leadServiceId: this.leadServiceOfferedId ? this.leadServiceOfferedId : '',
    });  
  }

  cancel() {
    this.paymentCreated.emit();    
}

}
