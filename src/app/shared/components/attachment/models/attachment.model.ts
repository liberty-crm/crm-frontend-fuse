export class Attachment {
    id: string;
    leadId: string;
    serviceId: string;
    userId: string;    
    originalDocumentName: string;
    uploadedDocumentName: string;
    uploadPath: string;
    type: string;
    size: number;
    uploadedOn: string | Date;
}