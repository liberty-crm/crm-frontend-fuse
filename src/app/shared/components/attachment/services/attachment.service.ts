import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { FilterOptionModel } from 'app/shared/models/filter-option.model';
import { Attachment } from '../models/attachment.model';

@Injectable({
  providedIn: 'root'
})
export class AttachmentService {
    
    Url = environment.apiUrl;

    constructor(private http: HttpClient) { 

    }
  
    getFilteredAttachment(filter: FilterOptionModel): Observable<any> {  
      const params = `PageNumber=${filter.pageNumber}&PageSize=${filter.pageSize}&SearchQuery=${filter.searchQuery}&OrderBy=${filter.orderBy}&Fields=${filter.fields}`;
      return this.http.get<any>(`${this.Url}attachments?${params}`, {observe: 'response'});
    }
  
    uploadAttachment(formData: FormData, attachmentDetails: Attachment): Observable<any> {
      const params = `attachmentType=${attachmentDetails.type}&leadId=${attachmentDetails.leadId}&serviceId=${attachmentDetails.serviceId}&userId=${attachmentDetails.userId}`;
      const options = { headers: new HttpHeaders().set('Content-Type', undefined) };
      return this.http.post<any>(`${this.Url}attachments/UploadAttachment?${params}`, formData);
    }    
  }
  