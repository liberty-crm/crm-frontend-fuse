import { Component, OnInit, ViewEncapsulation, OnDestroy, Input } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { Attachment } from './models/attachment.model';
import { Subject, Observable, Subscription } from 'rxjs';
import { FuseSidebarService } from '@fuse/components/sidebar/sidebar.service';
import { takeUntil } from 'rxjs/operators';
import { DataSource } from '@angular/cdk/table';
import { FileManagerService } from './file-manager.service';
import { MatTableDataSource } from '@angular/material/table';
import { AttachmentService } from './services/attachment.service';
import { OpenIdConnectService } from 'app/shared/services/open-id-connect.service';
import { Pagination } from 'app/shared/models/pagination';
import { FilterOptionModel } from 'app/shared/models/filter-option.model';
import { FuseUtils } from '@fuse/utils';
import { ALLOWED } from 'app/shared/constants/allowed.constant';
import { MatDialog } from '@angular/material/dialog';
import { AlertDialogComponent } from '../alert-dialog/alert-dialog.component';
import { saveAs } from 'file-saver';
import { HistoryData } from '../history/models/history.model';
import { HistoryEnum } from 'app/shared/enumerations/enum';
import { HistoryService } from '../history/services/history.service';

@Component({
  selector: 'app-attachment',
  templateUrl: './attachment.component.html',
  styleUrls: ['./attachment.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations   : fuseAnimations
})
export class AttachmentComponent implements OnInit , OnDestroy
{
    private subscription: Subscription = new Subscription();
    rows: Attachment[];
    loadingIndicator: boolean;
    reorderable: boolean;  
    paginationInfo: Pagination;
    filterOptionModel: FilterOptionModel; 
    files: any;
    dataSource: any;    
    selected: any;  
    @Input() leadId: string;
    @Input() serviceId: string;
    loggedInUserId: string;
   
    constructor(private attachmentService: AttachmentService,
                public dialog: MatDialog,
                private openIdConnectService: OpenIdConnectService,
                private historyService: HistoryService) {
                    this.loggedInUserId = openIdConnectService.user.profile.sub;
                    this.loadingIndicator = true;
                    this.reorderable = true;
    }

  
    ngOnInit(): void  {
        this.initFilterOptionModel();
        this.getAttachments();            
    }

    uploadAttachment(files) {
        if (files.length > 0) {
            const formData = new FormData;

            for (let file of files) {
                if (FuseUtils.isFileValid(file, ALLOWED.MAX_ATTACHMENT_SIZE, ALLOWED.ATTACHMENT_EXTENSIONS)) {
                    formData.append(file.name, file);
                } else {
                    this.showAlert();
                    return false;
                }
            } 
          
            const attachment = new Attachment();
            attachment.userId = this.openIdConnectService.user.profile.sub;
            attachment.leadId = this.leadId;
            attachment.serviceId = this.serviceId || '';
            attachment.type = FuseUtils.getFileType(files[0]);
            this.subscription.add(this.attachmentService.uploadAttachment(formData, attachment)
            .subscribe(res => {
                this.initFilterOptionModel();
                this.getAttachments();
            }))

            var historyToCreate = this.initHistoryDataForCreate();

            this.createHistory(historyToCreate);
        }
    }

    private createHistory(historyToCreate) {
        this.subscription.add(this.historyService.createHistory(historyToCreate)
          .subscribe(res => {
          }, err => {
            console.log(err);
          }));
      }

      private initHistoryDataForCreate() :HistoryData {
        var history = new HistoryData;
        history.leadId = this.leadId;
        history.serviceId = this.serviceId ? this.serviceId : null,
        history.activity = HistoryEnum["Attachment Created"];
        history.userId = this.loggedInUserId;
        return history;
      }

      private initHistoryDataForDownload() :HistoryData {
        var history = new HistoryData;
        history.leadId = this.leadId;
        history.serviceId = this.serviceId ? this.serviceId : null,
        history.activity = HistoryEnum["Attachment Downloaded"];
        history.userId = this.loggedInUserId;
        return history;
      }

    attachmentRowSelect(event): any {        
        if (event.type === 'click') {                           
          // this.router.navigate([`lead/contacts/${this.leadId}/edit/${event.row.id}`]);
        } 
      }

    public getAttachments(): void {      
      this.attachmentService.getFilteredAttachment(this.filterOptionModel).subscribe(res => {  
          this.paginationInfo = JSON.parse(res.headers.get('X-Pagination'));
          this.paginationInfo.currentPage = this.paginationInfo.currentPage - 1; 
          this.rows = res.body;
          this.loadingIndicator = false;                      
      });
    }

    downloadAttachment(attachment: Attachment) {
        saveAs(attachment.uploadPath, attachment.originalDocumentName);
        var historyToCreate = this.initHistoryDataForDownload();
        this.createHistory(historyToCreate);
    }

    getNextPage(pageInfo) {
        this.filterOptionModel.pageNumber = pageInfo.offset + 1;
        this.getAttachments();
    }

    private initFilterOptionModel() {
        let searchQuery = '';
        if (this.serviceId) {
            searchQuery = 'ServiceId=="' + this.serviceId + '"'
                                                + ' AND LeadId=="' + this.leadId + '"';
        } else {
            searchQuery = 'LeadId=="' + this.leadId + '"'
                                                + ' AND ServiceId==NULL';
        }
        this.filterOptionModel = new FilterOptionModel({ searchQuery });
    }

    private showAlert() {
        this.dialog.open(AlertDialogComponent, {
            data: {
                title: 'Invalid file!',
                message: `Only ${ALLOWED.ATTACHMENT_EXTENSIONS.join(',')} files are accepted with size ${ALLOWED.MAX_ATTACHMENT_SIZE / 1000000} MB`
            }
        });
    }
    
    ngOnDestroy(): void {
        this.subscription.unsubscribe();
    }  
}