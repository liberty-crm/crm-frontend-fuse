import { Component, Inject, ViewEncapsulation, OnDestroy } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { CalendarEvent } from 'angular-calendar';

import { MatColors } from '@fuse/mat-colors';
import { CalendarEventModel } from '../event.model';
import { Task } from '../../task/models/task.model';
import { DatePipe } from '@angular/common';
import { OpenIdConnectService } from 'app/shared/services/open-id-connect.service';
import { TaskService } from '../../task/services/task.service';
import { Subscription } from 'rxjs';
import { SnackbarService } from 'app/shared/services/snackbar/snackbar.service';
import { CustomEventService } from 'app/shared/services/custom-event-serverice/custom-event.service';
import { HistoryData } from '../../history/models/history.model';
import { HistoryEnum } from 'app/shared/enumerations/enum';
import { User } from 'oidc-client';
import { HistoryService } from '../../history/services/history.service';
import { constant } from 'lodash';
import { FuseUtils } from '@fuse/utils';

@Component({
    selector     : 'calendar-event-form-dialog',
    templateUrl  : './event-form.component.html',
    styleUrls    : ['./event-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class CalendarEventFormDialogComponent implements OnDestroy
{
    private subscription: Subscription = new Subscription();
    action: string;
    event: CalendarEvent;
    task = new Task();
    openedTask = new Task();
    // eventForm: FormGroup;
    taskForm: FormGroup;
    dialogTitle: string;
    presetColors = MatColors.presets;
    newValue: any;
    oldValue: any;
    loggedInUser: User;

    /**
     * Constructor
     *
     * @param {MatDialogRef<CalendarEventFormDialogComponent>} matDialogRef
     * @param _data
     * @param {FormBuilder} _formBuilder
     */
    constructor(
        public matDialogRef: MatDialogRef<CalendarEventFormDialogComponent>,
        @Inject(MAT_DIALOG_DATA) private _data: any,
        private _formBuilder: FormBuilder,
        private datePipe: DatePipe,
        private taskService: TaskService,
        private customEventService: CustomEventService,
        private snackbarService: SnackbarService,
        private historyService: HistoryService,
        private openIdConnectService: OpenIdConnectService
    )
    {        
        this.loggedInUser = this.openIdConnectService.user;
        this.event = _data.event;
        this.action = _data.action;

        if ( this.action === 'edit' )
        {
            this.openedTask.id = _data.event.id.toString();
            this.openedTask.title = _data.event.title;
            this.openedTask.description = _data.event.meta.notes;
            this.openedTask.date =  this.datePipe.transform(_data.event.start.toString(), 'yyyy-MM-dd');
            this.openedTask.leadId = _data.event.leadId;
            this.openedTask.serviceId = _data.event.serviceId;
            this.openedTask.isCompleted = _data.event.isCompleted;
            this.task.isCompleted = _data.event.isCompleted;
            this.dialogTitle = this.event.title;
        }
        else
        {
            this.dialogTitle = 'New Task';
            this.event = new CalendarEventModel({
                start: _data.date,
                end  : _data.date
            });
        }
        this.initTaskForm();
        // this.eventForm = this.createEventForm();
    }    

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Create the event form
     *
     * @returns {FormGroup}
     */
    createEventForm(): FormGroup
    {
        return new FormGroup({
            title : new FormControl(this.event.title),
            start : new FormControl(this.event.start),
            end   : new FormControl(this.event.end),
            allDay: new FormControl(this.event.allDay),
            color : this._formBuilder.group({
                primary  : new FormControl(this.event.color.primary),
                secondary: new FormControl(this.event.color.secondary)
            }),
            meta  :
                this._formBuilder.group({
                    location: new FormControl(this.event.meta.location),
                    notes   : new FormControl(this.event.meta.notes)
                })
        });
    }

    toggleCompleted(event): void {
        event.stopPropagation();
        this.task.isCompleted = !this.task.isCompleted;      
    }

    submitForm() {        
        this.taskForm.markAllAsTouched();
        if (this.taskForm.valid) {
           if (this.action === 'edit') {
               this.updateTask();
           } else {
               this.createTask();
           }
        }
    }

    private updateTask() {
        const taskDetails = this.getTaskDetails();        
        taskDetails.id = this.event.id.toString();
        const partialTask = [
            {  "value": taskDetails.title, "path": "/title", "op": "replace" },
            {  "value": taskDetails.description, "path": "/description", "op": "replace" },
            {  "value": taskDetails.date, "path": "/date", "op": "replace" },
            {  "value": taskDetails.isCompleted, "path": "/isCompleted", "op": "replace" }
        ]
        this.subscription.add(this.taskService.updateTaskPartially(taskDetails.id, partialTask)
        .subscribe(res => {
            this.snackbarService.show('Task updated successfully!');
            this.customEventService.RefreshTaskListEvent.next();
            const updatedTask = Object.assign({}, this.taskForm.getRawValue());
            updatedTask.isCompleted = taskDetails.isCompleted;
            updatedTask.id = taskDetails.id;

            this.newValue = {
                details: {
                    with: taskDetails,
                },
            };
            const historyTagData = FuseUtils.getDifference(
                this.oldValue,
                this.newValue
            );

            if (historyTagData) {
                const history = this.initHistoryDataForUpdate(historyTagData);
                this.createHistory(history);
            }   
            
            this.matDialogRef.close(updatedTask);         
        }));
    }

    private createTask() {
        const taskDetails = this.getTaskDetails();
        this.subscription.add(this.taskService.createTask(taskDetails)
        .subscribe(res => {
            this.snackbarService.show('Task created successfully!');    
            this.customEventService.RefreshTaskListEvent.next(); 
            const newTask = Object.assign({}, this.taskForm.getRawValue()); 
            newTask.id = res.id;
            newTask.isCompleted = taskDetails.isCompleted;

            const historyToCreate = this.initHistoryDataForCreate();

            this.createHistory(historyToCreate);

            this.matDialogRef.close(newTask);
        }));
    }

    private initTaskForm(): void {
        if (this.action === 'edit') {            
            this.taskForm = this._formBuilder.group({         
                title: [this.event.title, [Validators.required, Validators.maxLength(250)]],
                description: [this.event.meta.notes],
                date: [this.event.start]
            });
            // this.task.isCompleted = this.event.isCompleted;
            this.oldValue = {        
                details: {          
                  with: this.openedTask
                }
              };  

        } else {
            this.taskForm = this._formBuilder.group({         
                title: ['', [Validators.required, Validators.maxLength(250)]],
                description: [''],
                date: [null]
            });
        }
          
    }  

    private getTaskDetails(): Task {
        return {
            id: '',
            title: this.taskForm.get('title').value,
            description: this.taskForm.get('description').value,
            date: this.datePipe.transform(this.taskForm.get('date').value?.toString(), 'yyyy-MM-dd'),
            isCompleted: this.task.isCompleted,
            isMarked: this.task.isMarked,
            isImportant: this.task.isImportant,
            leadId: this.openedTask.leadId,
            serviceId: this.openedTask.serviceId,
            userId: this.openIdConnectService.user.profile.sub
        }
    }

    private initHistoryDataForCreate(): HistoryData {
        const history = new HistoryData();
        history.leadId = null;
        history.serviceId = null,
        history.activity = HistoryEnum['Task Created'];
        history.userId = this.loggedInUser.profile.sub;
        return history;
    }

    private createHistory(historyToCreate): void {
        this.subscription.add(this.historyService.createHistory(historyToCreate)
          .subscribe(res => {
            // this.snackbarService.show('History created successfully!');            
          }, err => {
            console.log(err);
          }));
    }

    private initHistoryDataForUpdate(historyTagData: string): HistoryData {
        const history = new HistoryData();
        history.leadId = this.openedTask.leadId;
        history.serviceId = this.openedTask.serviceId,
        history.activity = HistoryEnum['Task Updated'];
        history.userId = this.loggedInUser.profile.sub;
        history.tagData = historyTagData;
        return history;
      }

    ngOnDestroy(): void {
        this.subscription.unsubscribe();
    }
}
