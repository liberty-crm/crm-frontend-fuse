import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { FilterOptionModel } from 'app/shared/models/filter-option.model';
@Injectable({
    providedIn: 'root'
  })
  export class InvoiceService {
      
      Url = environment.apiUrl;
  
      constructor(private http: HttpClient) { 
  
      }

      getInvoiceList(filter: FilterOptionModel): Observable<any> {
        const params = `PageNumber=${filter.pageNumber}&PageSize=${filter.pageSize}&SearchQuery=${filter.searchQuery}&OrderBy=${filter.orderBy}&Fields=${filter.fields}`;
        return this.http.get<any>(`${this.Url}serviceofferedinvoices?${params}`, { observe: 'response' });
      }

      getInvoice(leadServiceOfferedId) {
        return this.http.get<any>(`${this.Url}leadserviceoffereds/GetInvoice?leadServiceOfferedId=${leadServiceOfferedId}`);
      }
       
    }