import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { InvoiceService } from '../services/invoice.service';
import { Pagination } from 'app/shared/models/pagination';
import { FilterOptionModel } from 'app/shared/models/filter-option.model';
import { InvoiceListModel } from '../models/invoice-list.model';

@Component({
    selector: 'app-invoice-list',
    templateUrl: './invoice-list.component.html',
    styleUrls: ['./invoice-list.component.scss']
})
export class InvoiceListComponent implements OnInit, OnDestroy {

    private subscription: Subscription = new Subscription();
    @Input() leadServiceOfferedId: string;
    paginationInfo: Pagination;
    filterOptionModel: FilterOptionModel;
    loadingIndicator: boolean;
    reorderable: boolean;
    rows: InvoiceListModel[];
    constructor(private invoiceService: InvoiceService) {
        this.reorderable = true;
    }


    ngOnInit(): void {
        this.initFilterOptionModel();
        this.getInvoices();
    }

    getNextPage(pageInfo) {
        this.filterOptionModel.pageNumber = pageInfo.offset + 1;
        this.getInvoices();
    }

    invoiceRowSelect(event): any {
        if (event.type === 'click') {
            // this.router.navigate([`lead/contacts/${this.leadId}/edit/${event.row.id}`]);
        }
    }

    downloadInvoice(invoice: InvoiceListModel) {
        saveAs(invoice.attachmentPath, `${invoice.invoiceNumber}.pdf`);        
    }

    private getInvoices(): void {
        this.loadingIndicator = true;
        this.invoiceService.getInvoiceList(this.filterOptionModel).subscribe(res => {
            this.paginationInfo = JSON.parse(res.headers.get('X-Pagination'));
            this.paginationInfo.currentPage = this.paginationInfo.currentPage - 1;
            this.rows = res.body;
            this.loadingIndicator = false;
        });
    }

    private initFilterOptionModel() {
       let searchQuery = 'LeadServiceOfferedId=="' + this.leadServiceOfferedId + '"';
       this.filterOptionModel = new FilterOptionModel({ searchQuery })
    }

    ngOnDestroy(): void {
        this.subscription.unsubscribe();
    }

}
