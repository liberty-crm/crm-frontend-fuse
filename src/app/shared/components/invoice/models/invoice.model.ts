export class Invoice {
    invoiceNumber: string;
    issueDate: string | Date;
    dueDate: string | Date;
    from: InvoiceFrom;
    client: InvoiceClient;
    invoiceService: InvoiceServiceModel;
    hstAmount: number;
    totalAmount: number;
}


export class InvoiceFrom {
    title: string;
    address: string;
    phone: string;
    email: string;
    website: string;
}

export class InvoiceClient {
    title: string;
    address: string;
    phone: string;
    email: string;
    provinceDetails: ProvinceModel;
}

export class InvoiceServiceModel {
    title: string;
    description: string;
    qty: number;
    rate: number;
    total: number;
}

export class ProvinceModel {
    id: string;
    name: string;
    hstPercent: string;
    abbreviation: string;
}