export class InvoiceListModel {
    id: string;
    invoiceNumber: number;
    leadServiceOfferedId: string;
    amount: number;
    sendDate: Date | string;
    attachmentPath: string;
    createdOn: Date | string;
    updatedOn: Date | string;
    createdBy: string;
}