import { Component, OnInit, ViewEncapsulation, Input, OnDestroy, ElementRef, ViewChild } from '@angular/core';
import { takeUntil } from 'rxjs/operators';
import { Subject, Observable, Subscription } from 'rxjs';
import { Invoice } from './models/invoice.model';
import { InvoiceService } from './services/invoice.service';
import * as jsPDF from 'jspdf';
import { saveAs } from 'file-saver';
import html2canvas from 'html2canvas';


@Component({
  selector: 'app-invoice',
  templateUrl: './invoice.component.html',
  styleUrls: ['./invoice.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class InvoiceComponent implements OnInit, OnDestroy {

    private subscription: Subscription = new Subscription();
    invoice: Invoice;
    isInvoiceLoaded = false;
    @Input() leadServiceOfferedId: string;
    @ViewChild('invoiceElem') invoiceElem: ElementRef;
   
    constructor(private invoiceService: InvoiceService) { }
   
    ngOnInit(): void {
       this.getInvoice();
    }

    private getInvoice() {
        
        this.subscription.add(this.invoiceService.getInvoice(this.leadServiceOfferedId)
        .subscribe(res => {
            this.invoice = res;
            this.isInvoiceLoaded = true;
        }))
    }

    downloadInvoiceAsPdf() {       
        
        const invoiceElm = document.getElementById('invoice');
        html2canvas(invoiceElm).then(canvas => {  
        // Few necessary setting options  
        var imgWidth = 208;   
        var pageHeight = 295;    
        var imgHeight = canvas.height * imgWidth / canvas.width;  
        var heightLeft = imgHeight;  
    
        const contentDataURL = canvas.toDataURL('image/png')  
        let pdf = new jsPDF('p', 'mm', 'a4', true); // A4 size page of PDF  
        var position = 0;  
        pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight, '', 'FAST');
        pdf.save(`${this.invoice.client.title} - ${this.invoice.invoiceNumber}.pdf`); // Generated PDF   
        });  
      }

    ngOnDestroy(): void {    
        this.subscription.unsubscribe();            
    }
}
