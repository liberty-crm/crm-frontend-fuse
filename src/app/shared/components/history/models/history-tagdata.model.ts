export class HistoryTagData {
    key: string;
    oldValue: string;
    newValue: string;
    isNotification: boolean;
}