export class HistoryData {
    id: string;
    leadId: string;
    serviceId: string;
    userId: string;    
    activity: number;
    tagData: string;
}