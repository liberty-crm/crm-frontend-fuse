import { HistoryEnum } from 'app/shared/enumerations/enum';
import { HistoryTagData } from './history-tagdata.model';

export class HistoryViewModel {
    id: string;
    activity: HistoryEnum;
    tagData: HistoryTagData[];
    createdOn: string | Date;
    user: HistoryUser;
}


export class HistoryUser {
    id: string;
    fName: string;
    lName: string;
    email: string;
    userName: string;
}