import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { HistoryData } from '../models/history.model';
import { FilterOptionModel } from 'app/shared/models/filter-option.model';
import { HistoryViewModel } from '../models/history-viewmodel.model';

@Injectable({
    providedIn: 'root'
  })
  export class HistoryService {
      
      Url = environment.apiUrl;
  
      constructor(private http: HttpClient) { 
  
      }

      createHistory(history: HistoryData):Observable<HistoryData> {
        return this.http.post<HistoryData>(this.Url + 'historys', history);
      }   

      getFilteredHistory(filter: FilterOptionModel): Observable<any> {
        const params = `PageNumber=${filter.pageNumber}&PageSize=${filter.pageSize}&SearchQuery=${filter.searchQuery}&OrderBy=${filter.orderBy}&Fields=${filter.fields}`;
        return this.http.get<HistoryViewModel[]>(this.Url + `historys?${params}`, { observe: 'response' });
      }
       
    }