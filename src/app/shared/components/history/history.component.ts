import { Component, OnInit, Input } from '@angular/core';
import { Pagination } from 'app/shared/models/pagination';
import { FilterOptionModel } from 'app/shared/models/filter-option.model';
import { HistoryService } from './services/history.service';
import { HistoryViewModel } from './models/history-viewmodel.model';
import { HistoryTagData } from './models/history-tagdata.model';
import { DatePipe } from '@angular/common';
import { HistoryEnum } from 'app/shared/enumerations/enum';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss']
})
export class HistoryComponent implements OnInit {
    
  rows: HistoryViewModel[] = [];
  timelineAlt = false;
  paginationInfo: Pagination;
  filterOptionModel: FilterOptionModel; 
  @Input() leadId: string;
  @Input() serviceId: string;
  histroyEnum = HistoryEnum;
  isMoreDataAvailable: boolean;

  constructor(private historyService: HistoryService,
              private datePipe: DatePipe) { }

  ngOnInit(): void {
      this.initFilterOptionModel();
      this.getHistory();
  }

  public getHistory(): void {      
    this.historyService.getFilteredHistory(this.filterOptionModel).subscribe(res => {          
        this.paginationInfo = JSON.parse(res.headers.get('X-Pagination'));
        this.paginationInfo.currentPage = this.paginationInfo.currentPage; 
        const data = res.body;
        data.forEach(elem => {
            elem.tagData = elem.tagData ? JSON.parse(elem.tagData) : '';
            elem.createdOn = this.datePipe.transform(elem.createdOn + 'Z', 'MM/dd/yyyy');   

            if (this.histroyEnum[elem.activity] === 'Lead Updated') {
                if (elem.tagData && elem.tagData.length) {
                    elem.tagData.forEach(element => {
                        if (element.key === 'Assigned To' && !element.oldValue) {
                            element.isNotification = true;
                            element.key = `Lead assigned to ${element.newValue}`;
                        } else if (element.key === 'Assigned To' && !element.newValue) {
                            element.isNotification = true;
                            element.key = `Lead unassigned from ${element.oldValue}`;
                        }
                    });
                }                
            }
                     
        });        
        this.rows.push(...data);  

        if (this.rows.length < this.paginationInfo.totalCount) {
            this.isMoreDataAvailable = true;
        } else {
            this.isMoreDataAvailable = false;
        }
    });
  }

  loadMore() {
    this.filterOptionModel.pageNumber = this.paginationInfo.currentPage + 1;
    this.getHistory();
}

  private initFilterOptionModel() {
    let searchQuery = '';
    if (this.serviceId) {
        searchQuery = 'ServiceId=="' + this.serviceId + '"'
                                            + ' AND LeadId=="' + this.leadId + '"';
    } else {
        searchQuery = 'LeadId=="' + this.leadId + '"'
                                            + ' AND ServiceId==NULL';
    }
    this.filterOptionModel = new FilterOptionModel({searchQuery});
}

}
