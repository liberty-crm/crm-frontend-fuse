import { PipeTransform, Pipe } from '@angular/core';

@Pipe({ name: "secureText" })
export class SecureTextPipe implements PipeTransform {
  transform(value: any, digit: number = 4): any {
    if (value) {
        let str = value.toString();
        str = "*".repeat(str.length - digit) + str.toString().substr(str.length - digit);
        return str;
    }
    return value;
  }
}
