import { TestBed } from '@angular/core/testing';

import { AddAuthorizationHeaderInterceptorService } from './add-authorization-header-interceptor.service';

describe('AddAuthorizationHeaderInterceptorService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AddAuthorizationHeaderInterceptorService = TestBed.get(AddAuthorizationHeaderInterceptorService);
    expect(service).toBeTruthy();
  });
});
