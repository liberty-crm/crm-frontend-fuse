export interface Pagination {
    previousPageLink: string;
    nextPageLink: string;
    totalCount: number;
    pageSize: number;
    currentPage: number;
    totalPages: number;
}