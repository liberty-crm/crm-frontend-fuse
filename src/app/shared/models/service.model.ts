export interface Service {
    id: string;
    name: string;
    description: string; 
    code: string;
    category: string; 
}
