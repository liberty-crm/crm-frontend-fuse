export interface Asset {
    id: string;
    name: string;    
}


export interface AssetDto {
    id: string;
    type: string;
    name: string;
    valuation: string;
    description: string;
    action: string;

}