import { Store } from 'app/main/apps/settings/store/models/store.model';

export class LoggedInUserRoles {
    role: string;
    store: Store;
    onlyRole: string;
}
