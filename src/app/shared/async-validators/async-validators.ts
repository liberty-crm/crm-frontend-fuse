import { Injectable } from "@angular/core";
import { AbstractControl, AsyncValidatorFn, ValidationErrors } from "@angular/forms";
import { Observable, of } from "rxjs";
import { map, debounceTime, take, switchMap } from "rxjs/operators";
import { ContactService } from 'app/main/apps/settings/contact/services/contact.service';
import { UserService } from 'app/main/apps/settings/user/services/user.service';
import { LeadService } from 'app/main/apps/settings/lead/services/lead.service';



function isEmptyInputValue(value: any): boolean {
  // we don't check for string here so it also works with arrays
  return value === null || value.length === 0;
}

@Injectable({
  providedIn: 'root'
})
export class CustomAsyncValidator {
  constructor(private contactService: ContactService,
             private leadService: LeadService,
             private userService: UserService) {}

  leadEmailValidator(existingEmail = ''): AsyncValidatorFn {
    return (control: AbstractControl): Observable<ValidationErrors | null> => {
        if (existingEmail && existingEmail === control.value) {
            return of(null);
        } else {
            return this.contactService.isEmailExist(control.value).pipe(
                map(res => {
                  // if res is true, username exists, return true
                  return res ? { leadEmailExist: true } : null;
                  // NB: Return null if there is no error
                })
              );
        }      
    };
  }

  leadPhoneValidator(existingPhone = ''): AsyncValidatorFn {
    return (control: AbstractControl): Observable<ValidationErrors | null> => {
        if (existingPhone && existingPhone === control.value) {
            return of(null);
        } else {
            return this.contactService.isPhoneNumberExist(control.value).pipe(
                map(res => {
                  // if res is true, username exists, return true
                  return res ? { leadPhoneExist: true } : null;
                  // NB: Return null if there is no error
                })
            );
        }
    };
  }

  userEmailValidator(existingEmail = ''): AsyncValidatorFn {
    return (control: AbstractControl): Observable<ValidationErrors | null> => {
        if (existingEmail && existingEmail === control.value) {
            return of(null);
        } else {
            return this.userService.isEmailExist(control.value).pipe(
                map(res => {
                  // if res is true, username exists, return true
                  return res ? { userEmailExist: true } : null;
                  // NB: Return null if there is no error
                })
              );
        }
    };
  }

  userPhoneValidator(existingPhone = ''): AsyncValidatorFn {
    return (control: AbstractControl): Observable<ValidationErrors | null> => {
        if (existingPhone && existingPhone === control.value) {
            return of(null);
        } else {
            return this.userService.isPhoneNumberExist(control.value).pipe(
                map(res => {
                  // if res is true, username exists, return true
                  return res ? { userPhoneExist: true } : null;
                  // NB: Return null if there is no error
                })
            );
        }
    };
  }

  sinValidator(leadId: string): AsyncValidatorFn {
    return (control: AbstractControl): Observable<ValidationErrors | null> => {
        return this.leadService.isSinExist(leadId, control.value).pipe(
            map(res => {
              // if res is true, username exists, return true
              return res ? { sinExist: true } : null;
              // NB: Return null if there is no error
            })
        );
    };
  }
}
