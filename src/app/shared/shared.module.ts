import { CalendarComponent } from './components/calendar/calendar.component';
import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { TodoDetailsComponent } from './components/todo/todo-details/todo-details.component';
import { TodoListComponent } from './components/todo/todo-list/todo-list.component';
import { TodoListItemComponent } from './components/todo/todo-list/todo-list-item/todo-list-item.component';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatRippleModule } from '@angular/material/core';
import { MatSelectModule } from '@angular/material/select';
import { NgxDnDModule } from '@swimlane/ngx-dnd';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { FuseSharedModule } from '@fuse/shared.module';
import { ScrumboardBoardComponent } from './components/scrumboard/board/board.component';
import { ScrumboardBoardListComponent } from './components/scrumboard/board/list/list.component';
import { ScrumboardBoardCardComponent } from './components/scrumboard/board/list/card/card.component';
import { ScrumboardBoardEditListNameComponent } from './components/scrumboard/board/list/edit-list-name/edit-list-name.component';
import { ScrumboardBoardAddCardComponent } from './components/scrumboard/board/list/add-card/add-card.component';
import { ScrumboardBoardAddListComponent } from './components/scrumboard/board/add-list/add-list.component';
import { ScrumboardCardDialogComponent } from './components/scrumboard/board/dialogs/card/card.component';
import { ScrumboardLabelSelectorComponent } from './components/scrumboard/board/dialogs/card/label-selector/label-selector.component';
import { ScrumboardEditBoardNameComponent } from './components/scrumboard/board/edit-board-name/edit-board-name.component';
import { ScrumboardBoardSettingsSidenavComponent } from './components/scrumboard/board/sidenavs/settings/settings.component';
import { ScrumboardBoardColorSelectorComponent } from './components/scrumboard/board/sidenavs/settings/board-color-selector/board-color-selector.component';
import { MatChipsModule } from '@angular/material/chips';
import { MatDialogModule } from '@angular/material/dialog';
import { MatListModule } from '@angular/material/list';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { FuseConfirmDialogModule, FuseMaterialColorPickerModule } from '@fuse/components';
import { ScrumboardService, BoardResolve } from './components/scrumboard/scrumboard.service';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { CalendarEventFormDialogComponent } from './components/calendar/event-form/event-form.component';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { ColorPickerModule } from 'ngx-color-picker';
import { CalendarModule as AngularCalendarModule, DateAdapter } from 'angular-calendar';
import { CalendarService } from './components/calendar/calendar.service';
import { TaskComponent } from './components/task/task.component';
import { TaskListComponent } from './components/task/task-list/task-list.component';
import { TaskListItemComponent } from './components/task/task-list/task-list-item/task-list-item.component';
import { CreateTaskComponent } from './components/task/create-task/create-task.component';
import { AttachmentComponent } from './components/attachment/attachment.component';
import { MatTableModule } from '@angular/material/table';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { AlertDialogComponent } from './components/alert-dialog/alert-dialog.component';
import { SecureTextPipe } from './pipes/secure-text.pipe';
import { HistoryComponent } from './components/history/history.component';
import {MatCardModule} from '@angular/material/card';
import { ConfirmDialogComponent } from './components/confirm-dialog/confirm-dialog.component';
import { InvoiceComponent } from './components/invoice/invoice.component';
import {NgxPrintModule} from 'ngx-print';
import { PaymentComponent } from './components/payment/payment.component';
import { CreatePaymentComponent } from './components/payment/create-payment/create-payment.component';
import { EditPaymentComponent } from './components/payment/edit-payment/edit-payment.component';
import { InvoiceListComponent } from './components/invoice/invoice-list/invoice-list.component';

@NgModule({
  declarations: [
    TodoListComponent, 
    TodoDetailsComponent, 
    TodoListItemComponent,
    ScrumboardBoardComponent,
    ScrumboardBoardListComponent,
    ScrumboardBoardCardComponent,
    ScrumboardBoardEditListNameComponent,
    ScrumboardBoardAddCardComponent,
    ScrumboardBoardAddListComponent,
    ScrumboardCardDialogComponent,
    ScrumboardLabelSelectorComponent,
    ScrumboardEditBoardNameComponent,
    ScrumboardBoardSettingsSidenavComponent,
    ScrumboardBoardColorSelectorComponent,
    CalendarComponent,
    CalendarEventFormDialogComponent,
    TaskComponent,
    TaskListComponent,
    TaskListItemComponent,
    CreateTaskComponent,
    AttachmentComponent,
    AlertDialogComponent,
    SecureTextPipe,
    HistoryComponent,
    ConfirmDialogComponent,
    InvoiceComponent,
    PaymentComponent,
    CreatePaymentComponent,
    EditPaymentComponent,
    InvoiceListComponent,
  ],
  imports: [
    CommonModule,

    MatButtonModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatMenuModule,
    MatRippleModule,
    MatSelectModule,
    NgxDnDModule,
    FuseSharedModule,
   
    MatChipsModule,
    MatDialogModule,
    MatListModule,
    MatProgressBarModule,
    MatSidenavModule,
    MatToolbarModule,
    MatTooltipModule, 
    FuseConfirmDialogModule,
    FuseMaterialColorPickerModule,
    MatSlideToggleModule,
    MatTableModule,
    NgxDatatableModule,
    MatCardModule,
    NgxPrintModule,

    AngularCalendarModule.forRoot({
        provide   : DateAdapter,
        useFactory: adapterFactory
    }),
    ColorPickerModule,    
  ],
  exports: [
    TodoListComponent,
    TodoDetailsComponent,
    TodoListItemComponent,
    MatButtonModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatMenuModule,
    MatRippleModule,
    MatSelectModule,
    NgxDnDModule,
    ScrumboardBoardComponent,
    ScrumboardBoardListComponent,
    ScrumboardBoardCardComponent,
    ScrumboardBoardEditListNameComponent,
    ScrumboardBoardAddCardComponent,
    ScrumboardBoardAddListComponent,
    ScrumboardCardDialogComponent,
    ScrumboardLabelSelectorComponent,
    ScrumboardEditBoardNameComponent,
    ScrumboardBoardSettingsSidenavComponent,
    ScrumboardBoardColorSelectorComponent,

    MatChipsModule,
    MatDialogModule,
    MatListModule,
    MatProgressBarModule,
    MatSidenavModule,
    MatToolbarModule,
    MatTooltipModule, 
    FuseConfirmDialogModule,
    FuseMaterialColorPickerModule,
    MatSlideToggleModule,
    MatTableModule,
    NgxDatatableModule,

    CalendarComponent,
    CalendarEventFormDialogComponent,
    TaskComponent,
    TaskListComponent,
    TaskListItemComponent,
    AttachmentComponent,
    AlertDialogComponent,
    SecureTextPipe,
    HistoryComponent,
    ConfirmDialogComponent,
    InvoiceComponent,
    PaymentComponent,
    InvoiceListComponent,
  ],
  providers      : [
    ScrumboardService,
    BoardResolve,
    CalendarService,
    DatePipe,
    SecureTextPipe
  ],
  entryComponents: [
    ScrumboardCardDialogComponent,
    CalendarEventFormDialogComponent
  ]
})
export class SharedModule { }
