import { Injectable } from '@angular/core';
import { UserManager, User } from 'oidc-client';
import { environment } from '../../../environments/environment';
import { ReplaySubject } from 'rxjs/ReplaySubject';
import { LoggedInUserRoles } from '../models/logged-in-user-roles.model';
import { CustomEventService } from './custom-event-serverice/custom-event.service';
import { LeadService } from 'app/main/apps/settings/lead/services/lead.service';
import { Store } from 'app/main/apps/settings/store/models/store.model';
import { navigation } from 'app/navigation/navigation';
import { FuseNavigationService } from '@fuse/components/navigation/navigation.service';

@Injectable()
export class OpenIdConnectService {
    private userManager: UserManager = new UserManager(
        environment.openIdConnectSettings
    );
    private currentUser: User;
    private _loggedInUserRoles: LoggedInUserRoles[] = [];
    private _selectedUserRole: LoggedInUserRoles;
    userLoaded$ = new ReplaySubject<boolean>(1);

    get userAvailable(): boolean {
        return this.currentUser != null;
    }

    get user(): User {
        return this.currentUser;
    }

    get selectedUserRole(): LoggedInUserRoles {
        return this._selectedUserRole;
    }

    get loggedInUserRoles(): LoggedInUserRoles[] {
        return this._loggedInUserRoles;
    }

    constructor(
        private _leadService: LeadService,
        private _fuseNavigationService: FuseNavigationService,
        private _customEventService: CustomEventService
    ) {
        this.userManager.clearStaleState();

        this.userManager.events.addUserLoaded((user) => {
            if (!environment.production) {
                console.log("User loaded.", user);
            }
            this.currentUser = user;
            this.userLoaded$.next(true);
            this.prepareLoggedInUserDetails();
        });
    }

    roleSelectionChanged(selectedRole: LoggedInUserRoles) {
        this._selectedUserRole = selectedRole;
        localStorage.setItem("selectedUserRole", JSON.stringify(selectedRole));
        this.prepareNavigationMenus();
    }

    triggerSignIn() {
        this.userManager.signinRedirect().then(function () {
            if (!environment.production) {
                console.log("Redirection to signin triggered.");
            }
        });
    }

    handleCallback() {
        this.userManager.signinRedirectCallback().then(function (user) {
            if (!environment.production) {
                console.log("Callback after signin handled.", user);
            }
        });
    }

    handleSilentCallback() {
        this.userManager.signinSilentCallback().then(function (user) {
            this.currentUser = user;
            if (!environment.production) {
                console.log("Callback after silent signin handled.", user);
            }
        });
    }

    triggerSignOut() {
        this.userManager.signoutRedirect().then((resp) => {
            localStorage.removeItem('selectedUserRole');
            if (!environment.production) {
                console.log('Redirection to sign out triggered.', resp);
            }
        });
    }

    private prepareLoggedInUserDetails() {
        if (this.user) {
            this._leadService.getStore().subscribe((res) => {
                const stores = res;
                this._loggedInUserRoles = [];
                if (this.user.profile.role instanceof Array) {
                    this.user.profile.role.forEach((element) => {
                        const roleWithStore = element.split("|");
                        const userRole = new LoggedInUserRoles();
                        userRole.role = roleWithStore[0];
                        if (roleWithStore[1]) {
                            userRole.store = new Store();
                            userRole.store = stores.filter(
                                (x) => x.id === roleWithStore[1]
                            )[0];
                            userRole.onlyRole = `${userRole.role}`;
                            userRole.role = `${userRole.role} - ${userRole.store.name}`;
                        }
                        this._loggedInUserRoles.push(userRole);
                    });

                    // this.selectedRole = this.loggedInUserRoles[0];
                    const selectedUserRole =
                        JSON.parse(localStorage.getItem("selectedUserRole")) ||
                        this._loggedInUserRoles[0];
                    this.roleSelectionChanged(selectedUserRole);
                } else {
                    const roleWithStore = this.user.profile.role.split("|");
                    const userRole = new LoggedInUserRoles();
                    userRole.role = roleWithStore[0];
                    if (roleWithStore[1]) {
                        userRole.store = new Store();
                        userRole.store = stores.filter(
                            (x) => x.id === roleWithStore[1]
                        )[0];
                        userRole.onlyRole = `${userRole.role}`;
                        userRole.role = `${userRole.role} - ${userRole.store.name}`;
                    }
                    const selectedUserRole =
                        JSON.parse(localStorage.getItem("selectedUserRole")) ||
                        userRole;
                    this.roleSelectionChanged(selectedUserRole);
                }
                this._customEventService.RolesLoadedEvent.next(true);
            });
        }
    }

    private prepareNavigationMenus() {
        navigation.forEach((nav) => {
            if (nav.id !== "applications") {
                nav.hidden =
                    this.selectedUserRole.role.toLowerCase() !== "system admin";
            }
        });
        this._fuseNavigationService.unregister("main");
        this._fuseNavigationService.register("main", navigation);
        // Set the main navigation as our current navigation
        this._fuseNavigationService.setCurrentNavigation("main");
    }
}
