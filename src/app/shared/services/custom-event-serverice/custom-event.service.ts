import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CustomEventService {

  SidebarToggleEvent: Subject<boolean> = new Subject();
  CardDropEvent: Subject<string> = new Subject();
  LeadTabChangeEvent: Subject<any> = new Subject();
  RolesLoadedEvent: Subject<any> = new Subject();
  RefreshTaskListEvent: Subject<any> = new Subject();
  CalendarEventUpdated: Subject<any> = new Subject();
  constructor() { }
}
