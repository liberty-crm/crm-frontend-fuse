import { Injectable } from '@angular/core';
import { BaseService } from '../base.service';
import { OpenIdConnectService } from '../open-id-connect.service';
import { LoggedInUserRoles } from 'app/shared/models/logged-in-user-roles.model';

@Injectable({
  providedIn: 'root'
})
export class SecurityService extends BaseService{  
  private _loggedInUserRoles: LoggedInUserRoles[] = [];

  get loggedInUserRoles(): LoggedInUserRoles[] {
    return this._loggedInUserRoles;
  }
  constructor(private openIdConnectService: OpenIdConnectService) { 
    super(); 
  } 

  hasClaim(claimType: string) : boolean
  {
    if (this.openIdConnectService.userAvailable != false) {
      var Claim = claimType.split(':');
     
      // The claim assigned for the logged in user for the selected db 
      var claimValue = this.openIdConnectService.selectedUserRole.onlyRole ? this.openIdConnectService.selectedUserRole.onlyRole : this.openIdConnectService.selectedUserRole.role;
     
      // Is Claim present
      var isClaimPresent : boolean = false;

      // One of the conditions in matching. 
      Claim.forEach(element => {
        // if the claim value is set to all it means that everyone has access to this element and it
        // should always be displayed. 
        if(element == claimValue)
        {
          isClaimPresent = true;
        }      
      });
    }

    return isClaimPresent;
  }
}
