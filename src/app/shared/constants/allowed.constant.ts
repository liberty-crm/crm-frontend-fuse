export const ALLOWED = {
    MAX_ATTACHMENT_SIZE: 2000000, // 2 MB
    ATTACHMENT_EXTENSIONS: ['jpg', 'png', 'doc', 'docx', 'xls', 'xlsx', 'txt', 'pdf']
  };
  