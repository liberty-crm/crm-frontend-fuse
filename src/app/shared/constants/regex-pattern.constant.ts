export const REGEX_PATTERNS = {
    EMAIL: /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/,
    PASSWORD: /^(?=.*[A-Za-z])(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/,
    PHONE: /^[+]?(\d{1,2})?[\s.-]?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/,
    ERN: /^\d{06,16}$/,
    SIN: /^[0-9]{9}$/,
    DIGIT: /^\d+$/,
    ZIP_CODE: /^[ABCEGHJ-NPRSTVXY]\d[ABCEGHJ-NPRSTV-Z][ -]?\d[ABCEGHJ-NPRSTV-Z]\d$/i,
    URL: /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/,
    ALPHABET_ONLY: /^[a-zA-Z]+$/,
    ALPHABET_WITH_SPACE: /^[a-zA-Z ]+$/,
    POSITIVE_INTEGERS: /^[1-9]\d*$/,    
    POSITIVE_INTEGER_WITH_ZERO: /^[0-9]\d*$/,
    DECIMAL_NUMBER: /^\d+(\.\d{1,2})?$/
  };
  