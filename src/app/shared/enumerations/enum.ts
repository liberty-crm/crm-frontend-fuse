export enum HistoryEnum{
    "Lead Created" = 1,
    "Lead Updated" = 2,
    "Service Added" = 3,
    "Service Updated" = 4,
    "Service Removed" = 5,
    "Contact Created" = 6,
    "Contact Updated" = 7,
    "Task Created" = 8,
    "Task Updated" = 9,
    "Task Deleted" = 10,
    "Attachment Created" = 11,
    "Attachment Downloaded" = 12,
    "Sensitive Information Mail Sent" = 13,
    "Notification Mail Sent" = 14,
    "Integration Triggered Successfully" = 15,
    "Integration Trigger Failed" = 16
}