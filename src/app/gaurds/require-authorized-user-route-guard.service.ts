import { Injectable } from '@angular/core';
import { CanActivate, Router, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { OpenIdConnectService } from 'app/shared/services/open-id-connect.service';
import { LeadService } from 'app/main/apps/settings/lead/services/lead.service';
import { map } from 'rxjs/operators';
import { FilterOptionModel } from 'app/shared/models/filter-option.model';

@Injectable({
    providedIn: 'root'
})
export class RequireAuthorizedUserRouteGuardService implements CanActivate {

    constructor(public router: Router,
                public leadService: LeadService,
                public openIdConnectService: OpenIdConnectService) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | Observable<boolean> {
        const id = route.paramMap.get('id');
        let role = this.getSelectedUserRole();               
        if (role === 'System Admin') {

           return true;

        } else {
            
            if (state.url.includes('/lead/edit')) {
                
                return this.leadService.isUserAuthorizedToAccessLead(this.getFilterQueryBySelectedUserRole(id)).pipe(map(res => {
                    if (res.length > 0 && res[0].id) {
                      return true;
                    }
                    this.router.navigate([`/error404`]);
                    return false;
                  }));
            } else if (state.url.includes('/user')
                        || state.url.includes('/asset')
                        || state.url.includes('/serviceoffered')
                        || state.url.includes('/store')
                        || state.url.includes('/product')
                        || state.url.includes('/report'))    {
                this.router.navigate([`/error404`]);
                return false;
            }       
        }
    }

    private getFilterQueryBySelectedUserRole(leadId): string {
        let filter = '';
        let role = this.getSelectedUserRole();
        switch(role) {           
          case 'Product Manager':
              filter = '(LeadServiceOffereds.Any(ServiceOffered.ServiceManagerId=="' + 
              this.openIdConnectService.user.profile.sub +'")) AND Id=="' + leadId + '"';
              break;
          case 'Product Employee':
                  filter = '(LeadServiceOffereds.Any(AssignedTo=="' + 
                  this.openIdConnectService.user.profile.sub +'")) AND Id=="' + leadId + '"';
                  break;        
          case 'Ambassador':
          case 'Store Employee':
              filter = '(AgentId=="' + this.openIdConnectService.user.profile.sub +'") AND Id=="' + leadId + '"';
              break;
              case 'Store Manager':
              case 'Store Owner':
              filter = '(AssignedStore=="' + this.openIdConnectService.selectedUserRole.store.id +'") AND Id=="' + leadId + '"';
              break;            
          default:
              filter = '(AssignedTo=="'+ this.openIdConnectService.user.profile.sub +'") AND Id=="' + leadId + '"';
        }
        return filter;
    }

    private getSelectedUserRole() {
        let role;
        if (this.openIdConnectService.selectedUserRole) {
            role = this.openIdConnectService.selectedUserRole.onlyRole??this.openIdConnectService.selectedUserRole.role;
        } else {
            if (this.openIdConnectService.user.profile.role instanceof Array) {
                role = this.openIdConnectService.user.profile.role[0].split('|')[0];                   
            } else {
                role = this.openIdConnectService.user.profile.role.split('|')[0];
            }
        }
        return role;
    }
}
