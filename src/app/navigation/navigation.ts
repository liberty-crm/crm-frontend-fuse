import { FuseNavigation } from '@fuse/types';

export const navigation: FuseNavigation[] = [
    {
        id       : 'applications',
        title    : 'Applications',
        translate: 'NAV.APPLICATIONS',
        type     : 'group',
        children : [
            // {
            //     id       : 'sample',
            //     title    : 'Sample',
            //     translate: 'NAV.SAMPLE.TITLE',
            //     type     : 'item',
            //     icon     : 'email',
            //     url      : '/sample',
            //     badge    : {
            //         title    : '25',
            //         translate: 'NAV.SAMPLE.BADGE',
            //         bg       : '#F44336',
            //         fg       : '#FFFFFF'
            //     }
            // },
            {
                id       : 'Dashboard',
                title    : 'Dashboard',
                translate: 'NAV.DASHBOARD.TITLE',
                type     : 'item',
                icon     : 'dashboard',
                url      : '/dashboard'
            },
            {
                id       : 'Lead',
                title    : 'Lead',
                translate: 'NAV.LEAD.TITLE',
                type     : 'item',
                icon     : 'business_center',
                url      : '/lead'
            }
        ]
    },
    {
        id       : 'settings',
        title    : 'Settings',
        translate: 'NAV.SETTINGS',
        type     : 'group',
        children : [           
            {
                id       : 'User',
                title    : 'User',
                translate: 'NAV.USER.TITLE',
                type     : 'item',
                icon     : 'person',
                url      : '/user'
            }, 
            {
                id       : 'Asset',
                title    : 'Asset',
                translate: 'NAV.ASSET.TITLE',
                type     : 'item',
                icon     : 'web_asset',
                url      : '/asset'
            },
            {
                id       : 'Serviceoffered',
                title    : 'Products/Services',
                translate: 'NAV.SERVICEOFFERED.TITLE',
                type     : 'item',
                icon     : 'settings_applications',
                url      : '/serviceoffered'
            },
            {
                id       : 'Store',
                title    : 'Store',
                translate: 'NAV.STORE.TITLE',
                type     : 'item',
                icon     : 'store',
                url      : '/store'
            },
            // {
            //     id       : 'Product',
            //     title    : 'Product',
            //     translate: 'NAV.PRODUCT.TITLE',
            //     type     : 'item',
            //     icon     : 'category',
            //     url      : '/product'
            // } 
            {
                id       : 'Report',
                title    : 'Reports',
                translate: 'NAV.REPORT.TITLE',
                type     : 'item',
                icon     : 'report',
                url      : '/report'
            },                                
        ]
    }
];
