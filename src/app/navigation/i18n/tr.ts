export const locale = {
    lang: 'tr',
    data: {
        'NAV': {
            'APPLICATIONS': 'Programlar',
            'SETTINGS': 'Settings',
            'SAMPLE'        : {
                'TITLE': 'Örnek',
                'BADGE': '15'
            },
            'DASHBOARD'        : {
                'TITLE': 'Dashboard',
                'BADGE': ''
            },
            'USER'        : {
                'TITLE': 'User',
                'BADGE': ''
            },
            'ASSET'        : {
                'TITLE': 'Asset',
                'BADGE': ''
            },
            'SERVICEOFFERED'        : {
                'TITLE': 'Serviceoffered',
                'BADGE': ''
            },
            'STORE'        : {
                'TITLE': 'Store',
                'BADGE': ''
            },
            'PRODUCT'        : {
                'TITLE': 'Product',
                'BADGE': ''
            },
            'Report'        : {
                'TITLE': 'Report',
                'BADGE': ''
            }
        }
    }
};
