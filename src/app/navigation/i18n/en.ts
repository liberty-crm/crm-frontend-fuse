export const locale = {
    lang: 'en',
    data: {
        'NAV': {
            'APPLICATIONS': 'Applications',
            'SETTINGS': 'Settings',
            'SAMPLE'        : {
                'TITLE': 'Sample',
                'BADGE': '25'
            },
            'DASHBOARD'        : {
                'TITLE': 'Dashboard',
                'BADGE': ''
            },
            'USER'        : {
                'TITLE': 'Users',
                'BADGE': ''
            }
            ,
            'LEAD'        : {
                'TITLE': 'Leads',
                'BADGE': ''
            },
            'ASSET'        : {
                'TITLE': 'Assets',
                'BADGE': ''
            },
            'SERVICEOFFERED'        : {
                'TITLE': 'Products/Services',
                'BADGE': ''
            },
            'STORE'        : {
                'TITLE': 'Stores',
                'BADGE': ''
            },
            'PRODUCT'        : {
                'TITLE': 'Products',
                'BADGE': ''
            },
            'REPORT'        : {
                'TITLE': 'Reports',
                'BADGE': ''
            }
        }
    }
};
