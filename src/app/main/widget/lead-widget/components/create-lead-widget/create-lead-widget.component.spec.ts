import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateLeadWidgetComponent } from './create-lead-widget.component';

describe('CreateLeadWidgetComponent', () => {
  let component: CreateLeadWidgetComponent;
  let fixture: ComponentFixture<CreateLeadWidgetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateLeadWidgetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateLeadWidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
