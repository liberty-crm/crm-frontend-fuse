import { Component, OnInit, Input, ChangeDetectorRef, ViewEncapsulation, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { AppModule } from 'app/app.module';
import { LeadWidgetService } from '../../services/lead-widget.service';
import { Subscription, forkJoin } from 'rxjs';
import { Lead } from '../../models/lead.model';
import { SnackbarService } from 'app/shared/services/snackbar/snackbar.service';
import { REGEX_PATTERNS } from 'app/shared/constants/regex-pattern.constant';
import { CustomAsyncValidator } from 'app/shared/async-validators/async-validators';
import { HistoryData } from 'app/shared/components/history/models/history.model';
import { HistoryService } from 'app/shared/components/history/services/history.service';
import { Router } from '@angular/router';
import { HistoryEnum } from 'app/shared/enumerations/enum';

@Component({
  selector: 'app-create-lead-widget',
  templateUrl: './create-lead-widget.component.html',
  styleUrls: ['./create-lead-widget.component.scss'],
  encapsulation: ViewEncapsulation.Emulated
})
export class CreateLeadWidgetComponent implements OnInit, OnDestroy {
  
  private subscription: Subscription = new Subscription();
  @Input() agentid: string;
  leadForm: FormGroup;
  isFormSubmit = false;
  provinces = [];
  createdLead: Lead; 
  loggedInUser: Oidc.User; 
  selfEmployedOrJob = [
    'Self Employed',
    'Job'
  ];
  salaryFrequencies = [
    'Weekly',
    'Bi-Weekly',
    'Monthly'
  ];
  sources = [
    'Facebook',
    'Google Search',
    'Google Ads',
    'Website',
    'Liberty Tax Stores/Others',
    'Others'
  ];
  assets = [];
  existingClient = [
    { key: 'No', value: false },
    { key: 'Yes', value: true }
  ];
  services = [];
  selectService = [];
  selectedAssets = [];

  assetLiability = [
    'Assets',
    'Liabilities'
];
provinceList = [];
serviceRows = [];  
assetsLiabilities = [];
stores = [];
isFormLoaded = false;
snackbarMessage: string;
  constructor(private ref: ChangeDetectorRef, 
              private sanitizer: DomSanitizer,              
              private leadWidgetService: LeadWidgetService,
              private snackbarService: SnackbarService,
              private customAsyncValidator: CustomAsyncValidator,
              private fb: FormBuilder,
              private historyService: HistoryService,
              private router: Router,) {
    platformBrowserDynamic().bootstrapModule(AppModule, { 
      ngZone: 'noop'
      }).catch(err => console.error(err));
   }  

  ngOnInit(): void {
    // this.initLeadForm();
    this.resolveDropdownValues();
    // this.getAssets();
  }

  private initLeadForm(): void {
    this.leadForm = this.fb.group({
      fName: ['',
       [Validators.required, Validators.pattern(REGEX_PATTERNS.ALPHABET_ONLY), Validators.maxLength(50)]],
      mName: ['',
       [Validators.pattern(REGEX_PATTERNS.ALPHABET_ONLY), Validators.maxLength(50)]],
      lName: ['',
      [Validators.required, Validators.pattern(REGEX_PATTERNS.ALPHABET_ONLY), Validators.maxLength(50)]],
      address: ['', Validators.required],
      city: ['', 
        [Validators.required, Validators.pattern(REGEX_PATTERNS.ALPHABET_WITH_SPACE), Validators.maxLength(30)]],
      province: ['Alberta', Validators.required],
      zipcode: ['',
        [Validators.required, Validators.pattern(REGEX_PATTERNS.ZIP_CODE)]],     
      email: ['',
        [Validators.required, Validators.pattern(REGEX_PATTERNS.EMAIL)],
        this.customAsyncValidator.leadEmailValidator()],  
      phoneNumber: ['',
        [Validators.required, Validators.pattern(REGEX_PATTERNS.PHONE)],
        this.customAsyncValidator.leadPhoneValidator()],         
      source: ['Website', Validators.required],
      existingClient: [false, Validators.required],     
      allowMarketingMessages: [true],
      allowUseOfPersonalInfo:[true],      
      services   : this.fb.array(this.services.map(x => !1)),
      assignedStore: ['', Validators.required],   
    });  
    this.isFormLoaded = true;
    this.initServiceGrid();
  }

  convertToValue(key: string) {
    return this.leadForm.value[key].map((x, i) => x && { id: this[key][i].id, description: '' }).filter(x => !!x);
  }
  

  submitForm(): void {
    this.isFormSubmit = true;
    this.leadForm.markAllAsTouched();
    console.log(this.leadForm);
    if (this.leadForm.valid) {
      const lead = this.getLeadsDetails();
      console.log(lead);
      this.subscription.add(this.leadWidgetService.createLead(lead)
      .subscribe(res => {
          if (res) {
              this.showSuccessMessage('Form submitted successfully!');
              this.initLeadForm();
          }
      }, err => {
        console.log(err);
      }));
    } 
  } 
 
  serviceSelected(event): void {
    if (event.target.checked) {
      this.selectService.push(event.target.value);
    } else {
      this.selectService = this.selectService.filter(x => x !== event.target.value);
    }
  }

  assetsSelected(event): void {
    if (event.target.checked) {
      this.selectedAssets.push(event.target.value);
    } else {
      this.selectedAssets = this.selectedAssets.filter(x => x !== event.target.value);
    }
  }

  private getAssets(): void {
    this.subscription.add(this.leadWidgetService.getAssets()
    .subscribe(res => {
      console.log(res);
      this.assets = res;
    }));
  }

  private resolveDropdownValues(): void {
    const services = this.leadWidgetService.getServices(this.agentid);
    const provinces = this.leadWidgetService.getFilteredProvinces();
    const stores = this.leadWidgetService.getStore();
    this.subscription.add(forkJoin([services, provinces, stores])
    .subscribe(res => {
        this.services = res[0];
        this.provinceList = res[1];
        this.stores = res[2];
        this.provinces = this.provinceList;
        this.initLeadForm();
    }));
  }

  private getLeadsDetails(): Lead {
    const lead: Lead = {
      id: '',
      firstName: this.leadForm.controls['fName'].value,
      middleName: this.leadForm.controls['mName'].value,
      lastName: this.leadForm.controls['lName'].value,
      address: this.leadForm.controls['address'].value,
      city: this.leadForm.controls['city'].value,
      province: this.leadForm.controls['province'].value,
      zipcode: this.leadForm.controls['zipcode'].value,
      email: this.leadForm.controls['email'].value,
      phone: this.leadForm.controls['phoneNumber'].value,
      source: this.leadForm.controls['source'].value,
      existingClient: this.leadForm.controls['existingClient'].value,
      employment: '',
      position: '',
      companyName: '',
      website: '',
      employmentDate: null,
      salaryFrequency: '',
      noOfChildren: 0,
      spouseOf: '',
      customerInsight: '',
      allowMarketingMessages: this.leadForm.controls['allowMarketingMessages'].value,
      allowUseOfPersonalInfo: this.leadForm.controls['allowUseOfPersonalInfo'].value,      
      agentId: this.agentid,
      status: 'new',
      assignedTo: '',
      selectedAssetId: [],
      selectedServiceId: Object.assign([], this.convertToValue('services') ),
      assignedStore:this.leadForm.controls['assignedStore'].value,
    };
    return lead;
  }

private get serviceValue() {
    return this.leadForm.get('service')  as FormArray;    
}

private initServiceGrid() {    
    this.serviceRows = [{ name: '', description: '', action: '' }];    
    // this.serviceDataSource = new MatTableDataSource<Service[]>(this.serviceRows);
}

private getProvincesList(): void {
    this.leadWidgetService.getFilteredProvinces().subscribe(res => {
      this.provinceList = res;
      this.provinces = this.provinceList;
    });
  }

addServiceRow() {    
    this.serviceRows.push({ name: '', description: '', action: '' });
    const serviceArray = <FormArray>this.leadForm.controls['service'];
    serviceArray.push(this.initServiceFormRow());
   // this.serviceDataSource = new MatTableDataSource<Service[]>(this.serviceRows);
}

removeServiceRow(elem, index) {    
    this.serviceRows = this.serviceRows.filter(x => x !== elem);
    this.removeServiceFormRow(index);
    // this.serviceDataSource = new MatTableDataSource<Service[]>(this.serviceRows);
}

addAssetsLiabilities() {    
    this.assetsLiabilities.push({ type: '', name: '', valuation: '', description: '', action: '' });
    this.addAssetsLiabilitiesFormRow();
   // this.dataSource = new MatTableDataSource<AssetDto[]>(this.assetsLiabilities);
}

removeAssetsLiabilities(elem, index) {    
    this.assetsLiabilities = this.assetsLiabilities.filter(x => x !== elem);
    this.removetAssetsLiabilitiesFormRow(index);
    // this.dataSource = new MatTableDataSource<AssetDto[]>(this.assetsLiabilities);
}

private addAssetsLiabilitiesFormRow(): void {
    const assetLiabilityArray = <FormArray>this.leadForm.controls['assetsLiability'];
    assetLiabilityArray.push(this.initAssetsLiabilitiesFormRow());
  }

private initAssetsLiabilitiesFormRow(): FormGroup {
    return this.fb.group({
        type: [null, [Validators.required]],
        id: [null, [Validators.required]],
        valuation: [null],
        description: [null]
      });
}

private removetAssetsLiabilitiesFormRow(rowIndex: number): void {
    const assetLiabilityArray = <FormArray>this.leadForm.controls['assetsLiability'];
    if (assetLiabilityArray.length > 1) {
        assetLiabilityArray.removeAt(rowIndex);
    }
}

private removeServiceFormRow(rowIndex: number): void {
    const serviceArray = <FormArray>this.leadForm.controls['service'];
    if (serviceArray.length > 1) {
        serviceArray.removeAt(rowIndex);
    }
}

private initServiceFormRow(): FormGroup {
    return this.fb.group({        
        id: [null, [Validators.required]],        
        description: [null]
      });
}

private showSuccessMessage(message: string) {
    this.snackbarMessage = message;
    const x = document.getElementById("snackbar");
    x.className = "show";
    setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
}

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

}
