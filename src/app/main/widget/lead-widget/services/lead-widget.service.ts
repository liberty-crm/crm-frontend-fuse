import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'environments/environment';
import { Observable } from 'rxjs';
import { Asset } from 'app/shared/models/asset';
import { Service } from 'app/shared/models/service.model';
import { Lead } from '../models/lead.model';
import { Store } from 'app/main/apps/settings/store/models/store.model';

@Injectable({
  providedIn: 'root'
})
export class LeadWidgetService {
  Url = environment.apiUrl;

  constructor(private http: HttpClient) { }

  getAssets(): Observable<Asset[]> {   
   
    return this.http.get<Asset[]>(this.Url + 'assets');
  }

  getStore(): Observable<Store[]> {

    return this.http.get<any>(this.Url + 'stores');
  }

  getServices(salesRepId): Observable<Service[]> {   
   
    return this.http.get<Service[]>(this.Url + 'serviceoffereds', this.getSalesRepHeader(salesRepId));
  }  

  createLead(lead: Lead): Observable<Lead> {
    return this.http.post<Lead>(`${this.Url}leads/CreateWidgetLead`, lead, this.getSalesRepHeader(lead.agentId));
  }

  getFilteredProvinces(): Observable<any> {
    return this.http.get<any>(this.Url + 'provinces');
  }


  private getSalesRepHeader(salesRepId) {
    let headers = new HttpHeaders();
    headers = headers.set('X-Sales-Rep-Id', salesRepId);
    return { headers };
  }
}
