import { TestBed } from '@angular/core/testing';

import { LeadWidgetService } from './lead-widget.service';

describe('LeadWidgetService', () => {
  let service: LeadWidgetService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LeadWidgetService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
