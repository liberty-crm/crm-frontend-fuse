import { Component, OnInit, ViewEncapsulation, Input, ChangeDetectorRef,SimpleChanges, OnChanges } from '@angular/core'; 
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { AppModule } from 'app/app.module';
import { DomSanitizer } from '@angular/platform-browser';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-test-widget',
  templateUrl: './test-widget.component.html',
  styleUrls: ['./test-widget.component.scss'],
  encapsulation: ViewEncapsulation.Emulated
})
export class TestWidgetComponent implements OnInit {

  @Input() agentid: string;
  leadForm: FormGroup;
  isFormSubmit = false;
  provinces = [
    'Alberta',
    'British Columbia',
    'Manitoba',
    'New Brunswick',
    'Newfoundland and Labrador',
    'Nova Scotia',
    'Ontario',
    'Prince Edward Island',
    'Quebec',
    'Saskatchewan',
    'Northwest Territories',
    'Nunavut',
    'Yukon'
  ];
  selfEmployedOrJob = [
    'Self Employed',
    'Job'
  ];
  salaryFrequencies = [
    'weekly',
    'bi-weekly',
    'monthly'
  ];
  assets = [
    {id: 1, name: 'Home'},
    {id: 1, name: 'Car'},
    {id: 1, name: 'Credit cards'}
  ];
  existingClient = [
    { key: 'No', value: false },
    { key: 'Yes', value: true }
  ]
  services = [
    {id: 1, name: 'Service1'},
    {id: 1, name: 'Service3'},
    {id: 1, name: 'Service3'}
  ];
  constructor(private ref: ChangeDetectorRef, 
              private sanitizer: DomSanitizer,
              private fb: FormBuilder,) {
    platformBrowserDynamic().bootstrapModule(AppModule ,{ 
      ngZone: 'noop'
      }).catch(err => console.error(err));
   }

  ngOnInit(): void {
    this.initLeadForm();
  }

  private initLeadForm(): void {
    this.leadForm = this.fb.group({
      fName: ['', Validators.required],
      mName: ['', Validators.required],
      lName: ['', Validators.required],
      address: ['', Validators.required],
      city: ['', Validators.required],
      province: ['', Validators.required],
      zipcode: ['', Validators.required],     
      email: ['', Validators.required],  
      phoneNumber: ['', Validators.required],         
      source: ['', Validators.required],
      existingClient: [false, Validators.required],
      services: ['', Validators.required],
      job: ['', Validators.required],
      position: ['', Validators.required],
      companyName: ['', Validators.required],
      website: ['', Validators.required],
      employedSince: ['', Validators.required],
      salaryFrequency: ['', Validators.required],
      existingAssests: ['', Validators.required],
      noOfChildren: [0],
      okToReceiveMessage: ['', Validators.required]
    });  
  }

  submitForm(): void {
    this.isFormSubmit = true;
    if (this.leadForm.valid) {
      alert(`Form submitted successfully with agent id ${this.agentid}`);
    } else {
      alert('Pleas fill all required fields');
    }
  }
}
