import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { HttpClient } from '@angular/common/http';
import { environment } from 'environments/environment';
import { OpenIdConnectService } from 'app/shared/services/open-id-connect.service';
import { FilterOptionModel } from 'app/shared/models/filter-option.model';
import { Store } from '../models/store.model';

@Injectable({
  providedIn: 'root'
})
export class StoreService {
  selectedStore: Store;
  storeList: Store[];
  Url = environment.apiUrl;

  constructor(private http: HttpClient, private openIdConnectService: OpenIdConnectService) {
    this.openIdConnectService = openIdConnectService;
  }

  getStore(filter: FilterOptionModel): Observable<any> {

    const params = `PageNumber=${filter.pageNumber}&PageSize=${filter.pageSize}&SearchQuery=${filter.searchQuery}&OrderBy=${filter.orderBy}&Fields=${filter.fields}`;
    return this.http.get<any>(this.Url + `stores?${params}`, { observe: 'response' });
  }

  createStore(storeSetupData: Store): Observable<Store> {
    return this.http.post<Store>(this.Url + '/stores', storeSetupData);
  }

  getStoreById(id: string): Observable<Store> {
    return this.http.get<Store>(this.Url + '/stores/' + id);
  }

  updateStore(id: string, selectedStore: any): Observable<Store> {
    return this.http.put<Store>(this.Url + '/stores/' + id, selectedStore);

  }

  deleteStoreById(id: number): Observable<Store> {
    return this.http.delete<Store>(this.Url + '/stores/' + id);
  }

  getFilteredProvinces(): Observable<any> {
    return this.http.get<any>(this.Url + 'provinces');
  }

}
