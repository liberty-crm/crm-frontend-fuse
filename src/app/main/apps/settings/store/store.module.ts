import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from './../../../../shared/shared.module';
import { Routes, RouterModule } from '@angular/router';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { FuseSharedModule } from '@fuse/shared.module';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { StoreComponent } from './commands/store/store.component';
import { CreateStoreComponent } from './commands/create-store/create-store.component';
import { EditStoreComponent } from './commands/edit-store/edit-store.component';
const routes: Routes = [
  { path: '', component: StoreComponent },
  { path: 'create', component: CreateStoreComponent },
  { path: 'edit/:id', component: EditStoreComponent },
];

@NgModule({
  declarations: [
    StoreComponent,
    CreateStoreComponent,
    EditStoreComponent
  ],
  imports: [
    CommonModule,
    FuseSharedModule,
    SharedModule,
    RouterModule.forChild(routes),
    NgxDatatableModule,
    MatButtonModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatSelectModule,
    MatCheckboxModule,
  ]
})
export class StoreModule { }
