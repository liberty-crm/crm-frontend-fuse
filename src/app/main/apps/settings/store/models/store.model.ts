export class Store {
    id: string;
    code: string;
    name: string;
    address: string;
    zipcode: string;
    city: string;
    province: string;
    country: string;
    parentdistrict: string;
}