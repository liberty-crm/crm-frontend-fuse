import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription, Subject } from 'rxjs';
import { Pagination } from 'app/shared/models/pagination';
import { FilterOptionModel } from 'app/shared/models/filter-option.model';
import { StoreService } from '../../services/store.service';
import { Store } from '../../models/store.model';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-store',
  templateUrl: './store.component.html',
  styleUrls: ['./store.component.scss']
})
export class StoreComponent implements OnInit, OnDestroy {

  private subscription: Subscription = new Subscription();
  private subject: Subject<string> = new Subject();
  rows: Store[];
  loadingIndicator: boolean;
  reorderable: boolean;
  paginationInfo: Pagination;
  filterOptionModel: FilterOptionModel;

  columns = [
    { prop: 'code', name: 'Code' },
    { prop: 'name', name: 'Name' },
    { prop: 'address', name: 'Address' },
    { prop: 'zipcode', name: 'Postal Code' },
    { prop: 'city', name: 'City' },
    { prop: 'province', name: 'Province' },
    { prop: 'country', name: 'Country' },
    { prop: 'parentdistrict', name: 'Parent District' }
  ];

  constructor(private router: Router, private storeService: StoreService) {
    this.loadingIndicator = true;
    this.reorderable = true;
    this.initFilterOptionModel();
    this.getStoreList();
  }

  ngOnInit(): void {
    this.subject.pipe(
        debounceTime(500)
      ).subscribe(searchTextValue => {
        this.searchStore(searchTextValue);
      });
  }

  search(event) {
    const searchText = event.target.value    
    this.subject.next(searchText)
  }

  searchStore(searchString) {        
    const filter=`Name.Contains("${searchString}") OR Code.Contains("${searchString}")`   
    this.filterOptionModel = new FilterOptionModel({searchQuery: filter});
    this.getStoreList();
  }

  public getStoreList(): void {
    this.storeService.getStore(this.filterOptionModel).subscribe(res => {
      this.paginationInfo = JSON.parse(res.headers.get('X-Pagination'));
      this.paginationInfo.currentPage = this.paginationInfo.currentPage - 1;
      this.rows = res.body;
      this.loadingIndicator = false;
    });
  }

  getNextPage(pageInfo) {
    this.filterOptionModel.pageNumber = pageInfo.offset + 1;
    this.storeService.getStore(this.filterOptionModel).subscribe(res => {
      this.paginationInfo = JSON.parse(res.headers.get('X-Pagination'));
      this.paginationInfo.currentPage = this.paginationInfo.currentPage - 1;
      this.rows = res.body;
      this.loadingIndicator = false;
    });

  }

  userRowSelect(event): any {
    if (event.type === 'click') {
      this.router.navigate(['/store/edit/' + event.row.id]);
    }
  }

  goToCreateStore(): void {
    this.router.navigate(['/store/create']);
  }

  private initFilterOptionModel() {
    this.filterOptionModel = new FilterOptionModel();
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

}
