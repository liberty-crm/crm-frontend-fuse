import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { SnackbarService } from 'app/shared/services/snackbar/snackbar.service';
import { Store } from '../../models/store.model';
import { StoreService } from '../../services/store.service';
import { Province } from '../../../lead/models/lead.model';
import { REGEX_PATTERNS } from 'app/shared/constants/regex-pattern.constant';

@Component({
  selector: 'app-create-store',
  templateUrl: './create-store.component.html',
  styleUrls: ['./create-store.component.scss']
})
export class CreateStoreComponent implements OnInit {

  storeForm: FormGroup;
  storeModel: Store;
  provinces = [];
  district= [
    'London',
    'Barrie',
    'Oshawa',
    'Windsor',
    'Others'
  ]
  provinceList: Province[];
  province = [];

  constructor(private translate: TranslateService,
    private fb: FormBuilder,
    private storeService: StoreService,
    private activatedRoute: ActivatedRoute,
    private snackbarService: SnackbarService,
    private router: Router) { }

  ngOnInit(): void {
    this.storeModel = new Store();
    this.initiStoreForm();
    this.getProvincesList();  
  }

  submitForm(): void {
    if (this.storeForm.valid) {
      const result: Store = Object.assign({}, this.storeForm.value);
      this.storeService.createStore(result).subscribe((res: Store) => {
        this.snackbarService.show('Store added successfully!');
        this.router.navigate(['/store']);
      });
    }
  }

  private initiStoreForm(): void {
    this.storeForm = this.fb.group({
      code: [this.storeModel.code, Validators.required],
      name: [this.storeModel.name, Validators.required],
      address: [this.storeModel.address, Validators.required],
      zipcode: [this.storeModel.zipcode, [Validators.required, Validators.pattern(REGEX_PATTERNS.ZIP_CODE)]],
      city: [this.storeModel.city, Validators.required],
      province: [this.storeModel.province, Validators.required],
      country: [this.storeModel.country, Validators.required],
      parentdistrict: [this.storeModel.parentdistrict, Validators.required],

    });
  }

  cancel() {
    this.router.navigate(['/store']);
  }

  public getProvincesList(): void {
    this.storeService.getFilteredProvinces().subscribe(res => {
      this.provinceList = res;
      this.provinces = this.provinceList;
    });
  }

}
