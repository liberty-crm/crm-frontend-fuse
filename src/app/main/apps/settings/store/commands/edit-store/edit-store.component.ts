import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { SnackbarService } from 'app/shared/services/snackbar/snackbar.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Store } from '../../models/store.model';
import { StoreService } from '../../services/store.service';
import { Province } from '../../../lead/models/lead.model';
import { REGEX_PATTERNS } from 'app/shared/constants/regex-pattern.constant';

@Component({
  selector: 'app-edit-store',
  templateUrl: './edit-store.component.html',
  styleUrls: ['./edit-store.component.scss']
})
export class EditStoreComponent implements OnInit {

  storeForm: FormGroup;
  storeModel: Store;
  provinces = [];
  district= [
    'London',
    'Barrie',
    'Oshawa',
    'Windsor',
    'Others'
  ];
  id: any;
  provinceList: Province[];
  province = [];

  constructor(private translate: TranslateService,
    private fb: FormBuilder,
    private storeService: StoreService,
    private snackbarService: SnackbarService,
    private activatedRoute: ActivatedRoute,
    private router: Router) {
    this.activatedRoute.params.subscribe(params => {
      this.id = params.id;
    });
  }

  ngOnInit(): void {
    this.storeModel = new Store();
    this.initiStoreForm();
    this.getStoreById();
    this.getProvincesList();  
  }

  submitForm(): void {
    if (this.storeForm.valid) {
      const result: Store = Object.assign(this.storeModel, this.storeForm.value);
      this.storeService.updateStore(this.id, result).subscribe((res: Store) => {
        this.snackbarService.show('Store updated successfully!');
        this.router.navigate(['/store']);
      });
    }
  }

  private initiStoreForm(): void {
    this.storeForm = this.fb.group({
      code: [this.storeModel.code, Validators.required],
      name: [this.storeModel.name, Validators.required],
      address: [this.storeModel.address, Validators.required],
      zipcode: [this.storeModel.zipcode, [Validators.required, Validators.pattern(REGEX_PATTERNS.ZIP_CODE)]],
      city: [this.storeModel.city, Validators.required],
      province: [this.storeModel.province, Validators.required],
      country: [this.storeModel.country, Validators.required],
      parentdistrict: [this.storeModel.parentdistrict, Validators.required],
    });
  }

  cancel() {
    this.router.navigate(['/store']);
  }

  private getStoreById(): void {
    this.storeService.getStoreById(this.id).subscribe(res => {
      this.storeModel = res;
      this.initiStoreForm();
    });
  }

  delete() {
    this.storeService.deleteStoreById(this.id).subscribe((res: any) => {
      this.getStoreById();
      this.snackbarService.show('Store deleted successfully!');
      this.router.navigate(['/store']);
    });
  }

  public getProvincesList(): void {
    this.storeService.getFilteredProvinces().subscribe(res => {
      this.provinceList = res;
      this.provinces = this.provinceList;
    });
  }

}
