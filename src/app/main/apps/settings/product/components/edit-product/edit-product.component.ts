import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { SnackbarService } from 'app/shared/services/snackbar/snackbar.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ProductService } from '../../services/product.service';
import { Product } from '../../models/product.model';

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.scss']
})
export class EditProductComponent implements OnInit {

  productForm: FormGroup;
  productModel: Product;
  categorys = [
    'Insurance',
    'Mortgage',
    'Tax Filing',
    '10 Year Tax Review'
  ]
  id: any;

  constructor(private translate: TranslateService,
    private fb: FormBuilder,
    private productService: ProductService,
    private snackbarService: SnackbarService,
    private activatedRoute: ActivatedRoute,
    private router: Router) {
    this.activatedRoute.params.subscribe(params => {
      this.id = params.id;
    });
  }

  ngOnInit(): void {
    this.productModel = new Product();
    this.initiProductForm();
    this.getProductById();
  }

  submitForm(): void {
    if (this.productForm.valid) {
      const result: Product = Object.assign(this.productModel, this.productForm.value);
      this.productService.updateProduct(this.id, result).subscribe((res: Product) => {
        this.snackbarService.show('Product updated successfully!');
        this.router.navigate(['/product']);
      });
    }
  }

  private initiProductForm(): void {
    this.productForm = this.fb.group({
      code: [this.productModel.code, Validators.required],
      name: [this.productModel.name, Validators.required],
      category: [this.productModel.category, Validators.required],

    });

  }

  cancel() {
    this.router.navigate(['/product']);
  }

  private getProductById(): void {
    this.productService.getProductById(this.id).subscribe(res => {
      this.productModel = res;
      this.initiProductForm();
    });
  }

  delete() {
    this.productService.deleteProductById(this.id).subscribe((res: any) => {
      this.getProductById();
      this.snackbarService.show('Product deleted successfully!');
      this.router.navigate(['/product']);
    });
  }

}
