import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { SnackbarService } from 'app/shared/services/snackbar/snackbar.service';
import { Product } from '../../models/product.model';
import { ProductService } from '../../services/product.service';

@Component({
  selector: 'app-create-product',
  templateUrl: './create-product.component.html',
  styleUrls: ['./create-product.component.scss']
})
export class CreateProductComponent implements OnInit {

  productForm: FormGroup;
  productModel: Product;
  categorys = [
    'Insurance',
    'Mortgage',
    'Tax Filing',
    '10 Year Tax Review'
  ]


  constructor(private translate: TranslateService,
    private fb: FormBuilder,
    private productService: ProductService,
    private activatedRoute: ActivatedRoute,
    private snackbarService: SnackbarService,
    private router: Router) { }

  ngOnInit(): void {
    this.productModel = new Product();
    this.initiProductForm();
  }

  submitForm(): void {
    if (this.productForm.valid) {
      const result: Product = Object.assign({}, this.productForm.value);
      this.productService.createProduct(result).subscribe((res: Product) => {
        this.snackbarService.show('Product added successfully!');
        this.router.navigate(['/product']);
      });
    }
  }

  private initiProductForm(): void {
    this.productForm = this.fb.group({
      code: [this.productModel.code, Validators.required],
      name: [this.productModel.name, Validators.required],
      category: [this.productModel.category, Validators.required],

    });
  }

  cancel() {
    this.router.navigate(['/product']);
  }


}
