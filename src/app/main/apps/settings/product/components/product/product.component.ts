import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Pagination } from 'app/shared/models/pagination';
import { FilterOptionModel } from 'app/shared/models/filter-option.model';
import { Product } from '../../models/product.model';
import { ProductService } from '../../services/product.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit, OnDestroy {

  private subscription: Subscription = new Subscription();
  rows: Product[];
  loadingIndicator: boolean;
  reorderable: boolean;
  paginationInfo: Pagination;
  filterOptionModel: FilterOptionModel;

  columns = [
    { prop: 'name', name: 'Name' },
    { prop: 'code', name: 'Code' },
    { prop: 'category', name: 'Category' }
  ];

  constructor(private router: Router, private productService: ProductService) {
    this.loadingIndicator = true;
    this.reorderable = true;
    this.initFilterOptionModel();
    this.getProductList()
  }

  ngOnInit(): void {
  }
  public getProductList(): void {
    this.productService.getProduct(this.filterOptionModel).subscribe(res => {
      this.paginationInfo = JSON.parse(res.headers.get('X-Pagination'));
      this.paginationInfo.currentPage = this.paginationInfo.currentPage - 1;
      this.rows = res.body;
      this.loadingIndicator = false;
    });
  }

  getNextPage(pageInfo) {
    this.filterOptionModel.pageNumber = pageInfo.offset + 1;
    this.productService.getProduct(this.filterOptionModel).subscribe(res => {
      this.paginationInfo = JSON.parse(res.headers.get('X-Pagination'));
      this.paginationInfo.currentPage = this.paginationInfo.currentPage - 1;
      this.rows = res.body;
      this.loadingIndicator = false;
    });

  }

  userRowSelect(event): any {
    if (event.type === 'click') {
      this.router.navigate(['/product/edit/' + event.row.id]);
    }
  }

  goToCreateProduct(): void {
    this.router.navigate(['/product/create']);
  }

  private initFilterOptionModel() {
    this.filterOptionModel = new FilterOptionModel();
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

}
