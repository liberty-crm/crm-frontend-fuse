import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { HttpClient } from '@angular/common/http';
import { environment } from 'environments/environment';
import { OpenIdConnectService } from 'app/shared/services/open-id-connect.service';
import { FilterOptionModel } from 'app/shared/models/filter-option.model';
import { Product } from '../models/product.model';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  selectedProduct: Product;
  productList: Product[];
  Url = environment.apiUrl;

  constructor(private http: HttpClient, private openIdConnectService: OpenIdConnectService) {
    this.openIdConnectService = openIdConnectService;
  }

  getProduct(filter: FilterOptionModel): Observable<any> {

    const params = `PageNumber=${filter.pageNumber}&PageSize=${filter.pageSize}&SearchQuery=${filter.searchQuery}&OrderBy=${filter.orderBy}&Fields=${filter.fields}`;
    return this.http.get<any>(this.Url + `products?${params}`, { observe: 'response' });
  }

  createProduct(productSetupData: Product): Observable<Product> {
    return this.http.post<Product>(this.Url + '/products', productSetupData);
  }

  getProductById(id: string): Observable<Product> {
    return this.http.get<Product>(this.Url + '/products/' + id);
  }

  updateProduct(id: string, selectedProduct: any): Observable<Product> {
    return this.http.put<Product>(this.Url + '/products/' + id, selectedProduct);

  }

  deleteProductById(id: number): Observable<Product> {
    return this.http.delete<Product>(this.Url + '/products/' + id);
  }

}
