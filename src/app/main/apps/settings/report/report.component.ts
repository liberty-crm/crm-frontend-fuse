import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription, Subject } from 'rxjs';
import { Pagination } from 'app/shared/models/pagination';
import { FilterOptionModel } from 'app/shared/models/filter-option.model';
import { debounceTime } from 'rxjs/operators';
import { Report } from './models/report.model';
import { ReportService } from './services/report.service';
import { DatePipe } from '@angular/common';
import * as moment from "moment";
import { FuseUtils } from '@fuse/utils';
import { DataExportService } from 'app/shared/services/data-export.service';
import { transform } from 'lodash';

@Component({
    selector: "app-report",
    templateUrl: "./report.component.html",
    styleUrls: ["./report.component.scss"],
})
export class ReportComponent implements OnInit, OnDestroy {
    private subscription: Subscription = new Subscription();
    private subject: Subject<string> = new Subject();
    rows: Report[];
    loadingIndicator: boolean;
    reorderable: boolean;
    paginationInfo: Pagination;
    filterOptionModel: FilterOptionModel;
    from: "";

    columns = [
        { prop: "name", name: "Name" },
        { prop: "number", name: "Contact Number" },
        { prop: "email", name: "Email" },
        { prop: "refundAmount", name: "Refund Amount" },
        { prop: "invoiceAmount", name: "Invoice Amount" },
        { prop: "hst", name: "HST/GST" },
        // { prop: 'invoiceDue', name: 'Invoice Due' },
        // { prop: 'paid', name: 'Paid' },
        { prop: "familyTaxCharge", name: "Family Tax Charge" },
        { prop: "stanFinal", name: "Stan Final" },
        { prop: "lfgAmount", name: "LFG Amount" },
        { prop: "lfghst", name: "LFG HST" },
        { prop: "rep", name: "Rep" },
        { prop: "repPercentage", name: "Rep Percentage" },
        { prop: "repAmount", name: "Rep Amount" },
        { prop: "repHST", name: "Rep HST" },
        { prop: "lfgFinalAmount", name: "LFG Final Amount" },
        { prop: "lfgFinalHST", name: "LFG Final HST" },
        { prop: "invoiceDate", name: "Invoice Date" },
    ];
    selectedDuration = "7days";
    private fromDate;
    private toDate;
    isCustomDate = false;
    private customFromDate;
    private customToDate;
    customDateError = '';

    constructor(
        private router: Router,
        private datePipe: DatePipe,
        private reportService: ReportService,
        private dataExportService: DataExportService,
        private activatedRoute: ActivatedRoute
    ) {
        this.loadingIndicator = true;
        this.reorderable = true;
        this.initFilterOptionModel();
    }

    ngOnInit(): void {
        const reportData = this.activatedRoute.snapshot.data.report;
        this.paginationInfo = JSON.parse(
            reportData.headers.get("X-Pagination")
        );
        this.paginationInfo.currentPage = this.paginationInfo.currentPage - 1;
        this.rows = reportData.body;
        this.prepareReportData(this.rows);
        this.loadingIndicator = false;
        this.prepareDefaultDuration();
    }

    reportRowSelect(event): any {
        if (event.type === "click") {
            this.router.navigate(["/report/edit/" + event.row.id]);
        }
    }

    getNextPage(pageInfo) {
        this.filterOptionModel.pageNumber = pageInfo.offset + 1;
        this.getReport();
        this.getLead();
        this.getLeadServiceoffered();
    }

    toDateSelected(event): void {
        this.customToDate = new Date(event.value);   
    }

    fromDateSelected(event): void {
        this.customFromDate  = new Date(event.value);             
    }

    customDateApplied(): void {
        if (!this.customFromDate && !this.customToDate) {
            this.customDateError = 'Please choose From Date and To Date';
        } else if (!this.customFromDate) {
            this.customDateError = 'Please choose From Date';
        } else if (!this.customToDate) {
            this.customDateError = 'Please choose To Date';
        } else if (this.customToDate < this.customFromDate) {
            this.customDateError = 'To Date must be greater than From Date';
        } else {
            this.customDateError = '';
            const utcFromDateTimeString = new Date(this.customFromDate).toISOString();
            const utcFromDateString = utcFromDateTimeString.substr(
                0,
                utcFromDateTimeString.indexOf('T')
            );
            this.fromDate = utcFromDateString + ' 00:00';

            const utcDateTimeString = new Date(this.customToDate).toISOString();
            const utcDateString = utcDateTimeString.substr(
                0,
                utcDateTimeString.indexOf('T')
            );
            this.toDate = utcDateString + ' 23:59';
            
            this.filterOptionModel.searchQuery = `CreatedOn >= "${this.fromDate}" AND CreatedOn <= "${this.toDate}"`;
            this.getReport();
        }        
    }

    durationSelected(event) {
        this.isCustomDate = false;
        let today = FuseUtils.getUTCDate();
        this.fromDate =
            this.datePipe.transform(FuseUtils.getUTCDate(), "yyyy-MM-dd") +
            " 00:00";
        this.toDate =
            this.datePipe.transform(FuseUtils.getUTCDate(), "yyyy-MM-dd") +
            " 23:59";
        switch (event.value) {
            case "today":
                this.customDateError = '';
                this.fromDate =
                    this.datePipe.transform(
                        FuseUtils.getUTCDate(),
                        "yyyy-MM-dd"
                    ) + " 00:00";
                this.toDate =
                    this.datePipe.transform(
                        FuseUtils.getUTCDate(),
                        "yyyy-MM-dd"
                    ) + " 23:59";
                break;
            case "yesterday":
                this.customDateError = '';
                today = FuseUtils.getUTCDate();
                let yesterday = today.setDate(today.getDate() - 1);
                this.fromDate =
                    this.datePipe.transform(yesterday, "yyyy-MM-dd") + " 00:00";
                this.toDate =
                    this.datePipe.transform(
                        FuseUtils.getUTCDate(),
                        "yyyy-MM-dd"
                    ) + " 23:59";
                break;
            case "7days":
                this.customDateError = '';
                today = FuseUtils.getUTCDate();
                let day7 = today.setDate(today.getDate() - 7);
                this.fromDate =
                    this.datePipe.transform(day7, "yyyy-MM-dd") + " 00:00";
                this.toDate =
                    this.datePipe.transform(
                        FuseUtils.getUTCDate(),
                        "yyyy-MM-dd"
                    ) + " 23:59";
                break;
            case "28days":
                this.customDateError = '';
                today = FuseUtils.getUTCDate();
                let day28 = today.setDate(today.getDate() - 28);
                this.fromDate =
                    this.datePipe.transform(day28, "yyyy-MM-dd") + " 00:00";
                this.toDate =
                    this.datePipe.transform(
                        FuseUtils.getUTCDate(),
                        "yyyy-MM-dd"
                    ) + " 23:59";
                break;
            case "90days":
                this.customDateError = '';
                today = FuseUtils.getUTCDate();
                let day90 = today.setDate(today.getDate() - 90);
                this.fromDate =
                    this.datePipe.transform(day90, "yyyy-MM-dd") + " 00:00";
                this.toDate =
                    this.datePipe.transform(
                        FuseUtils.getUTCDate(),
                        "yyyy-MM-dd"
                    ) + " 23:59";
                break;
            case "custom":
                this.isCustomDate = true;
                break;
            default:
                today = FuseUtils.getUTCDate();
                let day = today.setDate(today.getDate() - 7);
                this.fromDate =
                    this.datePipe.transform(day, "yyyy-MM-dd") + " 00:00";
                this.toDate =
                    this.datePipe.transform(
                        FuseUtils.getUTCDate(),
                        "yyyy-MM-dd"
                    ) + " 23:59";
                break;
        }

        if (!this.isCustomDate) {
            this.filterOptionModel.searchQuery = `CreatedOn >= "${this.fromDate}" AND CreatedOn <= "${this.toDate}"`;
            this.getReport();
        }
    }

    downloadReport() {
        const filterOptionModel = new FilterOptionModel();
        filterOptionModel.searchQuery = `CreatedOn >= "${this.fromDate}" AND CreatedOn <= "${this.toDate}"`;
        filterOptionModel.pageSize = 10000;
        this.reportService
            .getOperationalReports(filterOptionModel)
            .subscribe((res) => {
                const transformedData = this.getReportDataToExport(res.body);

                if (transformedData && transformedData.length > 0) {
                    this.dataExportService.exportAsExcelFile(
                        transformedData,
                        "OperationalReport"
                    );
                }
            });
    }
    

    private getReportDataToExport(data) {
        let transformedData;
        if (data && data.length > 0) {
            data.forEach((element) => {
                element.invoiceDate = this.datePipe.transform(
                    element.invoiceDate,
                    "MM-dd-yyyy"
                );
            });

            transformedData = data.map((item) => {
                return {
                    Name: item.name,
                    "Contact Number": item.number,
                    Email: item.email,
                    "Refund Amount": item.refundAmount,
                    "Invoice Amount": item.invoiceAmount,
                    "HST/GST": item.hst,
                    // 'Invoice Due': item.invoiceDue,
                    // 'Paid': item.paid,
                    "Family Tax Charge": item.familyTaxCharge,
                    "Stan Final": item.stanFinal,
                    "LFG Amount": item.lfgAmount,
                    "LFG HST": item.lfghst,
                    Rep: item.rep,
                    "Rep Percentage": item.repPercentage,
                    "Rep Amount": item.repAmount,
                    "Rep HST": item.repHST,
                    "LFG Final Amount": item.lfgFinalAmount,
                    "LFG Final HST": item.lfgFinalHST,
                    "Invoice Date": item.invoiceDate,
                };
            });
        }
        return transformedData;
    }

    private prepareDefaultDuration() {
        const today = FuseUtils.getUTCDate();
        let day = today.setDate(today.getDate() - 7);
        this.fromDate = this.datePipe.transform(day, "yyyy-MM-dd") + " 00:00";
        this.toDate = this.datePipe.transform(today, "yyyy-MM-dd") + " 23:59";
    }

    private getReport(): void {
        this.reportService
            .getOperationalReports(this.filterOptionModel)
            .subscribe((res) => {
                this.paginationInfo = JSON.parse(
                    res.headers.get("X-Pagination")
                );
                this.paginationInfo.currentPage =
                    this.paginationInfo.currentPage - 1;
                this.rows = res.body;
                this.prepareReportData(this.rows);
                this.loadingIndicator = false;
            });
    }

    private getLead() {
        this.reportService
            .getFilteredLead(this.filterOptionModel)
            .subscribe((res) => {
                // this.paginationInfo = JSON.parse(res.headers.get('X-Pagination'));
                // this.paginationInfo.currentPage = this.paginationInfo.currentPage - 1;
                this.rows = res.body;
                this.prepareReportData(this.rows);
                this.loadingIndicator = false;
            });
    }

    private getLeadServiceoffered() {
        this.reportService
            .getFilteredLeadServiceOffered(this.filterOptionModel)
            .subscribe((res) => {
                // this.paginationInfo = JSON.parse(res.headers.get('X-Pagination'));
                // this.paginationInfo.currentPage = this.paginationInfo.currentPage - 1;
                this.rows = res.body;
                this.prepareReportData(this.rows);
                this.loadingIndicator = false;
            });
    }

    private prepareReportData(data) {
        if (data && data.length > 0) {
            data.forEach((element) => {
                element.invoiceDate = this.datePipe.transform(
                    element.invoiceDate,
                    "MM-dd-yyyy"
                );
            });
        }
    }

    private initFilterOptionModel(): void {
        this.filterOptionModel = new FilterOptionModel();
    }

    ngOnDestroy(): void {
        this.subscription.unsubscribe();
    }
}
