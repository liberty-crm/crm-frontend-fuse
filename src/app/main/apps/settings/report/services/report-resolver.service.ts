import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { ReportService } from './report.service';
import { FilterOptionModel } from 'app/shared/models/filter-option.model';
import { FuseUtils } from '@fuse/utils';

@Injectable({
  providedIn: 'root'
})
export class ReportResolverService {

  constructor(
    private reportService: ReportService
  ) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {  
    const filterOptionModel = this.getFilterOptionModel();  
    return this.reportService.getOperationalReports(filterOptionModel).pipe(map(res => {
      if (res) {
        return res;
      }
      return null; // map returns value as an observable so we don't need to use of
    }), catchError((err: any) => {
      return of(null);
    }));
  }

    private getFilterOptionModel(): FilterOptionModel {
        const today = FuseUtils.getUTCDate();
        const day = today.setDate(today.getDate() - 7);
        const fromDate = this.dateToYMD(new Date(day)) + ' 00:00';
        const toDate = this.dateToYMD(FuseUtils.getUTCDate()) + ' 23:59';

        const filterOptionModel = new FilterOptionModel();        
        filterOptionModel.searchQuery = `CreatedOn >= "${fromDate}" AND CreatedOn <= "${toDate}"`;
        return filterOptionModel;
    }

    // added function as datePipe throwing cycling dependency error
    private dateToYMD(date): string {
        const d = date.getDate();
        const m = date.getMonth() + 1; // Month from 0 to 11
        const y = date.getFullYear();
        return '' + y + '-' + (m <= 9 ? '0' + m : m) + '-' + (d <= 9 ? '0' + d : d);
    }

}


