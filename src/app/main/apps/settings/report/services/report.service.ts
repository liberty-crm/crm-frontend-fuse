import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'environments/environment';
import { FilterOptionModel } from 'app/shared/models/filter-option.model';
import { FuseUtils } from '@fuse/utils';



@Injectable({
  providedIn: 'root'
})

export class ReportService {  
  Url = environment.apiUrl;

  constructor(private http: HttpClient) {    
  }

  getOperationalReports(filter: FilterOptionModel): Observable<any> {
    const params = `PageNumber=${filter.pageNumber}&PageSize=${filter.pageSize}&SearchQuery=${filter.searchQuery}&OrderBy=${filter.orderBy}&Fields=${filter.fields}`;
    return this.http.get<any>(this.Url + `reports/OperationalReport?${params}`, { observe: 'response' });
  }

  getFilteredLead(filter: FilterOptionModel): Observable<any> {
    const params = `PageNumber=${filter.pageNumber}&PageSize=${filter.pageSize}&SearchQuery=${filter.searchQuery}&OrderBy=${filter.orderBy}&Fields=${filter.fields}`;
    return this.http.get<any>(this.Url + `reports/Lead?${params}`, { observe: 'response' });
  }

  getFilteredLeadServiceOffered(filter: FilterOptionModel): Observable<any> {
    const params = `PageNumber=${filter.pageNumber}&PageSize=${filter.pageSize}&SearchQuery=${filter.searchQuery}&OrderBy=${filter.orderBy}&Fields=${filter.fields}`;
    return this.http.get<any>(this.Url + `reports/LeadServiceOffered?${params}`, { observe: 'response' });
  }
}
