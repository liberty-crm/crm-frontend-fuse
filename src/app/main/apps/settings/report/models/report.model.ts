export class Report {
    id: string;
    name: string;  
    number: string;
    email: string;
    refundAmount: string;
    invoiceAmount: string;
    hst: string;
    invoiceDue: Date;
    paid: string;
    familyTaxCharge: string;
    stanFinal: string;
    lfgAmount: string;
    lfghst: string;
    rep: string;
    repPercentage: string;
    repAmount: string;
    repHST: string;
    lfgFinalAmount: string;
    lfgFinalHST: string;
    invoiceDate: Date;
}