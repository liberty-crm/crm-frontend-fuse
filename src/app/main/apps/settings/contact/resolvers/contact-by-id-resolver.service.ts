import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { ContactService } from '../services/contact.service';
import { Contact } from '../models/contact.model';

@Injectable({
  providedIn: 'root'
})
export class ContactByIdResolverService {

  constructor(
    private contactService: ContactService,
  ) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Contact> {
    const contactId = route.params['id'];
    return this.contactService.getContactById(contactId).pipe(map(data => {
      if (data) {
        return data;
      }
      return null; // map returns value as an observable so we don't need to use of
    }), catchError((err: any) => {
      return of(null);
    }));
  }
}
