import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContactComponent } from './components/contact/contact.component';
import { CreateContactComponent } from './components/create-contact/create-contact.component';
import { EditContactComponent } from './components/edit-contact/edit-contact.component';
import { FuseSharedModule } from '@fuse/shared.module';
import { SharedModule } from 'app/shared/shared.module';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { RouterModule, Routes } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { ContactByIdResolverService } from './resolvers/contact-by-id-resolver.service';

const routes: Routes = [
    { path: '', component: ContactComponent },
    { path: 'create', component: CreateContactComponent },
    { 
        path: 'edit/:id', 
        component: EditContactComponent,
        resolve: { contact: ContactByIdResolverService }
     },
  ];

@NgModule({
  declarations: [
    ContactComponent,
    CreateContactComponent,
    EditContactComponent
  ],
  imports: [
    CommonModule,
    FuseSharedModule,
    SharedModule,
    // RouterModule.forChild(routes),
    NgxDatatableModule,
    MatButtonModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatSelectModule,
    MatCheckboxModule,
  ],
  exports: [
    ContactComponent,
    CreateContactComponent,
    EditContactComponent
  ]
})
export class ContactModule { }
