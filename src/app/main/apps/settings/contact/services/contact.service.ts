import { Injectable } from '@angular/core';
import { Contact } from '../models/contact.model';
import { environment } from 'environments/environment';
import { HttpClient } from '@angular/common/http';
import { OpenIdConnectService } from 'app/shared/services/open-id-connect.service';
import { FilterOptionModel } from 'app/shared/models/filter-option.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ContactService {

    selectedContact: Contact;
    contactList: Contact[];
    Url = environment.apiUrl;
  
  
    constructor(private http: HttpClient, private openIdConnectService: OpenIdConnectService) { 
      this.openIdConnectService = openIdConnectService;
    }

    getLeadContacts(filter: FilterOptionModel): Observable<any> {   
        const params = `PageNumber=${filter.pageNumber}&PageSize=${filter.pageSize}&SearchQuery=${filter.searchQuery}&OrderBy=${filter.orderBy}&Fields=${filter.fields}`;
        return this.http.get<any>(this.Url + `contacts?${params}`, {observe: 'response'});
      }
    
    // getLeadContacts(filter: FilterOptionModel, leadId: string): Observable<any> {   
    //   const params = `PageNumber=${filter.pageNumber}&PageSize=${filter.pageSize}&SearchQuery=${filter.searchQuery}&OrderBy=${filter.oderBy}&Fields=${filter.fields}&LeadId=${leadId}`;
    //   return this.http.get<any>(this.Url + `contacts/LeadId?${params}`, {observe: 'response'});
    // }
  
  
    createContact(contactSetupData: Contact): Observable<Contact> {
      return this.http.post<Contact>(this.Url + 'contacts', contactSetupData);
    }
  
    getContactById(id: string): Observable<Contact> {
      return this.http.get<Contact>(this.Url + 'contacts/' + id);
    }
    updateContact(id: string, selectedContact: any): Observable<Contact> {
      return this.http.put<Contact>(this.Url + 'contacts/' + id, selectedContact);
  
    }
  
    deleteContactById(id: number): Observable<Contact> {
      return null; // return this.http.delete<Contact>(this.Url + 'contacts/' + id);
    }

    getFilteredProvinces(): Observable<any> {
      return this.http.get<any>(this.Url + 'provinces');
    }

    isEmailExist(email: string): Observable<any> {
        return this.http.get<any>(`${this.Url}/contacts/IsEmailExist?email=${email}`);
    }

    isPhoneNumberExist(phone: string): Observable<any> {
        // https://stackoverflow.com/questions/53546691/preserving-plus-sign-in-urlencoded-http-post-request/53546822#53546822
        phone = encodeURIComponent(phone);
        return this.http.get<any>(`${this.Url}/contacts/IsPhoneNumberExist?phone=${phone}`);
    }
  }
