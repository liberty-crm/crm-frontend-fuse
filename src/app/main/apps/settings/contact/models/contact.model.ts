import { Lead } from '../../lead/models/lead.model';

export class Contact extends Lead {
    relatedLeadContactId: string;
    relationWithLead: string;
    status:string;
}

