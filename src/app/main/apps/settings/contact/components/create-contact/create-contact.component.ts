import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Subscription } from 'rxjs';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ContactService } from '../../services/contact.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Contact } from '../../models/contact.model';
import { SnackbarService } from 'app/shared/services/snackbar/snackbar.service';
import { REGEX_PATTERNS } from 'app/shared/constants/regex-pattern.constant';
import { HistoryData } from 'app/shared/components/history/models/history.model';
import { OpenIdConnectService } from 'app/shared/services/open-id-connect.service';
import { HistoryService } from 'app/shared/components/history/services/history.service';
import * as Oidc from 'oidc-client';
import { HistoryEnum } from 'app/shared/enumerations/enum';
import { Province } from '../../../lead/models/lead.model';
import { CustomAsyncValidator } from 'app/shared/async-validators/async-validators';

@Component({
  selector: 'app-create-contact',
  templateUrl: './create-contact.component.html',
  styleUrls: ['./create-contact.component.scss']
})
export class CreateContactComponent implements OnInit {

  private subscription: Subscription = new Subscription();
  contactForm: FormGroup;
  isFormSubmit = false;
  loggedInUser: Oidc.User;
  createdContact: Contact;
  leadId: string;
  @Output() contactCreated: EventEmitter<any> = new EventEmitter();
  provinces = [];
  provinceList: Province[];
  province = [];
  relations = [
    'Spouse', 
    'Son',
    'Daughter',
    'Father',
    'Mother',
    'Brother',
    'Sister',
    'Others'
  ];

  constructor(private fb: FormBuilder,
              private contactService: ContactService,
              private activatedRoute: ActivatedRoute,
              private snackbarService: SnackbarService,
              private router: Router,           
              private customAsyncValidator: CustomAsyncValidator,   
              private openIdConnectService: OpenIdConnectService,
              private historyService: HistoryService)
               {
                this.loggedInUser = this.openIdConnectService.user;
                this.activatedRoute.params.subscribe(params => {
                    this.leadId = params.id;
                  });

               }

  ngOnInit(): void {
      this.initContactForm();
      this.getProvincesList(); 
  }

  private initContactForm(): void {
    this.contactForm = this.fb.group({
      firstName: ['', Validators.required],
      middleName: [''],
      lastName: ['', Validators.required],
      address: [''],
      city: [''],
      province: ['', Validators.required],
      zipcode: ['', [Validators.pattern(REGEX_PATTERNS.ZIP_CODE)]],     
      email: ['', [Validators.required, Validators.pattern(REGEX_PATTERNS.EMAIL)],
                 this.customAsyncValidator.leadEmailValidator()],  
      phoneNumber: ['', [Validators.required, Validators.pattern(REGEX_PATTERNS.PHONE)],
                  this.customAsyncValidator.leadPhoneValidator()],
      relationWithLead: ['', Validators.required],
    });  
  }

  submitForm(): void {
    if (this.contactForm.valid) {
      const contact = this.getContactDetails();
      this.subscription.add(this.contactService.createContact(contact)
      .subscribe(res => {
        this.createdContact = res;
        var historyToCreate = this.initHistoryData();
         this.createHistory(historyToCreate);
        this.snackbarService.show('Contact created successfully!');
        // this.router.navigate([`/lead/contacts/${this.leadId}`]);
        this.contactCreated.emit();
      }, err => {
        console.log(err);
      }));
    }
}

private createHistory(historyToCreate) {
  this.subscription.add(this.historyService.createHistory(historyToCreate)
    .subscribe(res => {
      this.snackbarService.show('History created successfully!');
      this.router.navigate(['/lead']);
    }, err => {
      console.log(err);
    }));
}

private initHistoryData() : HistoryData {
var history = new HistoryData;
history.leadId = this.leadId;
history.activity = HistoryEnum["Contact Created"];
history.userId = this.loggedInUser.profile.sub;
return history;  
}

private getContactDetails(): Contact {
    const contact: Contact = {
      id: '',
      firstName: this.contactForm.controls['firstName'].value,
      middleName: this.contactForm.controls['middleName'].value,
      lastName: this.contactForm.controls['lastName'].value,
      address: this.contactForm.controls['address'].value,
      city: this.contactForm.controls['city'].value,
      province: this.contactForm.controls['province'].value,
      zipcode: this.contactForm.controls['zipcode'].value,
      email: this.contactForm.controls['email'].value,
      phone: this.contactForm.controls['phoneNumber'].value,
      source: '',
      status: '',
      existingClient: false,
      employment: '',
      position: '',
      companyName: '',
      website: '',
      employmentDate: new Date(),
      salaryFrequency: '',
      noOfChildren: 0,      
      customerInsight: '',
      allowMarketingMessages: false,
      allowUseOfPersonalInfo:false,
      selectedAssetId: [],
      selectedServiceId: [],
      relatedLeadContactId: this.leadId,
      relationWithLead: this.contactForm.controls['relationWithLead'].value,
      assignedTo: '',
      maritalStatus:'',
      assignedStore: '',
      sin: '',
      salaryAmount: 0,
      agentId:'',
      referredBy:'',
      leadNumber:0
    };
    return contact;
  }

cancel() {
    this.router.navigate([`/lead/contacts/${this.leadId}`]);
}

public getProvincesList(): void {
  this.contactService.getFilteredProvinces().subscribe(res => {
    this.provinceList = res;
    this.provinces = this.provinceList;
  });
}

}
