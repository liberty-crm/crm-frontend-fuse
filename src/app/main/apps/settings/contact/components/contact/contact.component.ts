import { Component, OnInit, Input } from '@angular/core';
import { Subscription } from 'rxjs';
import { Pagination } from 'app/shared/models/pagination';
import { FilterOptionModel } from 'app/shared/models/filter-option.model';
import { Router, ActivatedRoute } from '@angular/router';
import { ContactService } from '../../services/contact.service';
import { Contact } from '../../models/contact.model';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {

  private subscription: Subscription = new Subscription();
  rows: Contact[];
  loadingIndicator: boolean;
  reorderable: boolean;  
  paginationInfo: Pagination;
  filterOptionModel: FilterOptionModel; 
  leadId: string;
  isEditContact = false;
  isNewContact = false;
  selectedContactId: string;

  columns = [
    { prop: 'firstName', name: 'First Name' },
    { prop: 'middleName', name: 'Middle Name' },
    { prop: 'lastName', name: 'Last Name' },
    { prop: 'email', name: 'Email' },
    // { prop: 'relationWithLead', name: 'Relation' }    
   ];

  constructor(private router: Router,
              private activatedRoute: ActivatedRoute,
              private contactService: ContactService) { 
                this.activatedRoute.params.subscribe(params => {
                    this.leadId = params.id;
                    this.initFilterOptionModel();
                    this.getLeadContacts();
                  });
                this.loadingIndicator = true;
                this.reorderable = true;
              }

  ngOnInit(): void {
   
  }

  getNextPage(pageInfo) {
    this.filterOptionModel.pageNumber = pageInfo.offset + 1;
    this.getLeadContacts();
  }
 

  public getLeadContacts(): void { 
    this.contactService.getLeadContacts(this.filterOptionModel).subscribe(res => {  
        this.paginationInfo = JSON.parse(res.headers.get('X-Pagination'));
        this.paginationInfo.currentPage = this.paginationInfo.currentPage - 1;
        this.rows = res.body;
        this.loadingIndicator = false;                      
    });
  }

  contactRowSelect(event): any {        
    if (event.type === 'click') {   
        this.isEditContact = true;
        this.selectedContactId = event.row.id;                     
      // this.router.navigate([`lead/contacts/${this.leadId}/edit/${event.row.id}`]);
    } 
  }

  goToCreateContact(): void {
    // this.router.navigate([`lead/contacts/${this.leadId}/create`]);
    this.isNewContact = true;
  }

  private initFilterOptionModel() {
      const searchQuery = 'RelatedLeadContactId=="' + this.leadId + '"' + ' AND RelationWithLead!="' + "Self" + '"';    
    this.filterOptionModel = new FilterOptionModel({ searchQuery });
  }

  contactUpdated() {
    this.initFilterOptionModel();
    this.getLeadContacts();
    this.isEditContact = false;
  }

  contactCreated() {
    this.initFilterOptionModel();
    this.getLeadContacts();
    this.isNewContact = false;    
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

}
