import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Contact } from '../../models/contact.model';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ContactService } from '../../services/contact.service';
import { ActivatedRoute, Router } from '@angular/router';
import { SnackbarService } from 'app/shared/services/snackbar/snackbar.service';
import { Subscription } from 'rxjs';
import { REGEX_PATTERNS } from 'app/shared/constants/regex-pattern.constant';
import { KeyValueChanges, KeyValueDiffer, KeyValueDiffers } from '@angular/core';
import { OpenIdConnectService } from 'app/shared/services/open-id-connect.service';
import { FuseUtils } from '@fuse/utils';
import { HistoryData } from 'app/shared/components/history/models/history.model';
import { HistoryEnum } from 'app/shared/enumerations/enum';
import { HistoryService } from 'app/shared/components/history/services/history.service';
import { Province } from '../../../lead/models/lead.model';
import { CustomAsyncValidator } from 'app/shared/async-validators/async-validators';
import { cloneDeep } from 'lodash';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-edit-contact',
  templateUrl: './edit-contact.component.html',
  styleUrls: ['./edit-contact.component.scss']
})
export class EditContactComponent implements OnInit {

    private subscription: Subscription = new Subscription();
    private contactDiffer: KeyValueDiffer<string, any>;
    contactForm: FormGroup;
    isFormSubmit = false;
    leadId: string;
    oldValue: any;
    newValue: any;
    history: any;
    loggedInUser: Oidc.User;  
    @Input() id: string;
    @Output() contactUpdated: EventEmitter<any> = new EventEmitter();
    contactModel: Contact;
    isFormLoaded = false;
    provinces = [];
    provinceList: Province[];
    province = [];
    relations = [
        'Spouse', 
        'Son',
        'Daughter',
        'Father',
        'Mother',
        'Brother',
        'Sister',
        'Others'
    ];

    leadStatus = [
      'New',
      'Already Availed',
      'Contacted',
      'Not Interested',
      'Interested',
      'Working',
      'Processing',
      'Closed',
    ];
  
    constructor(private fb: FormBuilder,
                private contactService: ContactService,
                private activatedRoute: ActivatedRoute,
                private snackbarService: SnackbarService,                              
                private router: Router,
                private datePipe: DatePipe,
                private openIdConnectService: OpenIdConnectService,
                private customAsyncValidator: CustomAsyncValidator,
                private historyService: HistoryService) {                
                 // this.contactModel = this.activatedRoute.snapshot.data.contact;
                  this.activatedRoute.params.subscribe(params => {
                      this.leadId = params.id;
                      // this.id = params.id;
                      this.loggedInUser = this.openIdConnectService.user;
                    });
                 }
  
    ngOnInit(): void {
        // this.initContactForm();
        this.getContactById();
        this.getProvincesList();
    }
  
    private initContactForm(): void {
      this.contactForm = this.fb.group({
        firstName: [this.contactModel.firstName, Validators.required],
        middleName: [this.contactModel.middleName],
        lastName: [this.contactModel.lastName, Validators.required],
        address: [this.contactModel.address],
        city: [this.contactModel.city],
        province: [this.contactModel.province, Validators.required],
        zipcode: [this.contactModel.zipcode, [Validators.pattern(REGEX_PATTERNS.ZIP_CODE)]],     
        email: [this.contactModel.email, [Validators.required, Validators.pattern(REGEX_PATTERNS.EMAIL)],
                this.customAsyncValidator.leadEmailValidator(this.contactModel.email)],  
        phoneNumber: [this.contactModel.phone, [Validators.required, Validators.pattern(REGEX_PATTERNS.PHONE)],
                    this.customAsyncValidator.leadPhoneValidator(this.contactModel.phone)], 
        relationWithLead: [this.contactModel.relationWithLead, Validators.required],
      });  
    }
  
    submitForm(): void {
      if (this.contactForm.valid) {
        const contact = this.getContactDetails();        

        const newCopy = cloneDeep(contact);
        if (contact.employmentDate) {      
            contact.employmentDate = this.datePipe.transform(
                contact.employmentDate,
                'yyyy-MM-dd'
            );      
            newCopy.employmentDate = this.datePipe.transform(
                newCopy.employmentDate,
                'MM-dd-yyyy'
            );
        }

        this.newValue = {        
          details: {          
            with: newCopy
          }
        };
        this.subscription.add(this.contactService.updateContact(this.id, contact)
        .subscribe(res => {
          
          this.snackbarService.show('Contact updated successfully!');

          const historyTagData = FuseUtils.getDifference(this.oldValue, this.newValue);

          if (historyTagData) {
            const historyToCreate = this.initHistoryData(historyTagData);

            this.createHistory(historyToCreate);
          } else {
            this.contactUpdated.emit();
          }          
        }, err => {
          console.log(err);
        }));
      }
  }
  private createHistory(historyToCreate) {
    this.subscription.add(this.historyService.createHistory(historyToCreate)
      .subscribe(res => {
        this.snackbarService.show('History created successfully!');
        this.router.navigate(['/lead']);
        this.contactUpdated.emit();
      }, err => {
        console.log(err);
      }));
  }

  private initHistoryData(historyTagData: string): HistoryData {
    this.history = new HistoryData();
    this.history.leadId = this.leadId;
    this.history.activity = HistoryEnum['Contact Updated'];
    this.history.userId = this.loggedInUser.profile.sub;
    this.history.tagData = historyTagData;
    return this.history;
  }
  
  private getContactDetails(): Contact {
      const contact: Contact = {
        id: this.contactModel.id,
        firstName: this.contactForm.controls['firstName'].value,
        middleName: this.contactForm.controls['middleName'].value,
        lastName: this.contactForm.controls['lastName'].value,
        address: this.contactForm.controls['address'].value,
        city: this.contactForm.controls['city'].value,
        province: this.contactForm.controls['province'].value,
        zipcode: this.contactForm.controls['zipcode'].value,
        email: this.contactForm.controls['email'].value,
        phone: this.contactForm.controls['phoneNumber'].value,
        source: '',
        status: '',
        existingClient: false,
        employment: '',
        position: '',
        companyName: '',
        website: '',
        employmentDate: new Date(),
        salaryFrequency: '',
        noOfChildren: 0,      
        customerInsight: '',
        allowMarketingMessages: false,
        allowUseOfPersonalInfo:false,
        selectedAssetId: [],
        selectedServiceId: [],
        relatedLeadContactId: this.leadId,
        relationWithLead: this.contactForm.controls['relationWithLead'].value,
        assignedTo: '',
        maritalStatus: '',
        assignedStore:'',
        sin: this.contactModel.sin,
        salaryAmount: 0,
        agentId:'',
        referredBy:'',
        leadNumber:0        
      };
      return contact;
    }

    private getContactById(): void {
        this.subscription.add(this.contactService.getContactById(this.id)
        .subscribe(res => {
            res.employmentDate = this.datePipe.transform(
                res.employmentDate,
                'yyyy-MM-dd'
            );
            this.contactModel = res;           

            const oldCopy = cloneDeep(res);
            oldCopy.employmentDate = this.datePipe.transform(
                res.employmentDate,
                'MM-dd-yyyy'
            );
            this.oldValue = {
                details: {
                    with: oldCopy,
                },
            };

            this.initContactForm();
            this.isFormLoaded = true;
        }));
    }
  
  cancel() {
      this.router.navigate([`/lead/contacts/${this.leadId}`]);
    }

    public getProvincesList(): void {
      this.contactService.getFilteredProvinces().subscribe(res => {
        this.provinceList = res;
        this.provinces = this.provinceList;
      });
    }
    
}
