import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { HttpClient } from '@angular/common/http';
import { environment } from 'environments/environment';
import { FilterOptionModel } from 'app/shared/models/filter-option.model';
import { ResetPasswordModel } from '../models/reset-password.model';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  Url = environment.apiUrl;

  constructor(private http: HttpClient) {    
   }

   resetPassword(resetPasswordModel: ResetPasswordModel): Observable<any> {
    return this.http.patch<any>(this.Url + '/aspusers/ResetPassword', resetPasswordModel);
  } 

}
