import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ResetPasswordModel } from '../../models/reset-password.model';
import { ProfileService } from '../../services/profile.service';
import { Subscription } from 'rxjs';
import { SnackbarService } from 'app/shared/services/snackbar/snackbar.service';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmDialogComponent } from 'app/shared/components/confirm-dialog/confirm-dialog.component';
import { OpenIdConnectService } from 'app/shared/services/open-id-connect.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit, OnDestroy {

  private subscription: Subscription = new Subscription();
  resetPasswordForm: FormGroup;
  isPasswordMatch = false;
  serverError = '';
  constructor(private fb: FormBuilder,
              private snackbarService: SnackbarService,
              private dialog: MatDialog,
              private openIdConnectService: OpenIdConnectService,
              private profileService: ProfileService,
              private router: Router) { }
    

  ngOnInit(): void {
      this.initResetPasswordForm();
  }

  submitForm() {
    this.resetPasswordForm.markAllAsTouched();
    if (this.resetPasswordForm.valid) {
        const resetPasswordModel = new ResetPasswordModel();
        resetPasswordModel.newPassword = this.resetPasswordForm.get('password').value;
        resetPasswordModel.currentPassword = this.resetPasswordForm.get('currentPassword').value;              
        this.subscription.add(this.profileService.resetPassword(resetPasswordModel)
        .subscribe(res => {                            
            if (res.status) {
                this.serverError = '';
                this.showMessageAndLogout();
            } else {
                this.serverError = res.message;
            }
            
        }));
    }
  }

  showMessageAndLogout() {
    const dialogRef = this.dialog.open(ConfirmDialogComponent,{
        data:{
          message: 'Password changed successfully! Please login with new password.',
          buttonText: {
            ok: 'Ok',
            showCancelButton: false
          },
          isAlert: true
        }
      });     
  
      dialogRef.afterClosed().subscribe((confirmed: boolean) => {
        this.openIdConnectService.triggerSignOut();
      });
  }

  cancel() {
    this.router.navigate(['/dashboard']);
  }

  checkIsPasswordMatch() {
    if (this.resetPasswordForm.get('confirmPassword').value) {
        if (this.resetPasswordForm.get('password').value === this.resetPasswordForm.get('confirmPassword').value) {
            this.isPasswordMatch = true;
            this.resetPasswordForm.controls['confirmPassword'].setErrors(null);
        } else {
            this.isPasswordMatch = false;
            this.resetPasswordForm.controls['confirmPassword'].setErrors({'incorrect': true});
        }   
    } else {
       setTimeout(() => {
        this.isPasswordMatch = true;
       });              
    }   
}

  private initResetPasswordForm(): void {
  
    this.resetPasswordForm = this.fb.group({
        currentPassword: ['', [Validators.required]],
        password: ['', [Validators.required]],
        confirmPassword: ['', [Validators.required]]
    });
  } 

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

}
