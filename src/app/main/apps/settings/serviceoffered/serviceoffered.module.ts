import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ServiceofferedComponent } from './components/serviceoffered/serviceoffered.component';
import { EditServiceofferedComponent } from './components/edit-serviceoffered/edit-serviceoffered.component';
import { SharedModule } from './../../../../shared/shared.module';
import { Routes, RouterModule } from '@angular/router';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { FuseSharedModule } from '@fuse/shared.module';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { CreateServiceofferedComponent } from './components/create-serviceoffered/create-serviceoffered.component';


const routes: Routes = [
  { path: '', component: ServiceofferedComponent },
  { path: 'create', component: CreateServiceofferedComponent },
  { path: 'edit/:id', component: EditServiceofferedComponent },
];

@NgModule({
  declarations: [
    ServiceofferedComponent,
    CreateServiceofferedComponent,
    EditServiceofferedComponent
  ],
  imports: [
    CommonModule,
    FuseSharedModule,
    SharedModule,
    RouterModule.forChild(routes),
    NgxDatatableModule,
    MatButtonModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatSelectModule,
    MatCheckboxModule,
  ]
})
export class ServiceofferedModule { }
