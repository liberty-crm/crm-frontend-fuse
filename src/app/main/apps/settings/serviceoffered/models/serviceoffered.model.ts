export class Serviceoffered {
    id: string;
    name: string;
    code: string;
    category: string;
    serviceManagerId: string;
    invoiceGeneration: string;
    invoiceAmountCalculation: string;
    value: number;
}