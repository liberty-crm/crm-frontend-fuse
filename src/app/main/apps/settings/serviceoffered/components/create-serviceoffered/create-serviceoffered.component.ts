import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { SnackbarService } from 'app/shared/services/snackbar/snackbar.service';
import { Serviceoffered } from '../../models/serviceoffered.model';
import { ServiceofferedService } from '../../services/serviceoffered.service';
import { User } from '../../../user/models/user.model';
import { REGEX_PATTERNS } from 'app/shared/constants/regex-pattern.constant';

// interface invoiceGeneration {
//   value: string;
//   viewValue: string;
// }

@Component({
  selector: 'app-create-serviceoffered',
  templateUrl: './create-serviceoffered.component.html',
  styleUrls: ['./create-serviceoffered.component.scss']
})
export class CreateServiceofferedComponent implements OnInit {
 
  // value: invoiceGeneration["value"];
  model: any;
  hideDropDown: boolean;
  IsHiddenValue : boolean;
  show :boolean = true;
  // @Input() 
  disabled : boolean;
  role: any;
  serviceofferedForm: FormGroup;
  serviceofferedModel: Serviceoffered;
  categorys = [
    'Personal & Corporate Income Tax Preparation',
    '10 Year Tax Review',
    'Roadmap to Financial Independence',
    'Insurance Services',
    'Mortgage Services',
    'Registered Education Savings Plans ("RESPs")'   
  ]
  userList: User[];
  serviceManager = [];
  // invoicedGeneration : invoiceGeneration[] = [
 
  // { value: 'Automatic', viewValue: 'Automatic' },
  // { value: 'Manual', viewValue: 'Manual' }
  // ];
  invoicedGeneration = [
    'Automatic',
    'Manual'
   ];
  Calculation = [
    'Fixed Price',
    '% Based'
   ]
   
  constructor(private translate: TranslateService,
    private fb: FormBuilder,
    private serviceofferedService: ServiceofferedService,
    private activatedRoute: ActivatedRoute,
    private snackbarService: SnackbarService,
    private router: Router) { }

  ngOnInit(): void {
    this.serviceofferedModel = new Serviceoffered();
    this.initiServiceofferedForm();
    this.getUserList();
    // this.pleaseSpecifyReasondisable();
  }

  submitForm(): void {
    if (this.serviceofferedForm.valid) {
      const result: Serviceoffered = Object.assign({}, this.serviceofferedForm.value);
      this.serviceofferedService.createServiceoffered(result).subscribe((res: Serviceoffered) => {
        this.snackbarService.show('Service added successfully!');
        this.router.navigate(['/serviceoffered']);
      });
    }

  }

  private initiServiceofferedForm(): void {
    this.serviceofferedForm = this.fb.group({
      code: [this.serviceofferedModel.code, Validators.required],
      name: [this.serviceofferedModel.name, Validators.required],
      category: [this.serviceofferedModel.category, Validators.required],
      serviceManagerId: [this.serviceofferedModel.serviceManagerId, Validators.required],
      invoiceGeneration: [this.serviceofferedModel.invoiceGeneration, Validators.required],
      invoiceAmountCalculation: [this.serviceofferedModel.invoiceAmountCalculation, Validators.required],
      value: [this.serviceofferedModel.value, [Validators.required, Validators.pattern(REGEX_PATTERNS.POSITIVE_INTEGERS)]],
    });
  }

  cancel() {
    this.router.navigate(['/serviceoffered']);
  }

  public getUserList(): void {
      this.serviceofferedService.getAspUserByRole('Product Manager').subscribe(res => {
        this.userList = res;
        this.serviceManager = this.userList;
      });
  }

// Enable/Disable for Not to quote
// pleaseSpecifyReasondisable() {
//   this.serviceofferedForm.get('invoiceGeneration').valueChanges
//       .subscribe(selectedInvoiceGeneration => {
//           if (selectedInvoiceGeneration != 'Automatic') {
//               this.IsHiddenValue = false;
//           }
//           else {
//               this.serviceofferedForm.get('value').enable();
//               this.IsHiddenValue = true;
//           }
//       });
// }
}
