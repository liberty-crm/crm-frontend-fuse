import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateServiceofferedComponent } from './create-serviceoffered.component';

describe('CreateServiceofferedComponent', () => {
  let component: CreateServiceofferedComponent;
  let fixture: ComponentFixture<CreateServiceofferedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateServiceofferedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateServiceofferedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
