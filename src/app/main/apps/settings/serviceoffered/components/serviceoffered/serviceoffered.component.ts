import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Pagination } from 'app/shared/models/pagination';
import { FilterOptionModel } from 'app/shared/models/filter-option.model';
import { Serviceoffered } from '../../models/serviceoffered.model';
import { ServiceofferedService } from '../../services/serviceoffered.service';

@Component({
  selector: 'app-serviceoffered',
  templateUrl: './serviceoffered.component.html',
  styleUrls: ['./serviceoffered.component.scss']
})
export class ServiceofferedComponent implements OnInit, OnDestroy {

  private subscription: Subscription = new Subscription();
  rows: Serviceoffered[];
  loadingIndicator: boolean;
  reorderable: boolean;  
  paginationInfo: Pagination;
  filterOptionModel: FilterOptionModel; 

  columns = [
    { prop: 'code', name: 'Code' },
    { prop: 'name', name: 'Name' },
    { prop: 'category', name: 'Category' },
    { prop: 'user.fName', name: 'Product Manager'},
    { prop: 'invoiceGeneration', name: 'Invoice Generation'},
    { prop: 'invoiceAmountCalculation', name: 'Invoice Amount Calculation'},
    { prop: 'value', name: 'Value'}
   ];

  constructor(private router: Router, private serviceofferedService: ServiceofferedService) { 
    this.loadingIndicator = true;
    this.reorderable = true;
    this.initFilterOptionModel();
    this.getSeviceofferedList();
  }

  ngOnInit(): void {
  }

  public getSeviceofferedList(): void {
    this.serviceofferedService.getServiceoffered(this.filterOptionModel).subscribe(res => {  
        this.paginationInfo = JSON.parse(res.headers.get('X-Pagination'));
        this.paginationInfo.currentPage = this.paginationInfo.currentPage - 1; 
        this.rows = res.body;
        this.loadingIndicator = false;                      
    });
  }

  getNextPage(pageInfo) {
    this.filterOptionModel.pageNumber = pageInfo.offset + 1;
    this.serviceofferedService.getServiceoffered(this.filterOptionModel).subscribe(res => {  
        this.paginationInfo = JSON.parse(res.headers.get('X-Pagination'));
        this.paginationInfo.currentPage = this.paginationInfo.currentPage - 1; 
        this.rows = res.body;
        this.loadingIndicator = false;                      
    });
  }

  userRowSelect(event): any {        
    if (event.type === 'click') {        
      this.router.navigate(['/serviceoffered/edit/' + event.row.id]);
    } 
  }

  goToCreateSeviceoffered(): void {
    this.router.navigate(['/serviceoffered/create']);
  }

  private initFilterOptionModel() {
    this.filterOptionModel = new FilterOptionModel();
}

ngOnDestroy(): void {
  this.subscription.unsubscribe();
}


}
