import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServiceofferedComponent } from './serviceoffered.component';

describe('ServiceofferedComponent', () => {
  let component: ServiceofferedComponent;
  let fixture: ComponentFixture<ServiceofferedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServiceofferedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServiceofferedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
