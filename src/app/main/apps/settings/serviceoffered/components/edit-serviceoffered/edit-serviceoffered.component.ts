import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { SnackbarService } from 'app/shared/services/snackbar/snackbar.service';
import { Serviceoffered } from '../../models/serviceoffered.model';
import { ServiceofferedService } from '../../services/serviceoffered.service';
import { User } from '../../../user/models/user.model';
import { REGEX_PATTERNS } from 'app/shared/constants/regex-pattern.constant';

@Component({
  selector: 'app-edit-serviceoffered',
  templateUrl: './edit-serviceoffered.component.html',
  styleUrls: ['./edit-serviceoffered.component.scss']
})
export class EditServiceofferedComponent implements OnInit {

  serviceofferedForm: FormGroup;
  serviceofferedModel: Serviceoffered;
  categorys = [
    'Personal & Corporate Income Tax Preparation',
    '10 Year Tax Review',
    'Roadmap to Financial Independence',
    'Insurance Services',
    'Mortgage Services',
    'Registered Education Savings Plans ("RESPs")'
  ]
  id: any;
  userList: User[];
  serviceManager = [];
  invoicedGeneration = [
    'Automatic',
    'Manual'
   ];
   Calculation = [
     'Fixed Price',
     '% Based'
    ]

  constructor(private translate: TranslateService,
    private fb: FormBuilder,
    private serviceofferedService: ServiceofferedService,
    private snackbarService: SnackbarService,
    private activatedRoute: ActivatedRoute,
    private router: Router) {
    this.activatedRoute.params.subscribe(params => {
      this.id = params.id;
    });
  }

  ngOnInit(): void {
    this.serviceofferedModel = new Serviceoffered();
    this.initiServiceofferedForm();
    this.getServiceofferedById();
    this.getUserList();
  }

  submitForm(): void {
    if (this.serviceofferedForm.valid) {
      const result: Serviceoffered = Object.assign(this.serviceofferedModel, this.serviceofferedForm.value);
      this.serviceofferedService.updateServiceoffered(this.id, result).subscribe((res: Serviceoffered) => {
        this.snackbarService.show('Service updated successfully!');
        this.router.navigate(['/serviceoffered']);
      });
    }
  }

  private initiServiceofferedForm(): void {
    this.serviceofferedForm = this.fb.group({
      code: [this.serviceofferedModel.code, Validators.required],
      name: [this.serviceofferedModel.name, Validators.required],
      category: [this.serviceofferedModel.category, Validators.required],
      serviceManagerId: [this.serviceofferedModel.serviceManagerId, Validators.required], 
      invoiceGeneration: [this.serviceofferedModel.invoiceGeneration, Validators.required],
      invoiceAmountCalculation: [this.serviceofferedModel.invoiceAmountCalculation, Validators.required],
      value: [this.serviceofferedModel.value, [Validators.required, Validators.pattern(REGEX_PATTERNS.POSITIVE_INTEGERS)]],      
    });
  }

  cancel() {
    this.router.navigate(['/serviceoffered']);
  }

  private getServiceofferedById(): void {
    this.serviceofferedService.getServiceofferedById(this.id).subscribe(res => {
      this.serviceofferedModel = res;
      this.initiServiceofferedForm();
    });
  }
  delete() {
    this.serviceofferedService.deleteServiceofferedById(this.id).subscribe((res: any) => {
      this.getServiceofferedById();
      this.snackbarService.show('Service deleted successfully!');
      this.router.navigate(['/serviceoffered']);
    });
  }

  public getUserList(): void {
    this.serviceofferedService.getAspUserByRole('Product Manager').subscribe(res => {
      this.userList = res;
      this.serviceManager = this.userList;
    });
  }
}
