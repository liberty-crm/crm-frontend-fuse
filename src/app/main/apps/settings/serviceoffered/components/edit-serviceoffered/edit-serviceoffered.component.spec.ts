import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditServiceofferedComponent } from './edit-serviceoffered.component';

describe('EditServiceofferedComponent', () => {
  let component: EditServiceofferedComponent;
  let fixture: ComponentFixture<EditServiceofferedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditServiceofferedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditServiceofferedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
