import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { HttpClient } from '@angular/common/http';
import { environment } from 'environments/environment';
import { OpenIdConnectService } from 'app/shared/services/open-id-connect.service';
import { FilterOptionModel } from 'app/shared/models/filter-option.model';
import { Serviceoffered } from '../models/serviceoffered.model';
import { User } from '../../user/models/user.model';

@Injectable({
  providedIn: 'root'
})
export class ServiceofferedService {
  selectedServiceoffered: Serviceoffered;
  serviceofferedList: Serviceoffered[];
  Url = environment.apiUrl;

  constructor(private http: HttpClient, private openIdConnectService: OpenIdConnectService) {
    this.openIdConnectService = openIdConnectService;
  }

  createServiceoffered(ServiceofferedSetupData: Serviceoffered): Observable<Serviceoffered> {
    return this.http.post<Serviceoffered>(this.Url + '/serviceoffereds', ServiceofferedSetupData);
  }

  getServiceoffered(filter: FilterOptionModel): Observable<any> {

    const params = `PageNumber=${filter.pageNumber}&PageSize=${filter.pageSize}&SearchQuery=${filter.searchQuery}&OrderBy=${filter.orderBy}&Fields=${filter.fields}`;
    return this.http.get<any>(this.Url + `serviceoffereds?${params}`, { observe: 'response' });
  }

  getServiceofferedById(id: string): Observable<Serviceoffered> {
    return this.http.get<Serviceoffered>(this.Url + '/serviceoffereds/' + id);
  }

  updateServiceoffered(id: string, selectedServiceoffered: any): Observable<Serviceoffered> {
    return this.http.put<Serviceoffered>(this.Url + '/serviceoffereds/' + id, selectedServiceoffered);

  }

  deleteServiceofferedById(id: number): Observable<Serviceoffered> {
    return this.http.delete<Serviceoffered>(this.Url + '/serviceoffereds/' + id);
  }

  getAspUserByRole(role: string): Observable<User[]> {
    return this.http.get<any>(this.Url + 'aspusers/GetAspUserByRole/' + role);
  }

}
