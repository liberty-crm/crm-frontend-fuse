import { TestBed } from '@angular/core/testing';

import { ServiceofferedService } from './serviceoffered.service';

describe('ServiceofferedService', () => {
  let service: ServiceofferedService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ServiceofferedService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
