import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { HttpClient } from '@angular/common/http';
import { User } from '../models/user.model';
import { environment } from 'environments/environment';
import { FilterOptionModel } from 'app/shared/models/filter-option.model';
import { FuseUtils } from '@fuse/utils';

@Injectable({
    providedIn: 'root'
})

export class UserService {
    selectedUser: User;
    userList: User[];
    Url = environment.apiUrl;


    constructor(private http: HttpClient) {
    }


    getUser(filter: FilterOptionModel): Observable<any> {

        const params = `PageNumber=${filter.pageNumber}&PageSize=${filter.pageSize}&SearchQuery=${filter.searchQuery}&OrderBy=${filter.orderBy}&Fields=${filter.fields}`;
        return this.http.get<any>(this.Url + `aspusers?${params}`, { observe: 'response' });
    }

    getFilteredUsers(filter: FilterOptionModel, isSkipLoader = false): Observable<User[]> {
        const params = `PageNumber=${filter.pageNumber}&PageSize=${filter.pageSize}&SearchQuery=${filter.searchQuery}&OrderBy=${filter.orderBy}&Fields=${filter.fields}`;
        if (isSkipLoader) {
            return this.http.get<any>(`${this.Url}aspusers?${params}`, FuseUtils.getSkipHttpLoaderHeader());
        } else {
            return this.http.get<any>(`${this.Url}aspusers?${params}`);
        }    
      }

    createUser(userSetupData: User): Observable<User> {
        return this.http.post<User>(this.Url + 'aspusers/InviteUser', userSetupData);
    }

    getUserById(id: string): Observable<User> {
        return this.http.get<User>(this.Url + 'aspusers/' + id);
    }

    updateUser(id: string, selectedUser: any): Observable<User> {
        return this.http.put<User>(this.Url + 'aspusers/' + id, selectedUser);
    }

    deleteUserById(id: number): Observable<User> {
        return null; // return this.http.delete<User>(this.Url + 'users/' + id);
    }

    getFilteredProvinces(): Observable<any> {
        return this.http.get<any>(this.Url + 'provinces');
    }

    isEmailExist(email: string): Observable<any> {
        return this.http.get<any>(`${this.Url}/aspusers/IsUserEmailExist?email=${email}`);
    }

    isPhoneNumberExist(phone: string): Observable<any> {
        // https://stackoverflow.com/questions/53546691/preserving-plus-sign-in-urlencoded-http-post-request/53546822#53546822
        phone = encodeURIComponent(phone);
        return this.http.get<any>(`${this.Url}/aspusers/IsUserPhoneExist?phone=${phone}`);
    }

}
