import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { UserService } from '../../services/user.service';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { User } from '../../models/user.model';
import { SnackbarService } from 'app/shared/services/snackbar/snackbar.service';
import { REGEX_PATTERNS } from 'app/shared/constants/regex-pattern.constant';
import { Store } from '../../../store/models/store.model';
import { LeadService } from '../../../lead/services/lead.service';
import { Subscription } from 'rxjs';
import { MatTableDataSource } from '@angular/material/table';
import { UserRoleGrid } from '../../models/user-role-grid.model';
import { Province } from '../../../lead/models/lead.model';
import { CustomAsyncValidator } from 'app/shared/async-validators/async-validators';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.scss']
})
export class CreateUserComponent implements OnInit, OnDestroy {

  private subscription: Subscription = new Subscription();
  userForm: FormGroup;
  userModel: User;
  storeList: Store[] = [];
  userRoles = [    
    'Store Manager',
    'Store Employee',
    'System Admin',
    'Lead Admin',
    'Relationship Manager',
    'Product Manager',
    'Product Employee',
    'District Manager',
    'Store Owner',
    'Stackholder'
  ];
  isFormLoaded = false;
  displayedRoleColumns: string[] = ['roleName', 'storeId', 'action'];
  roleRows = [];
  roleDataSource =  new MatTableDataSource<UserRoleGrid[]>(null);
  provinceList: Province[];
  province = [];

  constructor(private translate: TranslateService,
    private fb: FormBuilder,
    private userService: UserService,
    private activatedRoute: ActivatedRoute,
    private snackbarService: SnackbarService,
    private customAsyncValidator: CustomAsyncValidator,
    private leadService: LeadService,
    private router: Router) { }    

  ngOnInit(): void {
    this.userModel = new User();
    this.getStoreList();
    this.getProvincesList();
  }

  submitForm(): void {
    if (this.userForm.valid) {
      const result: User = this.getUserFormDetails();
      this.userService.createUser(result).subscribe((res: User) => {
        this.snackbarService.show('User added successfully!');
        this.router.navigate(['/user']);
      });
    }
  }

  cancel() {
    this.router.navigate(['/user']);
  }

  showStoreDropdown(index: number) {
    const roles = Object.assign([], this.rolesValue.value);
    if (roles[index] && roles[index].roleName
        && (roles[index].roleName.toLowerCase() === 'store owner'
           || roles[index].roleName.toLowerCase() === 'store manager'
           || roles[index].roleName.toLowerCase() === 'store employee')) {

            setTimeout(() => {
                this.rolesValue.controls[index]?.get('storeId').setValidators([Validators.required]);
                this.rolesValue.controls[index]?.get('storeId').updateValueAndValidity();
            });           
            return true;
    } else {
        setTimeout(() => {
            this.rolesValue.controls[index]?.get('storeId').clearValidators();
            this.rolesValue.controls[index]?.get('storeId').updateValueAndValidity();
        });
        
        return false;
    }
  }

  addRoleRow() {    
    this.roleRows.push({ roleName: '', storeId: '', action: '' });
    const roleArray = <FormArray>this.userForm.controls['role'];
    roleArray.push(this.initRoleFormRow());
    this.roleDataSource = new MatTableDataSource<UserRoleGrid[]>(this.roleRows);    
  }

  removeRoleRow(elem, index) {    
    this.roleRows = this.roleRows.filter(x => x !== elem);
    this.removeRoleFormRow(index);
    this.roleDataSource = new MatTableDataSource<UserRoleGrid[]>(this.roleRows);
  }

  private getUserFormDetails() {
    const result = Object.assign({}, this.userForm.value);
    result.role = result.role.map(x => {
        if (x.storeId) {
            return `${x.roleName}|${x.storeId}`
        } else {
            return x.roleName;
        }        
    });
    result.role = result.role.toString(); 
    return result;
  }

  private initiUserForm(): void {
    this.userForm = this.fb.group({
      fName: [this.userModel.fName,
      [Validators.required, Validators.pattern(REGEX_PATTERNS.ALPHABET_ONLY), Validators.maxLength(50)]],
      lName: [this.userModel.lName,
      [Validators.required, Validators.pattern(REGEX_PATTERNS.ALPHABET_ONLY), Validators.maxLength(50)]],
      email: [this.userModel.email, [Validators.required, Validators.pattern(REGEX_PATTERNS.EMAIL)],
            this.customAsyncValidator.userEmailValidator()],
      // role: [this.userModel.role, Validators.required]
      middleName: [this.userModel.middleName, Validators.maxLength(50)],
      address: [this.userModel.address],
      city: [this.userModel.city],
      province: [this.userModel.province],
      postalCode: [this.userModel.postalCode, [Validators.pattern(REGEX_PATTERNS.ZIP_CODE)]],
      phoneNumber: [this.userModel.phoneNumber, [Validators.pattern(REGEX_PATTERNS.PHONE)],
        this.customAsyncValidator.userPhoneValidator()],
      referencePercentage: [0, [Validators.pattern(REGEX_PATTERNS.POSITIVE_INTEGER_WITH_ZERO), Validators.max(100)]],
      twoFactorEnabled: [false],
      isActive: [false],
      role: this.fb.array([
        this.fb.group({          
          roleName: [this.userModel.role, [Validators.required]],          
          storeId: [null]
        })
      ]),
    });
    
    this.initRoleGrid();
    this.isFormLoaded = true;

  }

  private removeRoleFormRow(rowIndex: number): void {
    const roleArray = <FormArray>this.userForm.controls['role'];
    if (roleArray.length > 1) {
        roleArray.removeAt(rowIndex);
    }
  }

  private initRoleFormRow(): FormGroup {
    return this.fb.group({        
        roleName: [null, [Validators.required]],        
        storeId: [null]
      });
  }

  private getStoreList(): void {
    this.subscription.add(this.leadService.getStore().subscribe(res => {
        this.storeList = res;     
        this.initiUserForm(); 
    }));
  }

  private get rolesValue() {
    return this.userForm.get('role')  as FormArray;    
  }

  private initRoleGrid() {    
    this.roleRows = [{ roleName: '', storeId: '', action: '' }];    
    this.roleDataSource = new MatTableDataSource<UserRoleGrid[]>(this.roleRows);
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  public getProvincesList(): void {
    this.leadService.getFilteredProvinces().subscribe(res => {
      this.provinceList = res;
    });
  }

}
