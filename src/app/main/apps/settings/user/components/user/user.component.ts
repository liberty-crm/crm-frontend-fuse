import { Component, OnInit, OnDestroy } from '@angular/core';
import { User } from '../../models/user.model';
import { UserService } from '../../services/user.service';
import { Router } from '@angular/router';
import { Subscription, Subject } from 'rxjs';
import { Pagination } from 'app/shared/models/pagination';
import { FilterOptionModel } from 'app/shared/models/filter-option.model';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit, OnDestroy {

  private subscription: Subscription = new Subscription();
  private subject: Subject<string> = new Subject();
  rows: User[];
  loadingIndicator: boolean;
  reorderable: boolean;  
  paginationInfo: Pagination;
  filterOptionModel: FilterOptionModel; 
  

  columns = [
    { prop: 'fName', name: 'First Name' },
    { prop: 'lName', name: 'Last Name' },
    { prop: 'email', name: 'Email' },
    { prop: 'role', name: 'Role' }
   ];

  constructor(private userService: UserService, 
              private router: Router) {               
      this.loadingIndicator = true;
      this.reorderable = true;
      this.initFilterOptionModel();
      this.getUserList();
  } 
    
    
   
  public ngOnInit(): void {
    this.subject.pipe(
        debounceTime(500)
      ).subscribe(searchTextValue => {
        this.searchUser(searchTextValue);
      });
  }

  public getUserList(): void {
    this.userService.getUser(this.filterOptionModel).subscribe(res => {  
        this.paginationInfo = JSON.parse(res.headers.get('X-Pagination'));
        this.paginationInfo.currentPage = this.paginationInfo.currentPage - 1; 
        this.rows = res.body;
        this.formatRoles();
        this.loadingIndicator = false;                      
    });
  }

  getNextPage(pageInfo) {
    this.filterOptionModel.pageNumber = pageInfo.offset + 1;
    this.getUserList();
  }
  
  userRowSelect(event): any {        
    if (event.type === 'click') {        
      this.router.navigate(['/user/edit/' + event.row.id]);
    } 
  }

  goToCreateUser(): void {
    this.router.navigate(['/user/create']);
  }

  search(event) {
    const searchText = event.target.value    
    this.subject.next(searchText)
  }

  searchUser(searchString) {    
    const filter=`FName.Contains("${searchString}") OR LName.Contains("${searchString}") OR Email.Contains("${searchString}")`    
    this.filterOptionModel = new FilterOptionModel({searchQuery: filter, orderBy: 'UpdatedOn desc'});
    this.getUserList();
  }

  private initFilterOptionModel() {
    this.filterOptionModel = new FilterOptionModel({ orderBy: 'UpdatedOn desc'});
  }

  private formatRoles() {
    this.rows.forEach(elem => {
        const role = elem.role.split(',');
        const roles = [];
        role.forEach(el => {
            const result = el.substr(0, el.indexOf('|'));
            if (result) {
                roles.push(result.trim());
            } else {
                roles.push(el);
            }
        });        
        elem.role = roles.toString();
    });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
