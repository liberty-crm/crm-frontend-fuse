export class User {
    id: string;
    userName: string;
    fName: string;    
    lName: string;
    email: string;
    role: string; 
    twoFactorEnabled: boolean;
    isActive: boolean;
    middleName: string;
    address: string;
    city: string;
    province: string;
    postalCode: string;
    phoneNumber: string;
    referencePercentage: string;
}
