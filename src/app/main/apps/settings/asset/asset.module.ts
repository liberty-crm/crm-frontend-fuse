import { SharedModule } from './../../../../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Routes, RouterModule } from '@angular/router';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { FuseSharedModule } from '@fuse/shared.module';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { AssetComponent } from './components/asset/asset.component';
import { CreateAssetComponent } from './components/create-asset/create-asset.component';
import { EditAssetComponent } from './components/edit-asset/edit-asset.component';

const routes: Routes = [
  { path: '', component: AssetComponent },
  { path: 'create', component: CreateAssetComponent },
  { path: 'edit/:id', component: EditAssetComponent },
];

@NgModule({
  declarations: [
    AssetComponent,
    CreateAssetComponent,
    EditAssetComponent
  ],
  imports: [
    CommonModule,
    FuseSharedModule,
    SharedModule,
    RouterModule.forChild(routes),
    NgxDatatableModule,
    MatButtonModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatSelectModule,
    MatCheckboxModule,
  ]
})
export class AssetModule { }
