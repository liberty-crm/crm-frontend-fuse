import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { SnackbarService } from 'app/shared/services/snackbar/snackbar.service';

import { AssetService } from '../../services/asset.service';
import { Asset } from '../../models/asset.model';
import { result, values } from 'lodash';

interface type {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-create-asset',
  templateUrl: './create-asset.component.html',
  styleUrls: ['./create-asset.component.scss']
})
export class CreateAssetComponent implements OnInit {

  string: any;
  value: type["value"];
  assetForm: FormGroup;
  assetModel: Asset;
  assetLiability: type[] = [
    { value: 'Assets', viewValue: 'Assets' },
    { value: 'Liabilities', viewValue: 'Liabilities' }
  ];
  selectedByDefault: any;
  viewValue: string;


  constructor(private translate: TranslateService,
    private fb: FormBuilder,
    private assetService: AssetService,
    private activatedRoute: ActivatedRoute,
    private snackbarService: SnackbarService,
    private router: Router) { }

  ngOnInit(): void {
    this.assetModel = new Asset();
    this.initiAssetForm();
  }

  submitForm(): void {
    if (this.assetForm.valid) {
      const result: Asset = Object.assign({}, this.assetForm.value);
      this.assetService.createAsset(result).subscribe((res: Asset) => {
        this.snackbarService.show('Asset added successfully!');
        this.router.navigate(['/asset']);
      });
    }
    }


  private initiAssetForm(): void {
    this.assetForm = this.fb.group({
      name: [this.assetModel.name, Validators.required],
      type: [this.assetModel.type, Validators.required],
    });
  }

  cancel() {
    this.router.navigate(['/asset']);
  }

  save(value) {
    this.value = value;
  }
}
