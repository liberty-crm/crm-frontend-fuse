import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription, Subject } from 'rxjs';
import { Pagination } from 'app/shared/models/pagination';
import { FilterOptionModel } from 'app/shared/models/filter-option.model';
// import { Asset } from 'app/shared/models/asset';
import { AssetService } from '../../services/asset.service';
import { Asset } from '../../models/asset.model';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-asset',
  templateUrl: './asset.component.html',
  styleUrls: ['./asset.component.scss']
})
export class AssetComponent implements OnInit, OnDestroy {

  private subscription: Subscription = new Subscription();
  private subject: Subject<string> = new Subject();
  rows: Asset[];
  loadingIndicator: boolean;
  reorderable: boolean;
  paginationInfo: Pagination;
  filterOptionModel: FilterOptionModel;

  columns = [
    { prop: 'name', name: 'Name' },
    { prop: 'type', name: 'Asset/Liability' }
  ];

  constructor(private router: Router, private assetService: AssetService) {
    this.loadingIndicator = true;
    this.reorderable = true;
    this.initFilterOptionModel();
    this.getAssetList();
  }

  ngOnInit(): void {
    this.subject.pipe(
        debounceTime(500)
      ).subscribe(searchTextValue => {
        this.searchAssets(searchTextValue);
      });
  }

  search(event) {
    const searchText = event.target.value    
    this.subject.next(searchText)
  }

  searchAssets(searchString) {     
    const filter=`Name.Contains("${searchString}") OR Type.Contains("${searchString}")`
    this.filterOptionModel = new FilterOptionModel({ searchQuery: filter });
    this.getAssetList();
  }

  public getAssetList(): void {
    this.assetService.getAsset(this.filterOptionModel).subscribe(res => {
      this.paginationInfo = JSON.parse(res.headers.get('X-Pagination'));
      this.paginationInfo.currentPage = this.paginationInfo.currentPage - 1;
      this.rows = res.body;
      this.loadingIndicator = false;
    });
  }

  getNextPage(pageInfo) {
    this.filterOptionModel.pageNumber = pageInfo.offset + 1;
    this.assetService.getAsset(this.filterOptionModel).subscribe(res => {
      this.paginationInfo = JSON.parse(res.headers.get('X-Pagination'));
      this.paginationInfo.currentPage = this.paginationInfo.currentPage - 1;
      this.rows = res.body;
      this.loadingIndicator = false;
    });
  }

  userRowSelect(event): any {
    if (event.type === 'click') {
      this.router.navigate(['/asset/edit/' + event.row.id]);
    }
  }

  goToCreateAsset(): void {
    this.router.navigate(['/asset/create']);
  }

  private initFilterOptionModel() {
    this.filterOptionModel = new FilterOptionModel();
  }
  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
