import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { SnackbarService } from 'app/shared/services/snackbar/snackbar.service';
import { Asset } from '../../models/asset.model';
import { AssetService } from '../../services/asset.service';

interface type {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-edit-asset',
  templateUrl: './edit-asset.component.html',
  styleUrls: ['./edit-asset.component.scss']
})
export class EditAssetComponent implements OnInit {

  value: type["value"];
  assetForm: FormGroup;
  assetModel: Asset;
  id: any;
  assetLiability: type[] = [
    {value: 'Assets', viewValue: 'Assets'},
    {value: 'Liabilities', viewValue: 'Liabilities'}
  ];

  constructor(private translate: TranslateService,
    private fb: FormBuilder,
    private assetService: AssetService,
    private snackbarService: SnackbarService,
    private activatedRoute: ActivatedRoute, 
    private router: Router) { 
      this.activatedRoute.params.subscribe(params => {
        this.id = params.id;
      });
    }

  ngOnInit(): void {
    this.assetModel = new Asset();
    this.initiAssetForm();
    this.getAssetById();
  }

  submitForm(): void {
    if (this.assetForm.valid) {
      const result: Asset = Object.assign(this.assetModel, this.assetForm.value);    
      this.assetService.updateAsset(this.id, result).subscribe((res: Asset) => {
        this.snackbarService.show('Asset updated successfully!');
        this.router.navigate(['/asset']);
      });
    }
}

private initiAssetForm(): void {
  this.assetForm = this.fb.group({
    name: [this.assetModel.name, Validators.required],
    type: [this.assetModel.type, Validators.required],         
  });  
}

cancel() {
  this.router.navigate(['/asset']);
}

private getAssetById(): void {
  this.assetService.getAssetById(this.id).subscribe(res => {
    this.assetModel = res;
    this.initiAssetForm();
});
}

delete() {
  this.assetService.deleteAssetById(this.id).subscribe((res:any)=>{
    this.getAssetById();
    this.snackbarService.show('Asset deleted successfully!');
    this.router.navigate(['/asset']);
  });
}

save(value) {
  this.value = value;
}

}
