import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { HttpClient } from '@angular/common/http';
import { environment } from 'environments/environment';
import { OpenIdConnectService } from 'app/shared/services/open-id-connect.service';
import { FilterOptionModel } from 'app/shared/models/filter-option.model';
import { Asset } from '../models/asset.model';

@Injectable({
  providedIn: 'root'
})

export class AssetService {
  selectedAsset: Asset;
  assetList: Asset[];
  Url = environment.apiUrl;

  constructor(private http: HttpClient, private openIdConnectService: OpenIdConnectService) { 
    this.openIdConnectService = openIdConnectService;
  }

  getAsset(filter: FilterOptionModel): Observable<any> {   
   
    const params = `PageNumber=${filter.pageNumber}&PageSize=${filter.pageSize}&SearchQuery=${filter.searchQuery}&OrderBy=${filter.orderBy}&Fields=${filter.fields}`;
    return this.http.get<any>(this.Url + `assets?${params}`, {observe: 'response'});
  }

  createAsset(assetSetupData: Asset): Observable<Asset> {
    return this.http.post<Asset>(this.Url + '/assets', assetSetupData);
  }

  getAssetById(id: string): Observable<Asset> {
    return this.http.get<Asset>(this.Url + '/assets/' + id);
  }

  updateAsset(id: string, selectedAsset: any): Observable<Asset> {
    return this.http.put<Asset>(this.Url + '/assets/' + id, selectedAsset);

  }

  deleteAssetById(id: number): Observable<Asset> {
    return this.http.delete<Asset>(this.Url + '/assets/' + id);
  }
}
