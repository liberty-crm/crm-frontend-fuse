import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'environments/environment';
import { Lead } from '../models/lead.model';
import { Asset } from 'app/shared/models/asset';
import { Service } from 'app/shared/models/service.model';
import { User } from '../../user/models/user.model';
import { FilterOptionModel } from 'app/shared/models/filter-option.model';
import { Store } from '../../store/models/store.model';
import { SensitiveInfo } from 'app/main/apps/lead-sin-details/models/sensitive-info.model';
import { LeadServiceModel, LeadServiceCreateModel } from '../models/lead-service.model';
import { FuseUtils } from '@fuse/utils';



@Injectable({
  providedIn: 'root'
})

export class LeadService {
  selectedLead: Lead;
  leadList: Lead[];
  Url = environment.apiUrl;

  constructor(private http: HttpClient) {    
  }

  getAssets(): Observable<Asset[]> {

    return this.http.get<Asset[]>(this.Url + 'assets');
  }
  getServices(): Observable<Service[]> {

    return this.http.get<Service[]>(this.Url + 'serviceoffereds');
  }

  getUser(): Observable<User[]> {
    return this.http.get<any>(this.Url + 'aspusers');
  }

  getFilteredUsers(filter: FilterOptionModel, isSkipLoader = false): Observable<User[]> {
    const params = `PageNumber=${filter.pageNumber}&PageSize=${filter.pageSize}&SearchQuery=${filter.searchQuery}&OrderBy=${filter.orderBy}&Fields=${filter.fields}`;
    if (isSkipLoader) {
        return this.http.get<any>(`${this.Url}aspusers?${params}`, FuseUtils.getSkipHttpLoaderHeader());
    } else {
        return this.http.get<any>(`${this.Url}aspusers?${params}`);
    }    
  }

  getStore(): Observable<Store[]> {

    return this.http.get<any>(this.Url + 'stores');
  }

  getLead(): Observable<Lead[]> {
    return this.http.get<Lead[]>(this.Url + 'leads');
  }

  getFilteredLeads(filter: FilterOptionModel): Observable<any> {
    const params = `PageNumber=${filter.pageNumber}&PageSize=${filter.pageSize}&SearchQuery=${filter.searchQuery}&OrderBy=${filter.orderBy}&Fields=${filter.fields}`;
    return this.http.get<Lead[]>(this.Url + `leads?${params}`, { observe: 'response' });
  }

  createLead(lead: Lead): Observable<Lead> {
    return this.http.post<Lead>(this.Url + 'leads', lead);
  }

  getLeadById(id: string): Observable<Lead> {
    return this.http.get<Lead>(this.Url + 'leads/' + id);
  }

  updateLead(id: string, selectedLead: any): Observable<Lead> {
    return this.http.put<Lead>(this.Url + 'leads/' + id, selectedLead);
  }

  addUpdateLeadServices(id: string, selectedServices: any) {
    return this.http.put<Lead>(`${this.Url}leads/AddUpdateLeadService?id=${id}`, selectedServices);
  }

  updateLeadPartially(id: string, selectedLead: any): Observable<any> {
    const options = { headers: new HttpHeaders().set('Content-Type', 'application/json-patch+json') };
    return this.http.patch<any>(this.Url + 'leads/' + id, selectedLead, options);
  }

  updateLeadServicePartially(id: string, selectedLeadService: any): Observable<any> {
    const options = { headers: new HttpHeaders().set('Content-Type', 'application/json-patch+json') };
    return this.http.patch<any>(`${this.Url}leadserviceoffereds/${id}`, selectedLeadService, options);
  }

  createLeadService(leadService: LeadServiceCreateModel): Observable<LeadServiceCreateModel> {
    return this.http.post<LeadServiceCreateModel>(this.Url + 'leadserviceoffereds', leadService);
  }

  getLeadServices(filter: FilterOptionModel): Observable<any> {
    const params = `PageNumber=${filter.pageNumber}&PageSize=${filter.pageSize}&SearchQuery=${filter.searchQuery}&OrderBy=${filter.orderBy}&Fields=${filter.fields}`;
    return this.http.get<any>(`${this.Url}leadserviceoffereds?${params}`, { observe: 'response' });
  }

  getLeadServiceById(id: string, fields: string): Observable<Lead> {
    return this.http.get<Lead>(`${this.Url}leadserviceoffereds/${id}?fields=${fields}`);
  }

  deleteLeadById(id: number): Observable<Lead> {
    return this.http.delete<Lead>(this.Url + 'leads/' + id);
  }

  getAspUserByRole(role: string): Observable<User[]> {
    return this.http.get<any>(this.Url + 'aspusers/GetAspUserByRole/' + role);
  }

  sendEmailToGetSensitiveInfo(id: string) {
    return this.http.post<any>(`${this.Url}leads/${id}`, null);
  }

  updateSensitiveInfo(sensitiveInfo: SensitiveInfo) {
    return this.http.put<Lead>(`${this.Url}leads/UpdateSensitiveLeadInfo`, sensitiveInfo);
  }

  isSinTokenValid(leadId: string, token: string) {
    return this.http.get<any>(`${this.Url}leads/ValidateToken?leadId=${leadId}&token=${token}`);
  }

  isSinExist(leadId: string, sin: string) {
    return this.http.get<any>(`${this.Url}leads/IsSinExist?leadId=${leadId}&sin=${sin}`);
  }

  removeLeadOfferedService(id: string) {
    return this.http.delete<any>(`${this.Url}leadserviceoffereds/${id}`);
  }

  triggerFTCrm(ftcrm: any)
  {
    return this.http.post<any>(this.Url + 'ftcrms', ftcrm);
  }
  
  getFilteredProvinces(): Observable<any> {
    return this.http.get<any>(this.Url + 'provinces');
  }

  isUserAuthorizedToAccessLead(searchQuery: string): Observable<any> {
    const params = `PageNumber=1&PageSize=1&SearchQuery=${searchQuery}&OrderBy=UpdatedOn desc&Fields=Id`;
    return this.http.get<Lead[]>(this.Url + `leads/GetFilteredLeadsDeatils?${params}`);
  }

}
