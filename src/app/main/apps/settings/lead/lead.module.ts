import { SharedModule } from './../../../../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { LeadComponent } from './components/lead/lead.component';
import { Routes, RouterModule } from '@angular/router';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { FuseSharedModule } from '@fuse/shared.module';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { CreateLeadComponent } from './components/create-lead/create-lead.component';
import { EditLeadComponent } from './components/edit-lead/edit-lead.component';
import { RequireAuthenticatedUserRouteGuardService } from 'app/shared/services/require-authenticated-user-route-guard.service';
import { MatTableModule } from '@angular/material/table';
import { MatTabsModule } from '@angular/material/tabs';
import { ContactModule } from '../contact/contact.module';
import { LeadServiceComponent } from './components/lead-service/lead-service.component';
import { LeadServiceTabsComponent } from './components/lead-service/lead-service-tabs/lead-service-tabs.component';
import { TextMaskModule } from 'angular2-text-mask';
import { AddEditServiceProductComponent } from './components/lead-service/add-edit-service-product/add-edit-service-product.component';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import { HasClaimDirective } from 'app/shared/services/security-service/has-claim.directive';
import { RequireAuthorizedUserRouteGuardService } from 'app/gaurds/require-authorized-user-route-guard.service';

const routes: Routes = [
  { path: '', component: LeadComponent },
  { path: 'create', component: CreateLeadComponent },
  { 
    path: 'edit/:id', 
    canActivate: [RequireAuthorizedUserRouteGuardService],
    component: EditLeadComponent 
  },
  {
    path: 'contacts/:leadId',
    canActivate: [RequireAuthenticatedUserRouteGuardService],
    loadChildren: () => import('../contact/contact.module').then(m => m.ContactModule)
},
];

@NgModule({
  declarations: [
    LeadComponent, 
    CreateLeadComponent, 
    EditLeadComponent, LeadServiceComponent, LeadServiceTabsComponent, AddEditServiceProductComponent,
    HasClaimDirective
  ],
  imports: [
    CommonModule,
    FuseSharedModule,
    SharedModule,
    RouterModule.forChild(routes),
    NgxDatatableModule,
    MatButtonModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatSelectModule,
    MatCheckboxModule,
    MatTableModule,
    MatTabsModule,
    ContactModule,
    TextMaskModule,
    MatAutocompleteModule,
  ],
  providers: [
    DatePipe,
  ],
})
export class LeadModule { }
