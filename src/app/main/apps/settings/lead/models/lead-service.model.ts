import { Service } from 'app/shared/models/service.model';

export interface LeadServiceModel {
    id: string;
    description: string;
    serviceOffered: Service;
    assignedTo: string;
    assignedStatus: string;
}

export interface LeadServiceCreateModel {
    id: string;
    serviceOfferedId: string;
    leadId: string;
    contactId: string;
    description: string;    
    assignedTo: string;
    assignedStatus: string;
}