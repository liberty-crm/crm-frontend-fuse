export class FTCrm {
    id: string;
    leadServiceId: string;
    status: string;
    amount: number;
    reason: string;
    ftcrmUniqueId: string;
}