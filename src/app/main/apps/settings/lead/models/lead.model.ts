import { AssetDto } from 'app/shared/models/asset';
import { Service } from 'app/shared/models/service.model';

export class Lead {
    id: string;
    leadNumber: number;
    firstName: string;
    middleName: string;
    lastName: string;
    address: string;
    city: string;
    province: string;
    zipcode: string;
    email: string;
    phone: string;
    source: string;
    existingClient: boolean;
    employment: string;
    position: string;
    companyName: string;
    website: string;
    employmentDate: Date | string;
    salaryFrequency: string;
    noOfChildren: number;
    customerInsight: string;
    allowMarketingMessages: boolean;
    allowUseOfPersonalInfo:boolean;
    selectedAssetId: AssetDto[];
    selectedServiceId: Service[];
    status: string;
    assignedTo: string;
    maritalStatus: string;
    assignedStore: string;
    sin: string;
    salaryAmount: number;
    agentId:any;
    referredBy:any;
}
export class Province{
    id:string;
    name:string;    
}

