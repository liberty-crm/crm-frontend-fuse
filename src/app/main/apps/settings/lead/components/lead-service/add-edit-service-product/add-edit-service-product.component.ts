import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import { Validators, FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { User } from 'app/main/apps/settings/user/models/user.model';
import { Subscription, forkJoin, Observable } from 'rxjs';
import { LeadServiceModel, LeadServiceCreateModel } from '../../../models/lead-service.model';
import { SnackbarService } from 'app/shared/services/snackbar/snackbar.service';
import { LeadService } from '../../../services/lead.service';
import { ActivatedRoute } from '@angular/router';
import { HistoryData } from 'app/shared/components/history/models/history.model';
import { HistoryEnum } from 'app/shared/enumerations/enum';
import { OpenIdConnectService } from 'app/shared/services/open-id-connect.service';
import { HistoryService } from 'app/shared/components/history/services/history.service';
import { FuseUtils } from '@fuse/utils';
import { ServiceofferedService } from 'app/main/apps/settings/serviceoffered/services/serviceoffered.service';
import { UserService } from 'app/main/apps/settings/user/services/user.service';
import { FilterOptionModel } from 'app/shared/models/filter-option.model';
import { debounceTime, switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-add-edit-service-product',
  templateUrl: './add-edit-service-product.component.html',
  styleUrls: ['./add-edit-service-product.component.scss']
})
export class AddEditServiceProductComponent implements OnInit, OnDestroy {

  isSalesRep:boolean = false;
    private subscription: Subscription = new Subscription();
    @Input() selectedLeadService: LeadServiceModel;
    @Output() back: EventEmitter<any> = new EventEmitter();
    leadServiceForm: FormGroup; 
    leadId: string; 
    loggedInUser: Oidc.User;
    newValue: any;
    oldValue: any;
    assignStatus = [
      'Interested',
      'Working',
      'Processing',
      'Process Complete',
      'Invoice',
      'Payment Complete',
      'Closed'
    ];    
    assignedTo: Observable<User[]>;     
    services = [];
    isFormLoaded = false;
    assingedToControl = new FormControl();
    selectedServiceManagerId: string;
  
    constructor(private fb: FormBuilder,
                private snackbarService: SnackbarService,
                private leadService: LeadService,
                private activatedRoute: ActivatedRoute,
                private serviceofferedService: ServiceofferedService,
                private openIdConnectService: OpenIdConnectService,
                private userService: UserService,
                private historyService: HistoryService) {
                  this.activatedRoute.params.subscribe(params => {
                      this.leadId = params.id;
                      this.loggedInUser = this.openIdConnectService.user;
                    });                  
                }
    
  
    ngOnInit(): void {
      this.getServices();       
    }

    displayFn(user: User) {
        if (user) { 
           return `${user.fName} ${user.lName}`;       
        }
      }
  
    submitForm() {
        this.leadServiceForm.markAllAsTouched();
        if (this.leadServiceForm.valid) {
            if (this.selectedLeadService?.id) {
                this.updateService();
            } else {
                this.createService();
            }          
        }
    }
  
    
  
    cancel() {
      this.back.emit();
    }

    serviceSelectionChanged() {        
        const selectedServiceId = this.leadServiceForm.get("id").value;
        const service = this.services.filter(x => x.id === selectedServiceId);
        this.selectedServiceManagerId = service[0].serviceManagerId;
        this.assingedToControl.setValue('');      
    }

    private getServices(): void {
        this.subscription.add(this.leadService.getServices()
        .subscribe(res => {
          this.services = res;
          if (this.loggedInUser.profile?.role === 'Ambassador' || this.loggedInUser.profile?.role === 'Store Manager' || this.loggedInUser.profile?.role === 'Store Employee') {
            this.isSalesRep = true;
          }
          this.initiLeadForm();          
          this.serviceSelectionChanged();
          this.prepareAssignToDropdown();
          setTimeout(() => {
            this.assingedToControl.setValue('');   
          });
        }));
      }

    private updateService(): void {
        const leadServiceFormDetails = this.getLeadServiceFormDetails();
        const partialLeadService = [
            {  value: leadServiceFormDetails.description, path: '/description', op: 'replace' },
            {  value: leadServiceFormDetails.assignedTo, path: '/assignedTo', op: 'replace' },
            {  value: leadServiceFormDetails.assignedStatus, path: '/assignedStatus', op: 'replace' }
        ];        
        this.subscription.add(
            this.leadService
                .updateLeadServicePartially(
                    this.selectedLeadService.id,
                    partialLeadService
                )
                .subscribe((res) => {
                    this.newValue = {
                        details: {
                            with: leadServiceFormDetails,
                        },
                    };

                    const newValue = this.prepareNewValue(this.newValue);

                    const historyTagData = FuseUtils.getDifference(
                        this.oldValue,
                        newValue
                    );

                    if (historyTagData) {

                        const historyToCreate = this.initHistoryDataForUpdate(
                            historyTagData
                        );

                        this.createHistory(historyToCreate);
                    } else {
                        this.back.emit();
                    }

                    this.snackbarService.show(
                        'Service details updated successfully!'
                    );
                    
                })
        );        
    }

    private createService() {
        const leadServiceCreateModel = this.getLeadServiceDetailsForCreate();
        this.subscription.add(this.leadService.createLeadService(leadServiceCreateModel)
        .subscribe(res => {
            this.snackbarService.show('Service created successfully!');
            this.back.emit();
        }));
    }

    private getLeadServiceDetailsForCreate(): LeadServiceCreateModel {
        const serviceDetails = this.getLeadServiceFormDetails();
        const leadServiceCreateModel: LeadServiceCreateModel = {
            id: '',
            serviceOfferedId: serviceDetails.serviceOfferedId,
            description: serviceDetails.description,
            leadId: this.leadId,
            contactId: '',
            assignedStatus: serviceDetails.assignedStatus,
            assignedTo: serviceDetails.assignedTo
        }
        return leadServiceCreateModel;
    }

    private getLeadServiceFormDetails() {
        return {
            id: this.selectedLeadService?.id || '',
            serviceOfferedId: this.leadServiceForm.get('id').value,
            description: this.leadServiceForm.get('description').value,            
            assignedTo: this.assingedToControl.value.id ?? null,
            assignedStatus: this.leadServiceForm.get('assignedStatus').value
        };
    }
  
    private initiLeadForm(): void {
     if (this.selectedLeadService?.id) {   
        this.oldValue = {        
            details: {          
              with: this.selectedLeadService
            }
          }; 

        this.leadServiceForm = this.fb.group({
            id: [this.selectedLeadService.serviceOffered.id, [Validators.required]],
            description: [{value : this.selectedLeadService.description, disabled: this.isSalesRep}],
            assignedTo: [{value : this.selectedLeadService.assignedTo, disabled: this.isSalesRep}],
            assignedStatus: [{value : this.selectedLeadService.assignedStatus, disabled: this.isSalesRep}]
        });
        this.leadServiceForm.get('id').disable();
        this.leadServiceForm.get('id').updateValueAndValidity();
        if (this.selectedLeadService.assignedTo) {
            const searchQuery = 'Id=="' + this.selectedLeadService.assignedTo + '"';       
            const filterOptionModel = new FilterOptionModel({ searchQuery, orderBy:'UpdatedOn desc' });
            this.subscription.add(this.leadService.getFilteredUsers(filterOptionModel, true)
            .subscribe(res => {
                this.assingedToControl.setValue(res[0]);
                this.oldValue.details.with.assignedTo = `${res[0].fName} ${res[0].lName}`;
            }));
          }   
     } else {
        this.leadServiceForm = this.fb.group({
            id: [this.services[0].id, [Validators.required]],
            description: [''],
            assignedTo: [''],
            assignedStatus: ['']
        });
     }
     this.isFormLoaded = true;         
    }

    private createHistory(historyToCreate): void {
        this.subscription.add(this.historyService.createHistory(historyToCreate)
          .subscribe(res => {
            this.snackbarService.show('History created successfully!');
            this.back.emit();
          }, err => {
            console.log(err);
          }));
      }

      private initHistoryDataForCreate(): HistoryData {
        const history = new HistoryData();
        history.leadId = this.leadId;
        history.serviceId = this.selectedLeadService?.id || '',
        history.activity = HistoryEnum['Service Added'];
        history.userId = this.loggedInUser.profile.sub;
        return history;
      }

      private initHistoryDataForUpdate(historyTagData: string): HistoryData {
        const history = new HistoryData();
        history.leadId = this.leadId;
        history.serviceId = this.selectedLeadService?.id || '',
        history.activity = HistoryEnum['Service Updated'];
        history.userId = this.loggedInUser.profile.sub;
        history.tagData = historyTagData;
        return history;
      }

      private prepareAssignToDropdown() {
        this.assignedTo = this.assingedToControl
        .valueChanges
        .pipe(
          debounceTime(300),
          switchMap(value => {
            let filter = '';
            if (value) {
                filter=`(FName.Contains("${value}") OR LName.Contains("${value}")) AND ((AspNetUserClaims.Any(ClaimValue=="Product Employee")) || Id=="${this.selectedServiceManagerId}") AND IsActive==true`;
            }
            if (!filter) {
                filter = `((AspNetUserClaims.Any(ClaimValue=="Product Employee")) || Id=="${this.selectedServiceManagerId}") AND IsActive==true`;
            }
            const filterOptionModel = new FilterOptionModel({searchQuery: filter, orderBy: 'UpdatedOn desc'});
            return this.leadService.getFilteredUsers(filterOptionModel, true);
          })
         );
      }

      private prepareNewValue(newCopy): any {
        const newValue = {
            details: {
                with: newCopy,
            },
        };        

        const assignedUser = this.assingedToControl.value;
        newValue.details.with.assignedTo = `${assignedUser.fName} ${assignedUser.lName}`;

        return newValue;
    }

    ngOnDestroy(): void {
        this.subscription.unsubscribe();
    }
}
