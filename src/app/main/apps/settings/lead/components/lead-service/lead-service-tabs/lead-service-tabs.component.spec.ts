import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeadServiceTabsComponent } from './lead-service-tabs.component';

describe('LeadServiceTabsComponent', () => {
  let component: LeadServiceTabsComponent;
  let fixture: ComponentFixture<LeadServiceTabsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeadServiceTabsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeadServiceTabsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
