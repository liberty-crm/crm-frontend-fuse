import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditServiceProductComponent } from './add-edit-service-product.component';

describe('AddEditServiceProductComponent', () => {
  let component: AddEditServiceProductComponent;
  let fixture: ComponentFixture<AddEditServiceProductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddEditServiceProductComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditServiceProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
