import { Component, OnInit, Input } from '@angular/core';
import { Subscription } from 'rxjs';
import { LeadService } from '../../services/lead.service';
import { MatTableDataSource } from '@angular/material/table';
import { Service } from 'app/shared/models/service.model';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, FormArray, Validators, FormGroup } from '@angular/forms';
import { SnackbarService } from 'app/shared/services/snackbar/snackbar.service';
import { Pagination } from 'app/shared/models/pagination';
import { FilterOptionModel } from 'app/shared/models/filter-option.model';
import { LeadServiceModel } from '../../models/lead-service.model';
import { CustomEventService } from 'app/shared/services/custom-event-serverice/custom-event.service';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmDialogComponent } from 'app/shared/components/confirm-dialog/confirm-dialog.component';
import { FTCrm } from '../../models/ftcrm.model';
import { OpenIdConnectService } from 'app/shared/services/open-id-connect.service';

@Component({
  selector: 'app-lead-service',
  templateUrl: './lead-service.component.html',
  styleUrls: ['./lead-service.component.scss']
})
export class LeadServiceComponent implements OnInit {

  private subscription: Subscription = new Subscription();
  leadId: string;
  @Input() selectedServiceIds = [];
  rows: LeadServiceModel[];
  loadingIndicator: boolean;
  reorderable: boolean;  
  paginationInfo: Pagination;
  filterOptionModel: FilterOptionModel;
  selectedLeadService: any; 
  isNewServiceOrProduct = false;
  disableTrigger = false;
  selectedServiceRefundAmount: number;

  columns = [
    // { prop: 'ftcrm.amount',name: 'Refund Amount'},
    { prop: 'ftcrm.reason',name: 'Integration Message'},
    { prop: 'ftcrm.status',name: 'FTCrm Status'},
    { prop: 'ftcrm.ftCrmUniqueId',name: 'FTCrm Id'},
    { prop: 'serviceOffered.name', name: 'Product/Service' },
    { prop: 'description', name: 'Description' },
    { prop: 'user.fName', name: 'Assigned To' },
    { prop: 'assignedStatus', name: 'Assigned Status' },    
   ];

  constructor(private leadService: LeadService,
              private activatedRoute: ActivatedRoute,
              private fb: FormBuilder,
              private dialog: MatDialog,
              private openIdConnectService: OpenIdConnectService,
              private customEventService: CustomEventService,
              private snackbarService: SnackbarService) {
                this.activatedRoute.params.subscribe(params => {
                    this.leadId = params.id;
                    this.loadingIndicator = true;
                    this.reorderable = true;                    
                  });
               }

  ngOnInit(): void {   
    this.initFilterOptionModel();
    this.getLeadServices();
      this.subscription.add(this.customEventService.LeadTabChangeEvent
        .subscribe(res => {
            this.showServiceListing();
        }))
  }

  public getLeadServices(): void {
    this.leadService.getLeadServices(this.filterOptionModel).subscribe(res => {  
        this.paginationInfo = JSON.parse(res.headers.get('X-Pagination'));
        this.paginationInfo.currentPage = this.paginationInfo.currentPage - 1; 
        this.rows = res.body;
        this.loadingIndicator = false;                      
    });
  }

  getNextPage(pageInfo) {
    this.filterOptionModel.pageNumber = pageInfo.offset + 1;
    this.leadService.getLeadServices(this.filterOptionModel).subscribe(res => {  
        this.paginationInfo = JSON.parse(res.headers.get('X-Pagination'));
        this.paginationInfo.currentPage = this.paginationInfo.currentPage - 1; 
        this.rows = res.body;
        this.loadingIndicator = false;                      
    });
  }

  userRowSelect(event): any {        
    if (event.type === 'click') {     
      // this.router.navigate(['/user/edit/' + event.row.id]);
      this.getLeadServiceById(event.row.id);
      this.selectedServiceRefundAmount = event.row.ftcrm?.amount;
      
    } 
  }

  private getLeadServiceById(id) {
    const fields = "Id,Description,ServiceOffered,AssignedTo,AssignedStatus";
    this.subscription.add(this.leadService.getLeadServiceById(id, fields)
    .subscribe(res => {
        this.selectedLeadService = res;
    }))
  }

  // private getFTCrm(id) {
  //   const fields = "Id,RefundAmount,FTCrmStatus,Reason";
  //   this.subscription.add(this.leadService.getFTCrm(id, fields)
  //   .subscribe(res => {
  //       this.selectedLeadService = res;
  //   }))
  // }

  showServiceListing() {
      this.selectedLeadService = null;
      this.isNewServiceOrProduct = false;
      this.initFilterOptionModel();
      this.getLeadServices();
  }

  goToAddServiceOrProduct() {
    this.isNewServiceOrProduct = true;
  }

  removeService(service: any, event: Event) {
    event.stopPropagation();
    const dialogRef = this.dialog.open(ConfirmDialogComponent,{
        data:{
          message: 'Are you sure want to remove this service?',
          buttonText: {
            ok: 'Remove',
            cancel: 'Cancel'
          }
        }
      });     
  
      dialogRef.afterClosed().subscribe((confirmed: boolean) => {
        if (confirmed) {
            this.subscription.add(this.leadService.removeLeadOfferedService(service.id)
            .subscribe(res => {
                this.rows = this.rows.filter(x => x.id !== service.id);
                this.snackbarService.show('Service removed successfully!');
            }));
        }
      });
  }


  triggerIntegration(service: any, event: Event) {
    event.stopPropagation();
    const dialogRef = this.dialog.open(ConfirmDialogComponent,{
        data:{
          message: 'Are you sure want to trigger integration for this service?',
          buttonText: {
            ok: 'Trigger',
            cancel: 'Cancel'
          }
        }
      });     
  
      dialogRef.afterClosed().subscribe((confirmed: boolean) => {
        if (confirmed) {
          var ftcrm = new FTCrm();
          ftcrm.leadServiceId = service.id;
            this.subscription.add(this.leadService.triggerFTCrm(ftcrm)
            .subscribe(res => {
                this.snackbarService.show('Integration triggered successfully!');
            }));
        }
      });
  }

  private initFilterOptionModel() {
    this.filterOptionModel = new FilterOptionModel({ searchQuery: this.getFilterQueryBySelectedUserRole()});
  }

  private getFilterQueryBySelectedUserRole(): string {
    let filter = '';
    switch(this.openIdConnectService.selectedUserRole.role) {
        case 'System Admin':
        case 'Relationship Manager':
        case 'Lead Admin':
          case'Ambassador':
          filter = 'LeadId=="' + this.leadId + '"';
          break;
      case 'Product Manager':
          filter = 'LeadId=="' + this.leadId + '" AND (ServiceOffered.ServiceManagerId=="' + 
            this.openIdConnectService.user.profile.sub +'")';
          break;     
      default:
          filter = 'LeadId=="' + this.leadId + '" AND (AssignedTo=="'+ this.openIdConnectService.user.profile.sub +'")';
    }
    return filter;
}
}
