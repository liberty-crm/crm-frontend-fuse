import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeadServiceComponent } from './lead-service.component';

describe('LeadServiceComponent', () => {
  let component: LeadServiceComponent;
  let fixture: ComponentFixture<LeadServiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeadServiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeadServiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
