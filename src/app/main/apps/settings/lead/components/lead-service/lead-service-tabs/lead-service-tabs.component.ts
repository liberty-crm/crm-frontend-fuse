import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { LeadServiceModel } from '../../../models/lead-service.model';
import { Subscription } from 'rxjs';
import { LeadService } from '../../../services/lead.service';
import { SnackbarService } from 'app/shared/services/snackbar/snackbar.service';
import { ActivatedRoute } from '@angular/router';
import { User } from 'app/main/apps/settings/user/models/user.model';
import { InvoiceService } from 'app/shared/components/invoice/services/invoice.service';


@Component({
  selector: 'app-lead-service-tabs',
  templateUrl: './lead-service-tabs.component.html',
  styleUrls: ['./lead-service-tabs.component.scss']
})
export class LeadServiceTabsComponent implements OnInit {

  private subscription: Subscription = new Subscription();
  @Input() selectedLeadService: LeadServiceModel;
  @Input() selectedServiceRefundAmount: number;
  @Output() back: EventEmitter<any> = new EventEmitter();
  leadServiceForm: FormGroup; 
  leadId: string; 
  assignStatus = [
    'Interested',
    'Working',
    'Processing',
    'Process Complete',
    'Invoice',
    'Payment Complete',
    'Closed'
  ];
  assignedTo = [];
  userList: User[];

  constructor(private fb: FormBuilder,
              private snackbarService: SnackbarService,
              private leadService: LeadService,
              private invoiceService: InvoiceService,
              private activatedRoute: ActivatedRoute) {
                this.activatedRoute.params.subscribe(params => {
                    this.leadId = params.id;
                  });
               }

  ngOnInit(): void {
    this.initiLeadForm();
    this.getUserList(); 
  }

  submitForm() {
      this.leadServiceForm.markAllAsTouched();
      if (this.leadServiceForm.valid) {
        const description = this.leadServiceForm.get("description").value;
        const partialLeadService = [{  "value": description, "path": "/description", "op": "replace" }]
        const assignedTo = this.leadServiceForm.get("assignedTo").value;
        const partialLeadService1 = [{  "value": assignedTo, "path": "/assignedTo", "op": "replace" }]
        const assignedStatus = this.leadServiceForm.get("assignedStatus").value;
        const partialLeadService2 = [{  "value": assignedStatus, "path": "/assignedStatus", "op": "replace" }]
        this.subscription.add(this.leadService.updateLeadServicePartially(this.selectedLeadService.id, partialLeadService)
        .subscribe(res => {
            // this.snackbarService.show('Service details updated successfully!');
            this.back.emit();
        }));
        this.subscription.add(this.leadService.updateLeadServicePartially(this.selectedLeadService.id, partialLeadService1)
        .subscribe(res => {
            // this.snackbarService.show('Service details updated successfully!');
            this.back.emit();
        }));
        this.subscription.add(this.leadService.updateLeadServicePartially(this.selectedLeadService.id, partialLeadService2)
        .subscribe(res => {
            this.snackbarService.show('Service details updated successfully!');
            this.back.emit();
        }));
      }
  }

  public getUserList(): void {
    this.leadService.getAspUserByRole('Product Employee').subscribe(res => {
      this.userList = res;
      this.assignedTo = this.userList;
    });
  }

  cancel() {
    this.back.emit();
  }

  private initiLeadForm(): void {
    this.leadServiceForm = this.fb.group({
        name: [this.selectedLeadService.serviceOffered.name, [Validators.required]],
        description: [this.selectedLeadService.description, [Validators.required]],
        assignedTo: [this.selectedLeadService.assignedTo, [Validators.required]],
        assignedStatus: [this.selectedLeadService.assignedStatus, [Validators.required]]
    });
    this.leadServiceForm.get('name').disable();
    this.leadServiceForm.get('name').updateValueAndValidity();
  } 
}
