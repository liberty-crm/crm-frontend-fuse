import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { SnackbarService } from 'app/shared/services/snackbar/snackbar.service';
import { Lead, Province } from '../../models/lead.model';
import { LeadService } from '../../services/lead.service';
import { Subscription } from 'rxjs';
import { AssetDto } from 'app/shared/models/asset';
import { MatTableDataSource } from '@angular/material/table';
import { Service } from 'app/shared/models/service.model';
import { REGEX_PATTERNS } from 'app/shared/constants/regex-pattern.constant';
import { Store } from '../../../store/models/store.model';
import { HistoryData } from 'app/shared/components/history/models/history.model';
import { HistoryEnum } from 'app/shared/enumerations/enum';
import { OpenIdConnectService } from 'app/shared/services/open-id-connect.service';
import { HistoryService } from 'app/shared/components/history/services/history.service';
import { DatePipe } from '@angular/common';
import { ContactService } from '../../../contact/services/contact.service';
import { CustomAsyncValidator } from 'app/shared/async-validators/async-validators';

@Component({
  selector: 'app-create-lead',
  templateUrl: './create-lead.component.html',
  styleUrls: ['./create-lead.component.scss']
})
export class CreateLeadComponent implements OnInit,OnDestroy {

  private subscription: Subscription = new Subscription();
  @Input() agentid: string;
  leadForm: FormGroup;
  assetsLiabilitiesForm: FormGroup;
  isFormSubmit = false;
  rows: Lead[];
  loadingIndicator: boolean;
  reorderable: boolean;
  createdLead: Lead; 
  loggedInUser: Oidc.User; 
  selfEmployedOrJob = [
    'Self Employed',
    'Job'
  ];
  salaryFrequencies = [
    'weekly',
    'bi-weekly',
    'monthly'
  ];
  sources = [
    'Facebook',
    'Google Search',
    'Google Ads',
    'Website',
    'Liberty Tax Stores/Others',
    'Others'
  ];
  assets = [];
  provinces = [];
  existingClient = [
    { key: 'No', value: false },
    { key: 'Yes', value: true }
  ];
  services = [];
  selectService = [];
  selectedAssets = [];
  assetLiability = [
      'Assets',
      'Liabilities'
  ];
  maritalstatus = [
    'Married',
    'Unmarried'
  ];
  storeList: Store[];
  assignedStore = [];
  provinceList: Province[];
  province = [];
  
  displayedColumns: string[] = ['type', 'name', 'valuation', 'description', 'action'];
  displayedServiceColumns: string[] = ['serviceName', 'serviceDescription', 'action'];
  serviceRows = [];  
  assetsLiabilities = [];
  dataSource =  new MatTableDataSource<AssetDto[]>(null);
  serviceDataSource =  new MatTableDataSource<Service[]>(null);
  constructor(private translate: TranslateService,
              private fb: FormBuilder,
              private leadService: LeadService,
              private activatedRoute: ActivatedRoute, 
              private snackbarService: SnackbarService,
              private router: Router,
              private datePipe: DatePipe,
              private contactService: ContactService,
              private customAsyncValidator: CustomAsyncValidator,
              private openIdConnectService: OpenIdConnectService,
              private historyService: HistoryService) { 
                this.loggedInUser = this.openIdConnectService.user;
              }

  ngOnInit(): void {
    this.initLeadForm();
    this.getServices();
    this.getAssets();  
    this.getStoreList();
    this.getProvincesList();  
    // this.initAssetsLiabilitiesForm();
  } 
  
  private initLeadForm(): void {
    this.leadForm = this.fb.group({
      fName: ['', 
             [Validators.required, Validators.pattern(REGEX_PATTERNS.ALPHABET_ONLY), Validators.maxLength(50)]],
      mName: ['', 
             [Validators.pattern(REGEX_PATTERNS.ALPHABET_ONLY), Validators.maxLength(50)]],
      lName: ['', 
             [Validators.required, Validators.pattern(REGEX_PATTERNS.ALPHABET_ONLY), Validators.maxLength(50)]],
      address: ['', Validators.required],
      city: ['',
        [Validators.required, Validators.pattern(REGEX_PATTERNS.ALPHABET_WITH_SPACE), Validators.maxLength(30)]],
      province: ['', Validators.required],
      zipcode: ['', [Validators.required, Validators.pattern(REGEX_PATTERNS.ZIP_CODE)]],     
      email: ['', [Validators.required, Validators.pattern(REGEX_PATTERNS.EMAIL)], 
                  this.customAsyncValidator.leadEmailValidator()],  
      phoneNumber: ['', [Validators.required, Validators.pattern(REGEX_PATTERNS.PHONE)],
                        this.customAsyncValidator.leadPhoneValidator()],         
      source: ['', Validators.required],
      existingClient: [false, Validators.required],      
      employment: [''],
      position: [''],
      companyName: [''],
      website: ['', [Validators.pattern(REGEX_PATTERNS.URL)]],
      employmentDate: [''],
      salaryFrequency: ['weekly'],
      existingAssests: [''],
      noOfChildren: [0, [Validators.pattern(REGEX_PATTERNS.POSITIVE_INTEGER_WITH_ZERO), Validators.max(9)]],
      allowMarketingMessages: [''],
      allowUseOfPersonalInfo:[''],
      service: this.fb.array([]),
      assetsLiability: this.fb.array([]),    
      customerInsight: [''],
      maritalStatus: ['Married'],
      assignedStore: [''],
      salaryAmount: [0],  
    });      
  }

  submitForm(): void {  
    this.leadForm.markAllAsTouched();
    if (this.leadForm.valid) {
        const lead = this.getLeadsDetails();
        this.subscription.add(
            this.leadService.createLead(lead).subscribe(
                (res) => {
                    this.createdLead = res;
                    const historyToCreate = this.initHistoryData();
                    this.createHistory(historyToCreate);
                    this.snackbarService.show('Lead added successfully!');
                    this.router.navigate(['/lead']);
                },
                (err) => {
                    console.log(err);
                }
            )
        );
    }
}

  private createHistory(historyToCreate) {
    this.subscription.add(this.historyService.createHistory(historyToCreate)
      .subscribe(res => {
        this.snackbarService.show('History created successfully!');
        this.router.navigate(['/lead']);
      }, err => {
        console.log(err);
      }));
  }

private initHistoryData(): HistoryData {
  const history = new HistoryData();
  history.leadId = this.createdLead.id;
  history.activity = HistoryEnum['Lead Created'];
  history.userId = this.loggedInUser.profile.sub;
  return history;  
}

serviceSelected(event): void {
  if (event.target.checked) {
    this.selectService.push(event.target.value);
  } else {
    this.selectService = this.selectService.filter(x => x !== event.target.value);
  }
}

assetsSelected(event): void {
  if (event.target.checked) {
    this.selectedAssets.push(event.target.value);
  } else {
    this.selectedAssets = this.selectedAssets.filter(x => x !== event.target.value);
  }
}

private getAssets(): void {
  this.subscription.add(this.leadService.getAssets()
  .subscribe(res => {
    console.log(res);
    this.assets = res;
  }));
}

public getStoreList(): void {
  this.leadService.getStore().subscribe(res => {
    this.storeList = res;
    this.assignedStore = this.storeList;
  });
}

private getServices(): void {
  this.subscription.add(this.leadService.getServices()
  .subscribe(res => {
    console.log(res);
    this.services = res;
  }));
}

public getProvincesList(): void {
  this.leadService.getFilteredProvinces().subscribe(res => {
    this.provinceList = res;
    this.provinces = this.provinceList;
  });
}

private getLeadsDetails(): Lead {
  const lead: Lead = {
    id: '',
    leadNumber:0,
    firstName: this.leadForm.controls['fName'].value,
    middleName: this.leadForm.controls['mName'].value,
    lastName: this.leadForm.controls['lName'].value,
    address: this.leadForm.controls['address'].value,
    city: this.leadForm.controls['city'].value,
    province: this.leadForm.controls['province'].value,
    zipcode: this.leadForm.controls['zipcode'].value,
    email: this.leadForm.controls['email'].value,
    phone: this.leadForm.controls['phoneNumber'].value,
    source: this.leadForm.controls['source'].value,
    existingClient: this.leadForm.controls['existingClient'].value,
    employment: this.leadForm.controls['employment'].value,
    position: this.leadForm.controls['position'].value,
    companyName: this.leadForm.controls['companyName'].value,
    website: this.leadForm.controls['website'].value,
    employmentDate: this.datePipe.transform(this.leadForm.controls['employmentDate'].value?.toString(), 'yyyy-MM-dd'),
    salaryFrequency: this.leadForm.controls['salaryFrequency'].value,
    noOfChildren: this.leadForm.controls['noOfChildren'].value,  
    status:'',  
    customerInsight: this.leadForm.controls['customerInsight'].value,
    allowMarketingMessages: this.leadForm.controls['allowMarketingMessages'].value,
    allowUseOfPersonalInfo:this.leadForm.controls['allowUseOfPersonalInfo'].value,
    selectedAssetId: Object.assign([], this.assetsLiabilityValues.value),
    selectedServiceId: Object.assign([], this.serviceValuse.value),
    assignedTo : '',
    agentId:this.loggedInUser.profile.sub,
    maritalStatus: this.leadForm.controls['maritalStatus'].value,
    assignedStore:this.leadForm.controls['assignedStore'].value,
    sin: '',
    salaryAmount: this.leadForm.controls['salaryAmount'].value,
    referredBy: '',    
  };
  return lead;
}

private get assetsLiabilityValues() {
    return this.leadForm.get('assetsLiability')  as FormArray;    
}

private get serviceValuse() {
    return this.leadForm.get('service')  as FormArray;    
}

private initAssetGrid() {    
    this.assetsLiabilities = [{ type: '', name: '', valuation: '', description: '', action: '' }];    
    this.dataSource = new MatTableDataSource<AssetDto[]>(this.assetsLiabilities);
}

private initServiceGrid() {    
    this.serviceRows = [{ name: '', description: '', action: '' }];    
    this.serviceDataSource = new MatTableDataSource<Service[]>(this.serviceRows);
}

addServiceRow() {    
    this.serviceRows.push({ name: '', description: '', action: '' });
    const serviceArray = <FormArray>this.leadForm.controls['service'];
    serviceArray.push(this.initServiceFormRow());
    this.serviceDataSource = new MatTableDataSource<Service[]>(this.serviceRows);
}

removeServiceRow(elem, index) {    
    this.serviceRows = this.serviceRows.filter(x => x !== elem);
    this.removeServiceFormRow(index);
    this.serviceDataSource = new MatTableDataSource<Service[]>(this.serviceRows);
}

addAssetsLiabilities() {    
    this.assetsLiabilities.push({ type: '', name: '', valuation: '', description: '', action: '' });
    this.addAssetsLiabilitiesFormRow();
    this.dataSource = new MatTableDataSource<AssetDto[]>(this.assetsLiabilities);
}

removeAssetsLiabilities(elem, index) {    
    this.assetsLiabilities = this.assetsLiabilities.filter(x => x !== elem);
    this.removetAssetsLiabilitiesFormRow(index);
    this.dataSource = new MatTableDataSource<AssetDto[]>(this.assetsLiabilities);
}

private addAssetsLiabilitiesFormRow(): void {
    const assetLiabilityArray = <FormArray>this.leadForm.controls['assetsLiability'];
    assetLiabilityArray.push(this.initAssetsLiabilitiesFormRow());
  }

private initAssetsLiabilitiesFormRow(): FormGroup {
    return this.fb.group({
        type: [null, [Validators.required]],
        id: [null, [Validators.required]],
        valuation: [null],
        description: [null]
      });
}

private removetAssetsLiabilitiesFormRow(rowIndex: number): void {
    const assetLiabilityArray = <FormArray>this.leadForm.controls['assetsLiability'];
    if (assetLiabilityArray.length > 1) {
        assetLiabilityArray.removeAt(rowIndex);
    } else {
        assetLiabilityArray.clear();
    }
}

private removeServiceFormRow(rowIndex: number): void {
    const serviceArray = <FormArray>this.leadForm.controls['service'];
    if (serviceArray.length > 1) {
        serviceArray.removeAt(rowIndex);
    } else {
        serviceArray.clear();
    }
}

private initServiceFormRow(): FormGroup {
    return this.fb.group({        
        id: [null, [Validators.required]],        
        description: [null]
      });
}

ngOnDestroy(): void {
  this.subscription.unsubscribe();
}

  cancel() {
    this.router.navigate(['/lead']);
  }

}
