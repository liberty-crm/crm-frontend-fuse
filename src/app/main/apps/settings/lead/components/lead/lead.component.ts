import { Component, OnInit, OnDestroy } from '@angular/core';
import { Lead } from '../../models/lead.model';
import { LeadService } from '../../services/lead.service';
import { Router } from '@angular/router';
import { Subscription, Subject } from 'rxjs';
import { Pagination } from 'app/shared/models/pagination';
import { FilterOptionModel } from 'app/shared/models/filter-option.model';
import { OpenIdConnectService } from 'app/shared/services/open-id-connect.service';
import { isNullOrUndefined } from 'util';
import { debounceTime } from 'rxjs/operators';


@Component({
  selector: 'app-lead',
  templateUrl: './lead.component.html',
  styleUrls: ['./lead.component.scss']
})
export class LeadComponent implements OnInit, OnDestroy {

  private subscription: Subscription = new Subscription();
  private subject: Subject<string> = new Subject();
  rows: Lead[];
  loadingIndicator: boolean;
  reorderable: boolean;   
  paginationInfo: Pagination;
  filterOptionModel: FilterOptionModel;
  loggedInUser: Oidc.User;
  filteredData = [];
  columns = [
    { prop: 'leadNumber', name: 'Lead Id', width: 75 },
    { prop: 'contact.firstName', name: 'First Name', width: 100 },    
    { prop: 'contact.lastName', name: 'Last Name', width: 100 },
    { prop: 'contact.email', name: 'Email', width: 200 },
    { prop: 'contact.phone', name: 'Phone', width: 150 },
    { prop: 'status', name: 'Status', width: 100 },
    { prop:  'user.fName', name:  'Assigned To', width: 100},
    { prop:  'store.name', name:  'Assigned Store', width: 150}
   ];

  constructor(private leadService: LeadService, 
              private router: Router,
              private openIdConnectService: OpenIdConnectService) {               
      this.loadingIndicator = true;
      this.reorderable = true;
      this.loggedInUser = this.openIdConnectService.user;
      this.initFilterOptionModel();
      this.getLeadList();
      
  } 
    
    
   
  public ngOnInit(): void {
    this.filteredData = this.rows;
    this.subject.pipe(
        debounceTime(500)
      ).subscribe(searchTextValue => {
        this.searchLead(searchTextValue);
      });
  }

  public getLeadList(): void {
    this.leadService.getFilteredLeads(this.filterOptionModel).subscribe(res => {            
        this.paginationInfo = JSON.parse(res.headers.get('X-Pagination'));
        this.paginationInfo.currentPage = this.paginationInfo.currentPage - 1;
        if(this.paginationInfo.totalCount===0 && res.body.length>0)
        {
          this.paginationInfo.totalCount = this.paginationInfo.totalCount + res.body.length;
          this.paginationInfo.totalPages = this.paginationInfo.totalPages + 1;
        }        

        res.body.forEach(element => {
            element.contact = element.contacts[0]
        });

        this.rows = res.body;
        this.loadingIndicator = false;                    
    });
  }

  getNextPage(pageInfo) {
    this.filterOptionModel.pageNumber = pageInfo.offset + 1;
    this.leadService.getFilteredLeads(this.filterOptionModel).subscribe(res => {  
        this.paginationInfo = JSON.parse(res.headers.get('X-Pagination'));
        this.paginationInfo.currentPage = this.paginationInfo.currentPage - 1; 
        res.body.forEach(element => {
            element.contact = element.contacts[0]
        });
        this.rows = res.body;
        this.loadingIndicator = false;                      
    });
  }
  
  leadRowSelect(event): any {        
    if (event.type === 'click') {  
      this.router.navigate(['/lead/edit/' + event.row.id]);
    } 
  }

  goToCreateLead(): void {
    this.router.navigate(['/lead/create']);
  }

  search(event) {
    const searchText = event.target.value    
    this.subject.next(searchText)
  }

  searchLead(searchString) {    
    let filter = '';   

    searchString = encodeURIComponent(searchString);
    if (searchString && !isNaN(searchString)) {
        filter = `(LeadNumber==${searchString} OR Contacts.Any(FirstName.Contains("${searchString}")) OR Contacts.Any(LastName.Contains("${searchString}"))`;        
    } else {
        filter = `(Contacts.Any(FirstName.Contains("${searchString}")) OR Contacts.Any(LastName.Contains("${searchString}"))`;
    } 
        filter = `${filter} OR Contacts.Any(Email.Contains("${searchString}")) OR Contacts.Any(Phone.Contains("${searchString}")))`;
        filter = `${filter} OR User.FName.Contains("${searchString}") OR User.LName.Contains("${searchString}")`;
        filter = `${filter} OR Store.Name.Contains("${searchString}")`;
        filter = `${filter} OR Status.Contains("${searchString}")`;
        filter = `${filter} AND Contacts.Any(RelationWithLead=="Self")`;
       
    
    const filterQuery = this.getFilterQueryBySelectedUserRole();
    if (filterQuery) {
        filter = filterQuery + ' AND ' + filter;
    }
    this.filterOptionModel = new FilterOptionModel({searchQuery: filter, orderBy: 'UpdatedOn desc'});
    this.getLeadList();
  }

  private initFilterOptionModel() {
    this.filterOptionModel = new FilterOptionModel({searchQuery: this.getFilterQueryBySelectedUserRole(), orderBy: 'UpdatedOn desc'});       
  }

  private getFilterQueryBySelectedUserRole(): string {
      let filter = '';
      let role = this.openIdConnectService.selectedUserRole.onlyRole??this.openIdConnectService.selectedUserRole.role;
      switch(role) {
          case 'System Admin':
            filter = '';
            break;
        case 'Product Manager':
            filter = '(LeadServiceOffereds.Any(ServiceOffered.ServiceManagerId=="' + 
                    this.loggedInUser.profile.sub +'"))';
            break;
        case 'Product Employee':
                filter = '(LeadServiceOffereds.Any(AssignedTo=="' + 
                        this.loggedInUser.profile.sub +'"))';
                break;        
        case 'Ambassador':
        case 'Store Employee':
            filter = '(AgentId=="' + this.loggedInUser.profile.sub +'")';
            break;
            case 'Store Manager':
            case 'Store Owner':
            filter = '(AssignedStore=="' + this.openIdConnectService.selectedUserRole.store.id +'")';
            break;            
        default:
            filter = '(AssignedTo=="'+ this.loggedInUser.profile.sub +'")';
      }
      return filter;
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
