import { Component, OnInit, SimpleChange, SimpleChanges, OnChanges, OnDestroy, KeyValueDiffer, KeyValueDiffers, KeyValueChanges } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray, FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { SnackbarService } from 'app/shared/services/snackbar/snackbar.service';
import { Lead, Province } from '../../models/lead.model';
import { LeadService } from '../../services/lead.service';
import { Subscription, Observable } from 'rxjs';
import { MatTableDataSource } from '@angular/material/table';
import { Service } from 'app/shared/models/service.model';
import { AssetDto } from 'app/shared/models/asset';
import { User } from '../../../user/models/user.model';
import { UserService } from '../../../user/services/user.service';
import { REGEX_PATTERNS } from 'app/shared/constants/regex-pattern.constant';
import { CustomEventService } from 'app/shared/services/custom-event-serverice/custom-event.service';
import { Store } from '../../../store/models/store.model';
import { FuseUtils } from '@fuse/utils';
import { HistoryService } from 'app/shared/components/history/services/history.service';
import { HistoryData } from 'app/shared/components/history/models/history.model';
import { HistoryEnum } from 'app/shared/enumerations/enum';
import { OidcClient } from 'oidc-client';
import { OpenIdConnectService } from 'app/shared/services/open-id-connect.service';
import { DatePipe } from '@angular/common';
import * as moment from 'moment';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmDialogComponent } from 'app/shared/components/confirm-dialog/confirm-dialog.component';
import { startWith, map, debounceTime, switchMap } from 'rxjs/operators';
import { isNullOrUndefined } from 'util';
import { CustomAsyncValidator } from 'app/shared/async-validators/async-validators';
import { FilterOptionModel } from 'app/shared/models/filter-option.model';
import { cloneDeep } from 'lodash';

@Component({
    selector: "app-edit-lead",
    templateUrl: "./edit-lead.component.html",
    styleUrls: ["./edit-lead.component.scss"],
})
export class EditLeadComponent implements OnInit, OnDestroy {
    readonly: boolean = false;
    isSalesRep: boolean = false;
    private subscription: Subscription = new Subscription();
    leadForm: FormGroup;
    isFormSubmit = false;
    leadModel: Lead;
    isFormLoaded = false;
    userList: User[];
    storeList: Store[];
    history: HistoryData;
    oldValue: any;
    provinces = [];
    provinceList: Province[];
    province = [];
    selfEmployedOrJob = ["Self Employed", "Job"];
    salaryFrequencies = ["weekly", "bi-weekly", "monthly"];
    leadStatus = [
        "New",
        "Already Availed",
        "Contacted",
        "Not Interested",
        "Interested",
        "Working",
        "Processing",
        "Closed",
    ];
    assets = [];
    existingClient = [
        { key: "No", value: false },
        { key: "Yes", value: true },
    ];
    assignedTo: Observable<User[]>;
    assignedStore = [];
    services = [];
    selectService = [];
    selectedAssets = [];
    id: string;
    assetLiability = ["Assets", "Liabilities"];
    maritalstatus = ["Married", "Unmarried"];
    sources = [
        "Facebook",
        "Google Search",
        "Google Ads",
        "Website",
        "Liberty Tax Stores/Others",
        "Others",
    ];
    displayedColumns: string[] = [
        "type",
        "name",
        "valuation",
        "description",
        "action",
    ];
    displayedServiceColumns: string[] = [
        "serviceName",
        "serviceDescription",
        "action",
    ];
    serviceRows = [];
    assetsLiabilities = [];
    showGetSensitiveInfoButton = false;
    dataSource = new MatTableDataSource<AssetDto[]>(null);
    serviceDataSource = new MatTableDataSource<Service[]>(null);
    diff = require("deep-diff").diff;
    loggedInUser: Oidc.User;
    assingedToControl = new FormControl();

    constructor(
        private translate: TranslateService,
        private fb: FormBuilder,
        private leadService: LeadService,
        private snackbarService: SnackbarService,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private customEventService: CustomEventService,
        private historyService: HistoryService,
        private datePipe: DatePipe,
        private dialog: MatDialog,
        private customAsyncValidator: CustomAsyncValidator,
        private openIdConnectService: OpenIdConnectService
    ) {
        this.activatedRoute.params.subscribe((params) => {
            this.id = params.id;
        });
        this.getLeadById();
        this.loggedInUser = this.openIdConnectService.user;
    }

    ngOnInit(): void {
        this.leadModel = new Lead();
        this.getServices();
        this.getAssets();
    }

    submitForm(): void {
        this.leadForm.markAllAsTouched();
        if (this.leadForm.valid) {
            const lead = this.getLeadsDetails();

            if (lead.employmentDate) {
                lead.employmentDate = this.datePipe.transform(
                    lead.employmentDate,
                    "yyyy-MM-dd"
                );
            }

            const newCopy = cloneDeep(lead);
            if (lead.employmentDate) {
                lead.employmentDate = this.datePipe.transform(
                    lead.employmentDate,
                    "yyyy-MM-dd"
                );
                newCopy.employmentDate = this.datePipe.transform(
                    newCopy.employmentDate,
                    "MM-dd-yyyy"
                );
            }

            const newValue = this.prepareNewValue(newCopy);

            this.subscription.add(
                this.leadService.updateLead(lead.id, lead).subscribe(
                    (res) => {
                        this.snackbarService.show('Lead updated successfully!');
                        this.router.navigate(['/lead']);
                    },
                    (err) => {
                        console.log(err);
                    }
                )
            );

            const historyTagData = FuseUtils.getDifference(
                this.oldValue,
                newValue
            );

            if (historyTagData) {
                this.initHistoryData(historyTagData);
                this.createHistory();
            }
        }
    }

    private createHistory(): void {
        this.subscription.add(
            this.historyService.createHistory(this.history).subscribe(
                (res) => {
                    this.snackbarService.show(
                        'History created successfully!'
                    );
                    this.router.navigate(['/lead']);
                },
                (err) => {
                    console.log(err);
                }
            )
        );
    }

    private prepareNewValue(newCopy) {
        const newValue = {
            details: {
                with: newCopy,
            },
        };

        const assignedStoreName = this.assignedStore.filter(
            (x) => x.id === newValue.details.with.assignedStore
        );
        if (assignedStoreName && assignedStoreName.length > 0) {
            newValue.details.with.assignedStore = assignedStoreName[0].name;
        }

        const provinceName = this.provinces.filter(
            (x) => x.id === newValue.details.with.province
        );
        if (provinceName && provinceName.length > 0) {
            newValue.details.with.province = provinceName[0].name;
        }

        const assignedUser = this.assingedToControl.value;
        if (assignedUser) {
            newValue.details.with.assignedTo = `${assignedUser.fName} ${assignedUser.lName}`;
        } else {
            newValue.details.with.assignedTo = '';
        }
        

        return newValue;
    }

    private initHistoryData(historyTagData: string): void {
        this.history = new HistoryData();
        this.history.leadId = this.id;
        this.history.activity = HistoryEnum['Lead Updated'];
        this.history.userId = this.loggedInUser.profile.sub;
        this.history.tagData = historyTagData;
    }

    public getUserList(searchQuery): any {
        return this.leadService.getUser();
        // this.leadService.getUser().subscribe(res => {
        //   this.userList = res;
        //   this.assignedTo = this.userList;
        // });
    }

    public getStoreList(): void {
        this.leadService.getStore().subscribe((res) => {
            this.storeList = res;
            this.assignedStore = this.storeList;
            const assignedStoreName = this.assignedStore.filter(
                (x) => x.id === this.oldValue.details.with.assignedStore
            );
            if (assignedStoreName && assignedStoreName.length > 0) {
                this.oldValue.details.with.assignedStore =
                    assignedStoreName[0].name;
            }
        });
    }

    public getProvincesList(): void {
        this.leadService.getFilteredProvinces().subscribe((res) => {
            this.provinceList = res;
            this.provinces = this.provinceList;
            const provinceName = this.provinces.filter(
                (x) => x.id === this.oldValue.details.with.province
            );
            if (provinceName && provinceName.length > 0) {
                this.oldValue.details.with.province = provinceName[0].name;
            }
        });
    }

    private initiLeadForm(): void {
        if (this.leadModel != undefined) {
            this.leadForm = this.fb.group({
                firstName: [
                    {
                        value: this.leadModel.firstName,
                        disabled: this.isSalesRep,
                    },
                    [
                        Validators.required,
                        Validators.pattern(REGEX_PATTERNS.ALPHABET_ONLY),
                        Validators.maxLength(50),
                    ],
                ],
                middleName: [
                    {
                        value: this.leadModel.middleName,
                        disabled: this.isSalesRep,
                    },
                    [
                        Validators.pattern(REGEX_PATTERNS.ALPHABET_ONLY),
                        Validators.maxLength(50),
                    ],
                ],
                lastName: [
                    {
                        value: this.leadModel.lastName,
                        disabled: this.isSalesRep,
                    },
                    [
                        Validators.required,
                        Validators.pattern(REGEX_PATTERNS.ALPHABET_ONLY),
                        Validators.maxLength(50),
                    ],
                ],
                address: [
                    {
                        value: this.leadModel.address,
                        disabled: this.isSalesRep,
                    },
                    Validators.required,
                ],
                city: [
                    { value: this.leadModel.city, disabled: this.isSalesRep },
                    [
                        Validators.required,
                        Validators.pattern(REGEX_PATTERNS.ALPHABET_WITH_SPACE),
                        Validators.maxLength(30),
                    ],
                ],
                province: [
                    {
                        value: this.leadModel.province,
                        disabled: this.isSalesRep,
                    },
                    Validators.required,
                ],
                zipcode: [
                    {
                        value: this.leadModel.zipcode,
                        disabled: this.isSalesRep,
                    },
                    [
                        Validators.required,
                        Validators.pattern(REGEX_PATTERNS.ZIP_CODE),
                    ],
                ],
                email: [
                    { value: this.leadModel.email, disabled: this.isSalesRep },
                    [
                        Validators.required,
                        Validators.pattern(REGEX_PATTERNS.EMAIL),
                    ],
                    this.customAsyncValidator.leadEmailValidator(
                        this.leadModel.email
                    ),
                ],
                phoneNumber: [
                    { value: this.leadModel.phone, disabled: this.isSalesRep },
                    [
                        Validators.required,
                        Validators.pattern(REGEX_PATTERNS.PHONE),
                    ],
                    this.customAsyncValidator.leadPhoneValidator(
                        this.leadModel.phone
                    ),
                ],
                source: [
                    { value: this.leadModel.source, disabled: this.isSalesRep },
                    Validators.required,
                ],
                existingClient: [
                    {
                        value: this.leadModel.existingClient,
                        disabled: this.isSalesRep,
                    },
                    Validators.required,
                ],
                employment: [
                    {
                        value: this.leadModel.employment,
                        disabled: this.isSalesRep,
                    },
                ],
                position: [
                    {
                        value: this.leadModel.position,
                        disabled: this.isSalesRep,
                    },
                ],
                companyName: [
                    {
                        value: this.leadModel.companyName,
                        disabled: this.isSalesRep,
                    },
                ],
                website: [
                    {
                        value: this.leadModel.website,
                        disabled: this.isSalesRep,
                    },
                    [Validators.pattern(REGEX_PATTERNS.URL)],
                ],
                employmentDate: [
                    {
                        value: this.leadModel.employmentDate,
                        disabled: this.isSalesRep,
                    },
                    new Date(),
                ],
                salaryFrequency: [
                    {
                        value: this.leadModel.salaryFrequency,
                        disabled: this.isSalesRep,
                    },
                ],
                noOfChildren: [
                    {
                        value: this.leadModel.noOfChildren,
                        disabled: this.isSalesRep,
                    },
                    [
                        Validators.pattern(
                            REGEX_PATTERNS.POSITIVE_INTEGER_WITH_ZERO
                        ),
                        Validators.max(9),
                    ],
                ],
                allowMarketingMessages: [
                    {
                        value: this.leadModel.allowMarketingMessages,
                        disabled: this.isSalesRep,
                    },
                ],
                allowUseOfPersonalInfo: [
                    {
                        value: this.leadModel.allowUseOfPersonalInfo,
                        disabled: this.isSalesRep,
                    },
                ],
                service: this.fb.array([]),
                assetsLiability: this.fb.array([]),
                status: [
                    { value: this.leadModel.status, disabled: this.isSalesRep },
                ],
                customerInsight: [
                    {
                        value: this.leadModel.customerInsight,
                        disabled: this.isSalesRep,
                    },
                ],
                assignedTo: [
                    {
                        value: this.leadModel.assignedTo,
                        disabled: this.isSalesRep,
                    },
                ],
                maritalStatus: [
                    {
                        value: this.leadModel.maritalStatus,
                        disabled: this.isSalesRep,
                    },
                ],
                assignedStore: [
                    {
                        value: this.leadModel.assignedStore,
                        disabled: this.isSalesRep,
                    },
                ],
                salaryAmount: [
                    {
                        value: this.leadModel.salaryAmount,
                        disabled: this.isSalesRep,
                    },
                ],
                referredBy: [
                    {
                        value: this.leadModel.referredBy,
                        disabled: this.isSalesRep,
                    },
                ],
            });
            this.leadModel.selectedAssetId.forEach((elem) => {
                const assetsLiabilityArray = <FormArray>(
                    this.leadForm.controls["assetsLiability"]
                );
                assetsLiabilityArray.push(
                    this.fb.group({
                        type: [
                            { value: elem.type, disabled: this.isSalesRep },
                            [Validators.required],
                        ],
                        id: [
                            { value: elem.id, disabled: this.isSalesRep },
                            [Validators.required],
                        ],
                        valuation: [
                            {
                                value: elem.valuation,
                                disabled: this.isSalesRep,
                            },
                            [
                                Validators.pattern(
                                    REGEX_PATTERNS.POSITIVE_INTEGERS
                                ),
                            ],
                        ],
                        description: [
                            {
                                value: elem.description,
                                disabled: this.isSalesRep,
                            },
                        ],
                    })
                );
            });

            //   if (this.leadModel.selectedAssetId?.length === 0) {
            //     const assetsLiabilityArray = <FormArray>this.leadForm.controls['assetsLiability'];
            //     assetsLiabilityArray.push(this.fb.group({
            //         type: [null, [Validators.required]],
            //         id: [null, [Validators.required]],
            //         valuation: [null, [Validators.pattern(REGEX_PATTERNS.POSITIVE_INTEGERS)]],
            //         description: [null]
            //       }));
            //   }

            this.leadModel.selectedServiceId.forEach((elem) => {
                const serviceArray = <FormArray>(
                    this.leadForm.controls["service"]
                );
                serviceArray.push(
                    this.fb.group({
                        id: [
                            { value: elem.id, disabled: this.isSalesRep },
                            [Validators.required],
                        ],
                        description: [
                            {
                                value: elem.description,
                                disabled: this.isSalesRep,
                            },
                        ],
                    })
                );
            });
            this.initAssetGrid();
            this.initServiceGrid();
            this.prepareAssignToDropdown();
            this.isFormLoaded = true;
        }
    }

    serviceSelected(event): void {
        if (event.target.checked) {
            this.selectService.push(event.target.value);
        } else {
            this.selectService = this.selectService.filter(
                (x) => x !== event.target.value
            );
        }
    }

    assetsSelected(event): void {
        if (event.target.checked) {
            this.selectedAssets.push(event.target.value);
        } else {
            this.selectedAssets = this.selectedAssets.filter(
                (x) => x !== event.target.value
            );
        }
    }

    tabChanged(event) {
        this.initiLeadForm();
        this.customEventService.LeadTabChangeEvent.next();
    }

    sendEmail() {
        this.subscription.add(
            this.leadService
                .sendEmailToGetSensitiveInfo(this.leadModel.id)
                .subscribe((res) => {
                    this.snackbarService.show("Email sent successfully!");
                })
        );

        var history = new HistoryData();
        history.leadId = this.id;
        history.activity = HistoryEnum["Sensitive Information Mail Sent"];
        history.userId = this.loggedInUser.profile.sub;

        this.subscription.add(
            this.historyService.createHistory(history).subscribe(
                (res) => {},
                (err) => {
                    console.log(err);
                }
            )
        );
    }

    getSensitiveInfo() {
        if (!isNullOrUndefined(this.leadModel.sin)) {
            const dialogRef = this.dialog.open(ConfirmDialogComponent, {
                data: {
                    message:
                        "Sin Number is already provided by the lead. Do you want to get it again?",
                    buttonText: {
                        ok: "Send",
                        cancel: "Cancel",
                    },
                },
            });

            dialogRef.afterClosed().subscribe((confirmed: boolean) => {
                if (confirmed) {
                    this.sendEmail();
                }
            });
        } else {
            const filterOptionModel = {
                pageNumber: 1,
                pageSize: 1,
                fields: "",
                searchQuery: "",
                orderBy: "CreatedOn Desc",
            };
            filterOptionModel.searchQuery =
                'LeadId=="' + this.id + '"' + " AND Activity==13";
            this.subscription.add(
                this.historyService
                    .getFilteredHistory(filterOptionModel)
                    .subscribe((res) => {
                        const result = res.body;
                        if (result.length > 0) {
                            this.getConfirmationToSentEmailAgain(
                                result[0].createdOn
                            );
                        } else {
                            this.sendEmail();
                        }
                    })
            );
        }
    }

    private getConfirmationToSentEmailAgain(date: any) {
        const confirmMessageText = this.getConfirmMessageTextForSensitiveInfoEmail(
            date
        );
        const dialogRef = this.dialog.open(ConfirmDialogComponent, {
            data: {
                message: confirmMessageText,
                buttonText: {
                    ok: "Send",
                    cancel: "Cancel",
                },
            },
        });

        dialogRef.afterClosed().subscribe((confirmed: boolean) => {
            if (confirmed) {
                this.sendEmail();
            }
        });
    }

    private getAssets(): void {
        this.subscription.add(
            this.leadService.getAssets().subscribe((res) => {
                console.log(res);
                this.assets = res;
            })
        );
    }

    private getServices(): void {
        this.subscription.add(
            this.leadService.getServices().subscribe((res) => {
                console.log(res);
                this.services = res;
            })
        );
    }

    private getLeadsDetails(): Lead {
        const lead: Lead = {
            id: this.leadModel.id,
            leadNumber: this.leadModel.leadNumber,
            firstName: this.leadForm.controls["firstName"].value,
            middleName: this.leadForm.controls["middleName"].value,
            lastName: this.leadForm.controls["lastName"].value,
            address: this.leadForm.controls["address"].value,
            city: this.leadForm.controls["city"].value,
            province: this.leadForm.controls["province"].value,
            zipcode: this.leadForm.controls["zipcode"].value,
            email: this.leadForm.controls["email"].value,
            phone: this.leadForm.controls["phoneNumber"].value,
            source: this.leadForm.controls["source"].value,
            existingClient: this.leadForm.controls["existingClient"].value,
            employment: this.leadForm.controls["employment"].value,
            position: this.leadForm.controls["position"].value,
            companyName: this.leadForm.controls["companyName"].value,
            website: this.leadForm.controls["website"].value,
            employmentDate: this.datePipe.transform(
                this.leadForm.controls["employmentDate"].value?.toString(),
                "yyyy-MM-dd"
            ),
            salaryFrequency: this.leadForm.controls["salaryFrequency"].value,
            noOfChildren: this.leadForm.controls["noOfChildren"].value,
            status: this.leadForm.controls["status"].value,
            customerInsight: this.leadForm.controls["customerInsight"].value,
            allowMarketingMessages: this.leadForm.controls[
                "allowMarketingMessages"
            ].value,
            allowUseOfPersonalInfo: this.leadForm.controls[
                "allowUseOfPersonalInfo"
            ].value,
            selectedAssetId: Object.assign(
                [],
                this.assetsLiabilityValues.value
            ),
            selectedServiceId: Object.assign([], this.serviceValuse.value),
            assignedTo: this.assingedToControl.value.id,
            maritalStatus: this.leadForm.controls["maritalStatus"].value,
            assignedStore: this.leadForm.controls["assignedStore"].value,
            sin: this.leadModel.sin,
            salaryAmount: this.leadForm.controls["salaryAmount"].value,
            agentId: this.leadModel.agentId,
            referredBy: this.leadModel.referredBy,
        };
        return lead;
    }

    private getConfirmMessageTextForSensitiveInfoEmail(date: any) {
        let text = "";
        const currentDate = moment();
        const createdOn = moment.utc(date).local().toDate();
        const dateDiff = FuseUtils.getDateDiff(currentDate, createdOn);

        if (dateDiff.dayDiff > 0) {
            if (dateDiff.dayDiff == 1) {
                text = `You have sent email ${dateDiff.dayDiff} day ago. Are you sure you want to sent it again?`;
            } else {
                text = `You have sent email ${dateDiff.dayDiff} days ago. Are you sure you want to sent it again?`;
            }
        } else if (dateDiff.hourDiff > 0) {
            if (dateDiff.hourDiff == 1) {
                text = `You have sent email ${dateDiff.hourDiff} hour ago. Are you sure you want to sent it again?`;
            } else {
                text = `You have sent email ${dateDiff.dayDiff} hours ago. Are you sure you want to sent it again?`;
            }
        } else if (dateDiff.minuteDiff > 0) {
            if (dateDiff.minuteDiff == 1) {
                text = `You have sent email ${dateDiff.minuteDiff} minute ago. Are you sure you want to sent it again?`;
            } else {
                text = `You have sent email ${dateDiff.minuteDiff} minutes ago. Are you sure you want to sent it again?`;
            }
        } else {
            text = `You have sent email ${dateDiff.secondDiff} seconds ago. Are you sure you want to sent it again?`;
        }
        return text;
    }

    goToContactListing(): void {
        this.router.navigate([`/lead/contacts/${this.id}`]);
    }

    cancel() {
        this.router.navigate(["/lead"]);
    }

    displayFn(user: User) {
        if (user) {
            return `${user.fName} ${user.lName}`;
        }
    }

    private getLeadById(): void {
        this.leadService.getLeadById(this.id).subscribe((res) => {
            res.employmentDate = this.datePipe.transform(
                res.employmentDate,
                "yyyy-MM-dd"
            );
            this.leadModel = res;

            const oldCopy = cloneDeep(res);
            oldCopy.employmentDate = this.datePipe.transform(
                res.employmentDate,
                "MM-dd-yyyy"
            );
            this.oldValue = {
                details: {
                    with: oldCopy,
                },
            };

            this.getStoreList();
            this.getProvincesList();

            if (this.leadModel.assignedTo) {
                const searchQuery = 'Id=="' + this.leadModel.assignedTo + '"';
                const filterOptionModel = new FilterOptionModel({
                    searchQuery,
                    orderBy: "UpdatedOn desc",
                });
                this.subscription.add(
                    this.leadService
                        .getFilteredUsers(filterOptionModel, true)
                        .subscribe((res) => {
                            this.assingedToControl.setValue(res[0]);
                            this.oldValue.details.with.assignedTo = `${res[0].fName} ${res[0].lName}`;
                        })
                );
            }
            if (
                this.loggedInUser.profile?.role == "Ambassador" ||
                this.loggedInUser.profile?.role == "Product Manager" ||
                this.loggedInUser.profile?.role == "Product Employee"
            ) {
                this.isSalesRep = true;
            }
            this.initiLeadForm();

            var isRelationshipManager = false;
            if (Array.isArray(this.openIdConnectService.user.profile.role)) {
                this.openIdConnectService.user.profile.role.forEach((role) => {
                    if (role.toLowerCase() === "relationship manager") {
                        isRelationshipManager = true;
                    }
                });
            } else {
                if (
                    this.openIdConnectService.user.profile.role.toLowerCase() ===
                    "relationship manager"
                ) {
                    isRelationshipManager = true;
                }
            }

            if (isRelationshipManager && this.leadModel.assignedTo) {
                this.showGetSensitiveInfoButton = true;
            } else {
                this.showGetSensitiveInfoButton = false;
            }
        });
    }

    private get assetsLiabilityValues() {
        return this.leadForm.get("assetsLiability") as FormArray;
    }

    private get serviceValuse() {
        return this.leadForm.get("service") as FormArray;
    }

    private initAssetGrid() {
        this.assetsLiabilities = [];
        this.leadModel.selectedAssetId.forEach((element) => {
            this.assetsLiabilities.push({
                type: "",
                name: "",
                valuation: "",
                description: "",
                action: "",
            });
        });
        this.dataSource = new MatTableDataSource<AssetDto[]>(
            this.assetsLiabilities
        );
    }

    private initServiceGrid() {
        this.serviceRows = [];
        this.leadModel.selectedServiceId.forEach((elem) => {
            this.serviceRows.push({ name: "", description: "", action: "" });
        });
        this.serviceDataSource = new MatTableDataSource<Service[]>(
            this.serviceRows
        );
    }

    addServiceRow() {
        this.serviceRows.push({ name: "", description: "", action: "" });
        const serviceArray = <FormArray>this.leadForm.controls["service"];
        serviceArray.push(this.initServiceFormRow());
        this.serviceDataSource = new MatTableDataSource<Service[]>(
            this.serviceRows
        );
    }

    removeServiceRow(elem, index) {
        this.serviceRows = this.serviceRows.filter((x) => x !== elem);
        this.removeServiceFormRow(index);
        this.serviceDataSource = new MatTableDataSource<Service[]>(
            this.serviceRows
        );
    }

    addAssetsLiabilities() {
        this.assetsLiabilities.push({
            type: "",
            name: "",
            valuation: "",
            description: "",
            action: "",
        });
        this.addAssetsLiabilitiesFormRow();
        this.dataSource = new MatTableDataSource<AssetDto[]>(
            this.assetsLiabilities
        );
    }

    removeAssetsLiabilities(elem, index) {
        this.assetsLiabilities = this.assetsLiabilities.filter(
            (x) => x !== elem
        );
        this.removetAssetsLiabilitiesFormRow(index);
        this.dataSource = new MatTableDataSource<AssetDto[]>(
            this.assetsLiabilities
        );
    }

    private addAssetsLiabilitiesFormRow(): void {
        const assetLiabilityArray = <FormArray>(
            this.leadForm.controls["assetsLiability"]
        );
        assetLiabilityArray.push(this.initAssetsLiabilitiesFormRow());
    }

    private initAssetsLiabilitiesFormRow(): FormGroup {
        return this.fb.group({
            type: [null, [Validators.required]],
            id: [null, [Validators.required]],
            valuation: [null],
            description: [null],
        });
    }

    private removetAssetsLiabilitiesFormRow(rowIndex: number): void {
        let assetLiabilityArray = <FormArray>(
            this.leadForm.controls["assetsLiability"]
        );
        if (assetLiabilityArray.length > 1) {
            assetLiabilityArray.removeAt(rowIndex);
        } else {
            assetLiabilityArray.clear();
        }
    }

    private removeServiceFormRow(rowIndex: number): void {
        const serviceArray = <FormArray>this.leadForm.controls["service"];
        if (serviceArray.length > 1) {
            serviceArray.removeAt(rowIndex);
        }
    }

    private initServiceFormRow(): FormGroup {
        return this.fb.group({
            id: [null, [Validators.required]],
            description: [null],
        });
    }

    private prepareAssignToDropdown() {
        this.assignedTo = this.assingedToControl.valueChanges.pipe(
            debounceTime(300),
            switchMap((value) => {
                let filter = "";
                if (value) {
                    filter = `(FName.Contains("${value}") OR LName.Contains("${value}")) AND (AspNetUserClaims.All(ClaimValue!="Ambassador")) AND IsActive==true`;
                }
                if (!filter) {
                    filter =
                        '(AspNetUserClaims.All(ClaimValue!="Ambassador")) AND IsActive==true';
                }
                const filterOptionModel = new FilterOptionModel({
                    searchQuery: filter,
                    orderBy: "UpdatedOn desc",
                });
                return this.leadService.getFilteredUsers(
                    filterOptionModel,
                    true
                );
            })
        );
    }

    ngOnDestroy(): void {
        this.subscription.unsubscribe();
    }
}
