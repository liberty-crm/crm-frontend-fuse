import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { TaskService } from 'app/shared/components/task/services/task.service';
import { OpenIdConnectService } from 'app/shared/services/open-id-connect.service';
import { CalendarEventModel } from 'app/shared/components/calendar/event.model';

@Injectable({
  providedIn: 'root'
})
export class CalendarResolverService {

  constructor(
    private taskService: TaskService,
    private openIdConnectService: OpenIdConnectService
  ) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {  
    const filterOptionModel = this.getFilterOptionModel();  
    return this.taskService.getFilteredTasks(filterOptionModel).pipe(map(res => {
      if (res) {
        return this.getCalendarData(res.body);
      }
      return null; // map returns value as an observable so we don't need to use of
    }), catchError((err: any) => {
      return of(null);
    }));
  }

  private getCalendarData(data) {
    const calendarEvents = [];
    data.forEach(element => {
        calendarEvents.push(new CalendarEventModel(
            { 
                id: element.id,
                title: element.title,
                meta: { notes: element.description },
                start: element.date,
                end: element.date,
                allDay: true,                
                isCompleted: element.isCompleted,
                leadId: element.leadId,
                serviceId: element.serviceId
             }));
    });
    return calendarEvents;
  }

  private getFilterOptionModel() {
    return {
        pageNumber: 1,
        pageSize: 1000,
        fields: '',
        searchQuery:  'UserId=="' + this.openIdConnectService.user.profile.sub + '"',
        orderBy: 'UpdatedOn desc'
    }
  }
}
