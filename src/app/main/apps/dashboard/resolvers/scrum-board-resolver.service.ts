import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { ScrumBoardService } from '../dashboard-grid/widgets/scrum-board/service/scrum-board.service';
import { Board } from 'app/shared/components/scrumboard/board.model';
import { List } from 'app/shared/components/scrumboard/list.model';
import { Card } from 'app/shared/components/scrumboard/card.model';

@Injectable({
  providedIn: 'root'
})
export class ScrumBoardResolverService {

  constructor(
    private scrumBoardService: ScrumBoardService,
  ) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {    
    return this.scrumBoardService.getLead().pipe(map(data => {
      if (data) {
        return this.getScrumBoardData(data);
      }
      return null; // map returns value as an observable so we don't need to use of
    }), catchError((err: any) => {
      return of(null);
    }));
  }

  private getScrumBoardData(leads: any[]) {      
    const newLead = leads.filter(x => x.status?.toLowerCase() === 'new');
    const alreadyAvailed = leads.filter(x => x.status?.toLowerCase() === 'already availed');
    const contacted = leads.filter(x => x.status?.toLowerCase() === 'contacted');
    const notInterested = leads.filter(x => x.status?.toLowerCase() === 'not interested');
    const interested = leads.filter(x => x.status?.toLowerCase() === 'interested');
    const working = leads.filter(x => x.status?.toLowerCase() === 'working');
    const processing = leads.filter(x => x.status?.toLowerCase() === 'processing');
    const closed = leads.filter(x => x.status?.toLowerCase() === 'closed');
    

    const board = new Board({ name: 'Lead Board'});

    board.lists.push(new List({ id: '101', name: 'New'}));
    board.lists.push(new List({ id: '103', name: 'Already Availed'}));
    board.lists.push(new List({ id: '104', name: 'Contacted'}));
    board.lists.push(new List({ id: '105', name: 'Not Interested'}));
    board.lists.push(new List({ id: '106', name: 'Interested'}));
    board.lists.push(new List({ id: '107', name: 'Working'}));
    board.lists.push(new List({ id: '108', name: 'Processing'}));
    board.lists.push(new List({ id: '109', name: 'Closed'}));
    

    const newList = board.lists.filter(x => x.name === 'New');
    newList[0].idCards = [];
    newLead.forEach(elem => {
        newList[0].idCards.push(elem.id);
        board.cards.push(new Card({ id: elem.id, name: `${elem?.contactDto?.firstName} ${elem?.contactDto?.lastName}`}));
    });     

    const alreadyAvailledList = board.lists.filter(x => x.name === 'Already Availed');
    alreadyAvailledList[0].idCards = [];
    alreadyAvailed.forEach(elem => {
        alreadyAvailledList[0].idCards.push(elem.id);
        board.cards.push(new Card({ id: elem.id, name: `${elem?.contactDto?.firstName} ${elem?.contactDto?.lastName}`}));
    });

    const contactedList = board.lists.filter(x => x.name === 'Contacted');
    contactedList[0].idCards = [];
    contacted.forEach(elem => {
        contactedList[0].idCards.push(elem.id);
        board.cards.push(new Card({ id: elem.id, name: `${elem?.contactDto?.firstName} ${elem?.contactDto?.lastName}`}));
    });

    const notInterestedList = board.lists.filter(x => x.name === 'Not Interested');
    notInterestedList[0].idCards = [];
    notInterested.forEach(elem => {
        notInterestedList[0].idCards.push(elem.id);
        board.cards.push(new Card({ id: elem.id, name: `${elem?.contactDto?.firstName} ${elem?.contactDto?.lastName}`}));
    });

    const interestedList = board.lists.filter(x => x.name === 'Interested');
    interestedList[0].idCards = [];
    interested.forEach(elem => {
        interestedList[0].idCards.push(elem.id);
        board.cards.push(new Card({ id: elem.id, name: `${elem?.contactDto?.firstName} ${elem?.contactDto?.lastName}`}));
    });

    const workingList = board.lists.filter(x => x.name === 'Working');
    workingList[0].idCards = [];
    working.forEach(elem => {
        workingList[0].idCards.push(elem.id);
        board.cards.push(new Card({ id: elem.id, name: `${elem?.contactDto?.firstName} ${elem?.contactDto?.lastName}`}));
    });

    const processingList = board.lists.filter(x => x.name === 'Processing');
    processingList[0].idCards = [];
    processing.forEach(elem => {
        processingList[0].idCards.push(elem.id);
        board.cards.push(new Card({ id: elem.id, name: `${elem?.contactDto?.firstName} ${elem?.contactDto?.lastName}`}));
    });

    const closedList = board.lists.filter(x => x.name === 'Closed');
    closedList[0].idCards = [];
    closed.forEach(elem => {
        closedList[0].idCards.push(elem.id);
        board.cards.push(new Card({ id: elem.id, name: `${elem?.contactDto?.firstName} ${elem?.contactDto?.lastName}`}));
    });

    return board;
  }
}
