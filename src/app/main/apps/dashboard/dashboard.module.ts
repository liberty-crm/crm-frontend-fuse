import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SharedModule } from 'app/shared/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { GridsterModule } from 'angular-gridster2';
import { DashboardGridComponent } from './dashboard-grid/dashboard-grid.component';
import { ParentContainerComponent } from './dashboard-grid/parent-container/parent-container.component';
import { SummaryCardsComponent } from './dashboard-grid/widgets/summary-cards/summary-cards.component';
import { LeadsOverviewComponent } from './dashboard-grid/widgets/leads-overview/leads-overview.component';
import { ToDoListComponent } from './dashboard-grid/widgets/to-do-list/to-do-list.component';
import { CalendarComponent } from './dashboard-grid/widgets/calendar/calendar.component';
import { ScrumBoardComponent } from './dashboard-grid/widgets/scrum-board/scrum-board.component';
import { FuseWidgetModule } from '@fuse/components';
import { FuseSharedModule } from '@fuse/shared.module';
import { MatButtonModule } from '@angular/material/button';
import { MatDividerModule } from '@angular/material/divider';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatSelectModule } from '@angular/material/select';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { TodoService } from 'app/shared/components/todo/todo.service';
import { BoardResolve } from 'app/shared/components/scrumboard/scrumboard.service';
import { CalendarService } from 'app/shared/components/calendar/calendar.service';

const routes: Routes = [
  { 
    path: '', 
  component: DashboardComponent,
  resolve  : {
    todo: TodoService,
    board: BoardResolve,
    chat: CalendarService
    }
  },
];

@NgModule({
  declarations: [
    DashboardComponent, 
    DashboardGridComponent, 
    ParentContainerComponent, 
    SummaryCardsComponent, 
    LeadsOverviewComponent, 
    ToDoListComponent, 
    CalendarComponent, 
    ScrumBoardComponent,],
  imports: [
    CommonModule,
    SharedModule,
    GridsterModule,

    MatButtonModule,
    MatDividerModule,
    MatFormFieldModule,
    MatIconModule,
    MatMenuModule,
    MatSelectModule, 
    NgxChartsModule,

    FuseSharedModule,
    FuseWidgetModule,

    RouterModule.forChild(routes)
  ]
})
export class DashboardModule { }
