import { Component, OnInit } from '@angular/core';
import { DashboardOption } from '../models/dashboard-option.model';
import { Subject } from 'rxjs';
import { trigger, transition, style, animate } from '@angular/animations';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  animations: [
    trigger('slideInOut', [
      transition(':enter', [
        style({transform: 'translateY(-100%)'}),
        animate('200ms ease-in', style({transform: 'translateY(0%)'}))
      ]),
      transition(':leave', [
        animate('200ms ease-in', style({transform: 'translateY(-100%)'}))
      ])
    ]),
    trigger('fade', [
      transition('void => *', [
        style({ opacity: 0 }),
        animate(3000, style({opacity: 1}))
      ])
    ])
  ]
})
export class DashboardComponent implements OnInit {
  dashboardOPtions: DashboardOption[];
  isDashboardOPtionsPanel = false;
  dashboardOptionEventSubject: Subject<DashboardOption[]> = new Subject<DashboardOption[]>();
  constructor() { }

  ngOnInit(): void {
    this.initDashboardOptions();
  }

  initDashboardOptions(): void {
    this.dashboardOPtions = [
    //   { Name: 'Summary Cards', Type: 'summary-cards', Rows: 1, Cols: 4, IsChecked: true },
    { Name: 'To Do List', Type: 'to-do-list', Rows: 4, Cols: 4, IsChecked: true },
      { Name: 'Calendar', Type: 'calender', Rows: 4, Cols: 4, IsChecked: true },
    //   { Name: 'Scrum Board', Type: 'scrum-board', Rows: 4, Cols: 4, IsChecked: true },
    //   { Name: 'Leads Overview Chart', Type: 'leads-overview', Rows: 4, Cols: 2, IsChecked: true },
    //   { Name: 'To Do List', Type: 'to-do-list', Rows: 4, Cols: 2, IsChecked: true },
    ];
  }

  toggleDashboardOptionPanel(): void {
    this.isDashboardOPtionsPanel = !this.isDashboardOPtionsPanel;
  }

  dashboardOptionChanged(): void {
    this.dashboardOptionEventSubject.next(this.dashboardOPtions);
  }

}
