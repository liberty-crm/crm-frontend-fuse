import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-leads-overview',
  templateUrl: './leads-overview.component.html',
  styleUrls: ['./leads-overview.component.scss']
})
export class LeadsOverviewComponent implements OnInit {

  leadsOveriewWidget: any;
  constructor() { }

  ngOnInit(): void {
    this.leadsOveriewWidget = {
      legend       : false,
      explodeSlices: false,
      labels       : true,
      doughnut     : false,
      gradient     : false,
      scheme       : {
          domain: ['#f44336', '#9c27b0', '#03a9f4', '#e91e63', '#ffc107']
      },
      value: [
        {name: 'New', value: 30},
        {name: 'Contacted', value: 20},
        {name: 'Qualified', value: 15},
        {name: 'Working', value: 12},
        {name: 'Proposal Sent', value: 10},
        {name: 'Customer', value: 8},
        {name: 'Lost Leads', value: 10}
      ],
      onSelect     : (ev) => {
          console.log(ev);
      }
  };
  }

}
