import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Lead } from 'app/main/widget/lead-widget/models/lead.model';

@Injectable({
  providedIn: 'root'
})
export class ScrumBoardService {
  
  private Url = environment.apiUrl;
  constructor(private http: HttpClient) { }

  getLead(): Observable<Lead[]> {  
    return this.http.get<Lead[]>(this.Url + 'leads');
  }

}
