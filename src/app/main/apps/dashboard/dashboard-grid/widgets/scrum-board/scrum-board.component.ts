import { Component, OnInit, OnDestroy } from '@angular/core';
import { ScrumBoardService } from './service/scrum-board.service';
import { Subscription } from 'rxjs';
import { Board } from 'app/shared/components/scrumboard/board.model';
import { List } from 'app/shared/components/scrumboard/list.model';
import { Card } from 'app/shared/components/scrumboard/card.model';
import { ActivatedRoute } from '@angular/router';
import { CustomEventService } from 'app/shared/services/custom-event-serverice/custom-event.service';
import { LeadService } from 'app/main/apps/settings/lead/services/lead.service';

@Component({
  selector: 'app-scrum-board',
  templateUrl: './scrum-board.component.html',
  styleUrls: ['./scrum-board.component.scss']
})
export class ScrumBoardComponent implements OnInit, OnDestroy {

  private subscription: Subscription = new Subscription();
  board: Board;
  leads = [];
  constructor(private scrumBoardService: ScrumBoardService,
    private leadService: LeadService,
    private activatedRoute: ActivatedRoute,
    private customEventService: CustomEventService) { 
        this.getLeads();
        this.board = this.activatedRoute.snapshot.data.scrumboard;   
        this.board.settings = this.activatedRoute.snapshot.data.board.settings;
  }
    

  ngOnInit(): void {
    this.subscription.add(this.customEventService.CardDropEvent
        .subscribe(res => {
            const leadId = res;
            const leadList = this.board.lists.filter(x => x.idCards.indexOf(leadId) !== -1);
            const lead = this.leads.filter(x => x.id === leadId);
            lead[0].status = leadList[0].name;
            this.updateLeadStatus(leadId, lead[0].status);
        }));
  }
  
  getLeads() {
      this.subscription.add(this.leadService.getLead()
      .subscribe(res => {
        this.leads = res;
      }))
  }

  private updateLeadStatus(leadId, status) {
      const partialLead = [{  "value": status, "path": "/status", "op": "replace" }]
      this.subscription.add(this.leadService.updateLeadPartially(leadId, partialLead)
      .subscribe(res => {
        // do nothing
      }));
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

}
