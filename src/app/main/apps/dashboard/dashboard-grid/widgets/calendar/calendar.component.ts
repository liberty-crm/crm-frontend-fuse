import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CalendarEventModel } from 'app/shared/components/calendar/event.model';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss']
})
export class CalendarComponent implements OnInit {

  calendarEvents: CalendarEventModel[];
  constructor(private activatedRoute: ActivatedRoute) {      
    this.calendarEvents = this.activatedRoute.snapshot.data.calendarEvents;
  }

  ngOnInit(): void {
  }

}
