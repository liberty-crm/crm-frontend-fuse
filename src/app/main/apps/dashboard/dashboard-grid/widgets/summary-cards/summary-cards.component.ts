import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-summary-cards',
  templateUrl: './summary-cards.component.html',
  styleUrls: ['./summary-cards.component.scss']
})
export class SummaryCardsComponent implements OnInit {

  summaryCards = [
    { title: 'Invoices Awaiting Payment', subtitle: '11 / 14', color: 'accent', value: 80 },
    { title: 'Triggered Leads', subtitle: '8 / 50', color: 'warn', value: 15},
    { title: 'Todays Meetings/Reminders', subtitle: '4 / 4', color: 'accent', value: 100 },
    { title: 'Tasks Not Finished', subtitle: '46 / 62', color: 'teal', value: 70 }
  ];
  constructor() { }

  ngOnInit(): void {
  }

}
