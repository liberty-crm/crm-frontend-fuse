import { Component, OnInit, Input } from '@angular/core';
import { CalendarEvent } from 'angular-calendar';
import { Task } from 'app/shared/components/task/models/task.model';
import { MatDialog } from '@angular/material/dialog';
import { CalendarEventFormDialogComponent } from 'app/shared/components/calendar/event-form/event-form.component';
import { CalendarEventModel } from 'app/shared/components/calendar/event.model';
import { CustomEventService } from 'app/shared/services/custom-event-serverice/custom-event.service';

@Component({
  selector: 'app-parent-container',
  templateUrl: './parent-container.component.html',
  styleUrls: ['./parent-container.component.scss']
})
export class ParentContainerComponent implements OnInit {

  @Input() widget;
  dialogRef: any;

  constructor(private _matDialog: MatDialog,
              private customEventService: CustomEventService) { }

  ngOnInit(): void {
  }


  editTask(task: Task): void
    {
        const taskEvent = { 
            id: task.id,
            title: task.title,
            meta: { notes: task.description },
            start: task.date,
            end: task.date,
            allDay: true,
            isCompleted: task.isCompleted
         }        
        this.dialogRef = this._matDialog.open(CalendarEventFormDialogComponent, {
            panelClass: 'event-form-dialog',
            data      : {
                event : taskEvent,
                action: 'edit'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                if ( !response )
                {
                    return;
                }                                
                this.customEventService.CalendarEventUpdated.next(response);
            });
    }

}
