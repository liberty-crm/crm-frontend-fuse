export class SensitiveInfo {
    leadId: string;
    token: string;
    sin: string;
}