import { Component, OnInit, ViewEncapsulation, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { FuseConfigService } from '@fuse/services/config.service';
import { fuseAnimations } from '@fuse/animations';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { SensitiveInfo } from './models/sensitive-info.model';
import { LeadService } from '../settings/lead/services/lead.service';
import { SnackbarService } from 'app/shared/services/snackbar/snackbar.service';
import { TokenValidity } from './models/token-validity.model';
import { CustomAsyncValidator } from 'app/shared/async-validators/async-validators';
import { REGEX_PATTERNS } from 'app/shared/constants/regex-pattern.constant';

@Component({
    selector     : 'app-lead-sin-details',
    templateUrl  : './lead-sin-details.component.html',
    styleUrls    : ['./lead-sin-details.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class LeadSinDetailsComponent implements OnInit, OnDestroy
{
    private subscription: Subscription = new Subscription();
    sensitiveInfoForm: FormGroup;  
    isSinMatch = true;
    leadId: string;
    token: string;
    tokenValidityDetails: TokenValidity;
    isDetailsUpdated = false;
    constructor(
        private _fuseConfigService: FuseConfigService,
        private _formBuilder: FormBuilder,
        private activatedRoute: ActivatedRoute,
        private leadService: LeadService,
        private customAsyncValidator: CustomAsyncValidator,
        private snackbarService: SnackbarService
    ) {
        this.subscription.add(this.activatedRoute.params.subscribe(params => {
            this.leadId = params.id;
            this.token = params.token
          }));   

        this.tokenValidityDetails = this.activatedRoute.snapshot.data.tokenValidityDetails;

        // Configure the layout
        this._fuseConfigService.config = {
            layout: {
                navbar   : {
                    hidden: true
                },
                toolbar  : {
                    hidden: true
                },
                footer   : {
                    hidden: true
                },
                sidepanel: {
                    hidden: true
                }
            }
        };
    }
    

    ngOnInit(): void
    {
        this.sensitiveInfoForm = this._formBuilder.group({
            sin: ['', [Validators.required, Validators.pattern(REGEX_PATTERNS.SIN)], this.customAsyncValidator.sinValidator(this.leadId)],
            confirmSin: ['', [Validators.required]]
        });
    }

    checkIsSinMatch() {
        if (this.sensitiveInfoForm.get('confirmSin').value) {
            if (this.sensitiveInfoForm.get('sin').value === this.sensitiveInfoForm.get('confirmSin').value) {
                this.isSinMatch = true;
                this.sensitiveInfoForm.controls['confirmSin'].setErrors(null);
            } else {
                this.isSinMatch = false;
                this.sensitiveInfoForm.controls['confirmSin'].setErrors({'incorrect': true});
            }   
        } else {
           setTimeout(() => {
            this.isSinMatch = true;
           });              
        }   
    }

    submitForm() {
        this.sensitiveInfoForm.markAllAsTouched();
        if (this.sensitiveInfoForm.valid) {
            const sensitiveInfo = new SensitiveInfo();
            sensitiveInfo.leadId = this.leadId;
            sensitiveInfo.token = this.token;
            sensitiveInfo.sin = this.sensitiveInfoForm.get('sin').value;            
            this.subscription.add(this.leadService.updateSensitiveInfo(sensitiveInfo)
            .subscribe(res => {                
                this.isDetailsUpdated = true;
                this.snackbarService.show("SIN updated successfully!");
                setTimeout(() => {
                    window.scroll(0,0);
                }, 50);
            }));
        }
    }

    ngOnDestroy(): void {
        this.subscription.unsubscribe();
    }
}
