import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { LeadService } from '../../settings/lead/services/lead.service';

@Injectable({
  providedIn: 'root'
})
export class IsSinTokenValidResolverService {

  constructor(
    private leadService: LeadService,
  ) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    const leadId = route.params['id'];
    const token = route.params['token'];
    return this.leadService.isSinTokenValid(leadId, token).pipe(map(data => {
      if (data) {
        return data;
      }
      return null; // map returns value as an observable so we don't need to use of
    }), catchError((err: any) => {
      return of(null);
    }));
  }
}
