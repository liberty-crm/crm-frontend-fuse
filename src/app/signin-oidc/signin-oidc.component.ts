import { Component, OnInit, OnDestroy } from '@angular/core';
import { OpenIdConnectService } from '../shared/services/open-id-connect.service';
import { Router } from '@angular/router';
import { environment } from '../../environments/environment';
import { isNullOrUndefined } from 'util';
import { navigation } from 'app/navigation/navigation';
import { FuseNavigationService } from '@fuse/components/navigation/navigation.service';
import { CustomEventService } from 'app/shared/services/custom-event-serverice/custom-event.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-signin-oidc',
  templateUrl: './signin-oidc.component.html',
  styleUrls: ['./signin-oidc.component.scss']
})
export class SigninOidcComponent implements OnInit, OnDestroy {

 private subscription: Subscription = new Subscription();
 
  constructor(private openIdConnectService: OpenIdConnectService,
    private _fuseNavigationService: FuseNavigationService,
    private _customEventService: CustomEventService,
    private router: Router) { }    

  ngOnInit() {    
    this.openIdConnectService.userLoaded$.subscribe((userLoaded) => {              
      if (userLoaded) {    
        var url = localStorage.getItem('redirectUrl');
        if(!isNullOrUndefined(url))
        {
          localStorage.removeItem('redirectUrl');
          this.router.navigate([url]);

        }
        else
        {
          this.router.navigate(['./']);
        }        
      }
      else {
        if (!environment.production) {
          console.log("An error happened: user wasn't loaded.");
        }
      }
    });

    this.openIdConnectService.handleCallback();
  }  

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

}
