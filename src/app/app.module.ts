import { NgModule, Injector } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, Routes } from '@angular/router';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { TranslateModule } from '@ngx-translate/core';
import 'hammerjs';

import { FuseModule } from '@fuse/fuse.module';
import { FuseSharedModule } from '@fuse/shared.module';
import { FuseProgressBarModule, FuseSidebarModule, FuseThemeOptionsModule } from '@fuse/components';

import { fuseConfig } from 'app/fuse-config';

import { AppComponent } from 'app/app.component';
import { LayoutModule } from 'app/layout/layout.module';
import { SampleModule } from 'app/main/sample/sample.module';
import { SigninOidcComponent } from './signin-oidc/signin-oidc.component';
import { RequireAuthenticatedUserRouteGuardService } from './shared/services/require-authenticated-user-route-guard.service';
import { OpenIdConnectService } from './shared/services/open-id-connect.service';
import { AddAuthorizationHeaderInterceptorService } from './shared/interceptors/add-authorization-header-interceptor.service';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { NgHttpLoaderModule } from 'ng-http-loader';
import { TestWidgetComponent } from './main/widget/test-widget/test-widget.component';
import { createCustomElement } from '@angular/elements';
import { APP_BASE_HREF } from '@angular/common';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { CreateLeadWidgetComponent } from './main/widget/lead-widget/components/create-lead-widget/create-lead-widget.component';
import { ScrumBoardResolverService } from './main/apps/dashboard/resolvers/scrum-board-resolver.service';
import { CalendarResolverService } from './main/apps/dashboard/resolvers/calendar-resolver.service';
import { LeadSinDetailsComponent } from './main/apps/lead-sin-details/lead-sin-details.component';
import { IsSinTokenValidResolverService } from './main/apps/lead-sin-details/services/lead-sin-resolver.service';
import { ReportResolverService } from './main/apps/settings/report/services/report-resolver.service';
import { RequireAuthorizedUserRouteGuardService } from './gaurds/require-authorized-user-route-guard.service';
import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { FakeDbService } from './fake-db/fake-db.service';

const appRoutes: Routes = [
    { path: '', redirectTo: 'dashboard', pathMatch: 'full' }, 
    {
        path: 'profile',
        canActivate: [RequireAuthenticatedUserRouteGuardService],
        loadChildren: () => import('./main/apps/settings/profile/profile.module').then(m => m.ProfileModule)
    },  
    {
        path: 'dashboard',
        canActivate: [RequireAuthenticatedUserRouteGuardService],
        resolve: { scrumboard: ScrumBoardResolverService, calendarEvents: CalendarResolverService },
        loadChildren: () => import('./main/apps/dashboard/dashboard.module').then(m => m.DashboardModule)
    },
    {
        path: 'user',
        canActivate: [RequireAuthenticatedUserRouteGuardService, RequireAuthorizedUserRouteGuardService],
        loadChildren: () => import('./main/apps/settings/user/user.module').then(m => m.UserModule)
    },
    {
        path: 'lead',
        canActivate: [RequireAuthenticatedUserRouteGuardService],
        loadChildren: () => import('./main/apps/settings/lead/lead.module').then(m => m.LeadModule)
    },
    {
        path: 'lead-sin-details/:id/:token',
        component: LeadSinDetailsComponent,
        resolve: { tokenValidityDetails: IsSinTokenValidResolverService}
    },
        
    {
        path: 'signin-oidc',
        component: SigninOidcComponent
    },
    {
        path: 'asset',
        canActivate: [RequireAuthenticatedUserRouteGuardService, RequireAuthorizedUserRouteGuardService],
        loadChildren: () => import('./main/apps/settings/asset/asset.module').then(m => m.AssetModule)
    },
    {
        path: 'serviceoffered',
        canActivate: [RequireAuthenticatedUserRouteGuardService, RequireAuthorizedUserRouteGuardService],
        loadChildren: () => import('./main/apps/settings/serviceoffered/serviceoffered.module').then(m => m.ServiceofferedModule)
    },
    {
        path: 'store',
        canActivate: [RequireAuthenticatedUserRouteGuardService, RequireAuthorizedUserRouteGuardService],
        loadChildren: () => import('./main/apps/settings/store/store.module').then(m => m.StoreModule)
    },
    {
        path: 'product',
        canActivate: [RequireAuthenticatedUserRouteGuardService, RequireAuthorizedUserRouteGuardService],
        loadChildren: () => import('./main/apps/settings/product/product.module').then(m => m.ProductModule)
    },
    {
        path: 'report',
        canActivate: [RequireAuthenticatedUserRouteGuardService, RequireAuthorizedUserRouteGuardService],
        resolve: { report: ReportResolverService },
        loadChildren: () => import('./main/apps/settings/report/report.module').then(m => m.ReportModule)
    },
    {
        path: 'error404',
        loadChildren: () => import('./main/apps/error-pages/error-404.module').then(m => m.Error404Module)
    },
    // Not found
    { path: '**', redirectTo: 'dashboard' }
];

@NgModule({
    declarations: [
        AppComponent,
        SigninOidcComponent,
        TestWidgetComponent,
        CreateLeadWidgetComponent,
        LeadSinDetailsComponent
    ],
    imports     : [
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        NgHttpLoaderModule.forRoot(),
        RouterModule.forRoot(appRoutes),

        TranslateModule.forRoot(),
        InMemoryWebApiModule.forRoot(FakeDbService, {
            delay             : 0,
            passThruUnknownUrl: true
        }),

        // Material moment date module
        MatMomentDateModule,

        // Material
        MatButtonModule,
        MatIconModule,
        MatSnackBarModule,        
        MatFormFieldModule,        
        MatInputModule,

        // Fuse modules
        FuseModule.forRoot(fuseConfig),
        FuseProgressBarModule,
        FuseSharedModule,
        FuseSidebarModule,
        FuseThemeOptionsModule,

        // App modules
        LayoutModule,
        SampleModule
    ],
    providers: [  
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AddAuthorizationHeaderInterceptorService,
            multi: true,
        },         
        { provide: APP_BASE_HREF, useValue: '/' },
        RequireAuthenticatedUserRouteGuardService,
        OpenIdConnectService
    ],
    entryComponents: [CreateLeadWidgetComponent],
    bootstrap: [AppComponent]
})
export class AppModule
{
    constructor(private injector: Injector) { }
    ngDoBootstrap(): void {
        const el = createCustomElement(CreateLeadWidgetComponent, {
            injector: this.injector
        });
        // tslint:disable-next-line: no-unused-expression
        customElements.get('create-lead-widget') || customElements.define('create-lead-widget', el);
    }
}
