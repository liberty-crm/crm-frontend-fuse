import * as moment from 'moment';
import { HttpHeaders } from '@angular/common/http';

export class FuseUtils
{
    /**
     * Filter array by string
     *
     * @param mainArr
     * @param searchText
     * @returns {any}
     */
    public static filterArrayByString(mainArr, searchText): any
    {
        if ( searchText === '' )
        {
            return mainArr;
        }

        searchText = searchText.toLowerCase();

        return mainArr.filter(itemObj => {
            return this.searchInObj(itemObj, searchText);
        });
    }

    /**
     * Search in object
     *
     * @param itemObj
     * @param searchText
     * @returns {boolean}
     */
    public static searchInObj(itemObj, searchText): boolean
    {
        for ( const prop in itemObj )
        {
            if ( !itemObj.hasOwnProperty(prop) )
            {
                continue;
            }

            const value = itemObj[prop];

            if ( typeof value === 'string' )
            {
                if ( this.searchInString(value, searchText) )
                {
                    return true;
                }
            }

            else if ( Array.isArray(value) )
            {
                if ( this.searchInArray(value, searchText) )
                {
                    return true;
                }
            }

            if ( typeof value === 'object' )
            {
                if ( this.searchInObj(value, searchText) )
                {
                    return true;
                }
            }
        }
    }

    /**
     * Search in array
     *
     * @param arr
     * @param searchText
     * @returns {boolean}
     */
    public static searchInArray(arr, searchText): boolean
    {
        for ( const value of arr )
        {
            if ( typeof value === 'string' )
            {
                if ( this.searchInString(value, searchText) )
                {
                    return true;
                }
            }

            if ( typeof value === 'object' )
            {
                if ( this.searchInObj(value, searchText) )
                {
                    return true;
                }
            }
        }
    }

    /**
     * Search in string
     *
     * @param value
     * @param searchText
     * @returns {any}
     */
    public static searchInString(value, searchText): any
    {
        return value.toLowerCase().includes(searchText);
    }

    /**
     * Generate a unique GUID
     *
     * @returns {string}
     */
    public static generateGUID(): string
    {
        function S4(): string
        {
            return Math.floor((1 + Math.random()) * 0x10000)
                       .toString(16)
                       .substring(1);
        }

        return S4() + S4();
    }

    /**
     * Toggle in array
     *
     * @param item
     * @param array
     */
    public static toggleInArray(item, array): void
    {
        if ( array.indexOf(item) === -1 )
        {
            array.push(item);
        }
        else
        {
            array.splice(array.indexOf(item), 1);
        }
    }

    /**
     * Handleize
     *
     * @param text
     * @returns {string}
     */
    public static handleize(text): string
    {
        return text.toString().toLowerCase()
                   .replace(/\s+/g, '-')           // Replace spaces with -
                   .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
                   .replace(/\-\-+/g, '-')         // Replace multiple - with single -
                   .replace(/^-+/, '')             // Trim - from start of text
                   .replace(/-+$/, '');            // Trim - from end of text
    }

    public static isFileValid(file: File, size: number, extension: string[]) {
        const fileExt = file.name.split('.')[1].toLowerCase();
        if (file.size > size) {
            return false;
        } else if (extension.indexOf(fileExt.toLowerCase()) === -1) {
            return false;
        }
        return true;
    }

    public static getFileType(file: File) {
        const fileType = file.type;
        if (fileType.includes('image')) {
            return 'image';
        } else if (fileType.includes('document') && !fileType.includes('sheet')) {
            return 'document';
        } else if (fileType.includes('sheet')) {
            return 'spreadsheet';
        } else if (fileType.includes('pdf')) {
            return 'pdf';
        } else {
            return '';
        }
    }

    public static getDifference(oldValue, newValue): string
    {        
        const tagData = [];
        const diff = require('deep-diff').diff;
        const differences = diff(oldValue, newValue);
        if (differences) {
            const editDiffereces = differences.filter(x => x.kind === 'E');
            editDiffereces.forEach((difference, index) => {
                if (difference.kind === 'E')
                {
                    const obj = new Object() as any;
                    obj.key = this.getMappedKey(difference.path[2]);
                    obj.key = this.camelCaseToWord(obj.key);
                    obj.oldValue = difference.lhs;
                    obj.newValue = difference.rhs;
                    tagData.push(obj);
                }            
              });  
        }                       
        return tagData && tagData.length > 0 ? JSON.stringify(tagData) : '';
    }

    public static getDateDiff(date1, date2) {
        var a = moment(date1);
        var b = moment(date2);
        let secondDiff = a.diff(b, 'seconds');
        let minuteDiff = a.diff(b, 'minutes');
        let hourDiff = a.diff(b, 'hours');
        let dayDiff = a.diff(b, 'days');
        return {
            secondDiff,
            minuteDiff,
            hourDiff,
            dayDiff
        };
    }

    public static getSkipHttpLoaderHeader() {
        let headers = new HttpHeaders();
        headers = headers.set('x-skip-http-loader', '');
        return { headers };
      }

    public static camelCaseToWord(text: string) {
        return text.replace(/([A-Z])/g, ' $1')
            .replace(/^./, function (str) { return str.toUpperCase(); })
    }

    public static getMappedKey(key: string) {
        switch(key) {
            case 'zipcode':
                return 'postalCode';
            case 'phone':
                return 'phoneNo';
            default:
                return key;
        }
    }

    // https://medium.com/javascript-in-plain-english/it-is-really-easy-to-convert-local-time-to-utc-in-javascript-7e6a78460a7d
    public static getUTCDate() {
        const utcDateTimeString = new Date().toISOString();
        const utcDateString = utcDateTimeString.substr(0, utcDateTimeString.indexOf('T'));
        return new Date(utcDateString);
    }
}
